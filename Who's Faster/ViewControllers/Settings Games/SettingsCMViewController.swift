//
//  ThirdViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 7/22/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
var NOR = 3
//Number of Rounds
class ThirdViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate {

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if noAds == false {
            if popup.isReady {
                popup.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
       self.navigationController?.setNavigationBarHidden(false, animated: true)
        if noAds == false {
            
            
            // addBannerViewToView(bannerView)
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: Settings1Weak.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        normalFont()
        Forever.invalidate()
        self.view.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
        titleLabel.textColor = UIColor.black
        settingsColonLabel.textColor = UIColor.black
        TopSettingsWeak.textColor = UIColor.black
        SettingsLabel.textColor = UIColor.black
        namesWeak.backgroundColor = UIColor.orange
        StartWeak.backgroundColor = UIColor.green
        if hasTheme == true {
            if CustomTheme == true {
                normalFont()
                self.view.backgroundColor = PrimaryColor
                StartWeak.backgroundColor = SecondaryColor
                namesWeak.backgroundColor = SecondaryColor
                titleLabel.textColor = TextColor
                settingsColonLabel.textColor = TextColor
                TopSettingsWeak.textColor = TextColor
                SettingsLabel.textColor = TextColor
                
            } else {
                if Theme == 0 {
                    //default
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.orange
                    StartWeak.backgroundColor = UIColor.green
                } else if Theme == 1 {
                    //dark
                    TopSettingsWeak.font = UIFont (name: "Marker Felt", size: 30)!
                    SettingsLabel.font = UIFont (name: "Marker Felt", size: 30)!
                    titleLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    settingsColonLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    StartWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    namesWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    self.view.backgroundColor = UIColor(red: (128/255.0), green: (128/255.0), blue: (128/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.black
                    StartWeak.backgroundColor = UIColor.darkGray
                } else if Theme == 2 {
                    //chalk
                    TopSettingsWeak.font = UIFont (name: "Chalkduster", size: 30)!
                    SettingsLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    titleLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    settingsColonLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    StartWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    namesWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TopSettingsWeak.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    namesWeak.backgroundColor = UIColor.green
                    StartWeak.backgroundColor = UIColor.green
                } else if Theme == 3 {
                    //anicent
                    TopSettingsWeak.font = UIFont (name: "Papyrus", size: 30)!
                    SettingsLabel.font = UIFont (name: "Papyrus", size: 30)!
                    titleLabel.font = UIFont (name: "Papyrus", size: 25)!
                    settingsColonLabel.font = UIFont (name: "Papyrus", size: 25)!
                    StartWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    namesWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    self.view.backgroundColor = UIColor(red: (160/255.0), green: (82/255.0), blue: (45/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TopSettingsWeak.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.brown
                } else if Theme == 4 {
                    //notes
                    TopSettingsWeak.font = UIFont (name: "Noteworthy", size: 30)!
                    SettingsLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    titleLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    settingsColonLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    StartWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    namesWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.yellow
                } else if Theme == 5 {
                    //space
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TopSettingsWeak.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    namesWeak.backgroundColor = UIColor.black
                    StartWeak.backgroundColor = UIColor.black
                } else if Theme == 6 {
                    //ocean
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    titleLabel.textColor = UIColor.blue
                    settingsColonLabel.textColor = UIColor.blue
                    TopSettingsWeak.textColor = UIColor.blue
                    SettingsLabel.textColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.cyan
                } else if Theme == 7 {
                    //red
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (178/255.0), green: (34/255.0), blue: (34/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.red
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.blue
                } else if Theme == 9 {
                    //green
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.green
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (148/255.0), green: (0/255.0), blue: (211/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.purple
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.orange
                    StartWeak.backgroundColor = UIColor.orange
                } else if Theme == 12 {
                    //pink
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (193/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (199/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor(red: (169/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (218/255.0), green: (165/255.0), blue: (32/255.0), alpha: 1.0)
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TopSettingsWeak.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.yellow
                    StartWeak.backgroundColor = UIColor.yellow
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        StartWeak.layer.cornerRadius = 10
        StartWeak.clipsToBounds = true
        namesWeak.layer.cornerRadius = 10
        namesWeak.clipsToBounds = true
        
        if doingMD == true {
            StartWeak.isHidden = true
            namesWeak.isHidden = true
        }
        TopSettingsWeak.adjustsFontSizeToFitWidth = true
        SettingsLabel.adjustsFontSizeToFitWidth = true
        StartWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        namesWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        settingsColonLabel.adjustsFontSizeToFitWidth = true
        titleLabel.adjustsFontSizeToFitWidth = true
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       
    }
    
    func normalFont() {
        TopSettingsWeak.font = UIFont (name: "Helvetica Neue", size: 30)!
        SettingsLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        titleLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
        settingsColonLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
        StartWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
         namesWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    @IBOutlet weak var Settings1Weak: UISlider!
    
    @IBOutlet weak var namesWeak: UIButton!
    
    @IBOutlet weak var settingsColonLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var SettingsLabel: UILabel!
   
    @IBOutlet weak var StartWeak: UIButton!
    
    @IBOutlet weak var TopSettingsWeak: UILabel!
    @IBAction func Settings1(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        
        SettingsLabel.text = "\(currentValue)"
        
         NOR = Int("\(currentValue)")!
    }
    
    
    
    
    @IBAction func SCM(_ sender: UIButton) {
        //start CM
        PRN = "Player 1"
        //Player right now
         HP1P = "Not Played"
       
    }
    
    
    
    
    
   
    
}
