//
//  SettingsTapViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 9/3/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
var NOTBP = 5
//Number of times  button pressed
class SettingsTapViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Do any additional setup after loading the view.
        StartWeak.layer.cornerRadius = 10
        StartWeak.clipsToBounds = true
        namesWeak.layer.cornerRadius = 10
        namesWeak.clipsToBounds = true
        
        if doingMD == true {
            StartWeak.isHidden = true
            namesWeak.isHidden = true
        }
     TBSettingsLabel.adjustsFontSizeToFitWidth = true
        
        TBSettingslabelTop.adjustsFontSizeToFitWidth = true
       
      settingsColonLabel.adjustsFontSizeToFitWidth = true
        StartWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        namesWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel.adjustsFontSizeToFitWidth = true
        
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Forever.invalidate()
        self.view.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
        normalFont()
        StartWeak.backgroundColor = UIColor.blue
        namesWeak.backgroundColor = UIColor.orange
        titleLabel.textColor = UIColor.black
        settingsColonLabel.textColor = UIColor.black
        TBSettingsLabel.textColor = UIColor.black
        TBSettingslabelTop.textColor = UIColor.black
        if hasTheme == true {
            if CustomTheme == true {
                self.view.backgroundColor = PrimaryColor
                StartWeak.backgroundColor = SecondaryColor
                namesWeak.backgroundColor = SecondaryColor
                titleLabel.textColor = TextColor
                settingsColonLabel.textColor = TextColor
                TBSettingsLabel.textColor = TextColor
                TBSettingslabelTop.textColor = TextColor
                normalFont()
            } else {
                if Theme == 0 {
                    //default
                    self.view.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor.orange
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 1 {
                    //dark
                    self.view.backgroundColor = UIColor(red: (119/255.0), green: (136/255.0), blue: (153/255.0), alpha: 1.0)
                    namesWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    TBSettingsLabel.font = UIFont (name: "Marker Felt", size: 30)!
                    TBSettingslabelTop.font = UIFont (name: "Marker Felt", size: 23)!
                    titleLabel.font = UIFont (name: "Marker Felt", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    StartWeak.backgroundColor = UIColor(red: (112/255.0), green: (128/255.0), blue: (144/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 2 {
                    //chalk
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    namesWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    TBSettingsLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    TBSettingslabelTop.font = UIFont (name: "Chalkduster", size: 23)!
                    titleLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (250/255.0), blue: (154/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor.green
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TBSettingsLabel.textColor = UIColor.white
                    TBSettingslabelTop.textColor = UIColor.white
                } else if Theme == 3 {
                    //anicent
                    self.view.backgroundColor = UIColor.brown
                    namesWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    TBSettingsLabel.font = UIFont (name: "Papyrus", size: 30)!
                    TBSettingslabelTop.font = UIFont (name: "Papyrus", size: 23)!
                    titleLabel.font = UIFont (name: "Papyrus", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Papyrus", size: 25)!
                    StartWeak.backgroundColor = UIColor(red: (250/255.0), green: (235/255.0), blue: (215/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TBSettingsLabel.textColor = UIColor.white
                    TBSettingslabelTop.textColor = UIColor.white
                } else if Theme == 4 {
                    //notes
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    
                    
                    namesWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    TBSettingslabelTop.font = UIFont (name: "Noteworthy", size: 25)!
                    TBSettingsLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    titleLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    StartWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 5 {
                    //space
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    
                    normalFont()
                    StartWeak.backgroundColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.black
                    
                    titleLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    TBSettingsLabel.textColor = UIColor.white
                    TBSettingslabelTop.textColor = UIColor.white
                } else if Theme == 6 {
                    //ocean
                    
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.blue
                    settingsColonLabel.textColor = UIColor.blue
                    TBSettingsLabel.textColor = UIColor.blue
                    TBSettingslabelTop.textColor = UIColor.blue
                    normalFont()
                } else if Theme == 7 {
                    //red
                    self.view.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (220/255.0), green: (20255.0), blue: (60/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 8 {
                    //blue
                    self.view.backgroundColor = UIColor(red: (135/255.0), green: (206/255.0), blue: (250/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 9 {
                    //green
                    self.view.backgroundColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (46/255.0), green: (139/255.0), blue: (87/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 10 {
                    //purple
                    self.view.backgroundColor = UIColor(red: (72/255.0), green: (61/255.0), blue: (139/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 11 {
                    //orange
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (99/255.0), blue: (71/255.0), alpha: 1.0)
                    
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor.orange
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 12 {
                    //pink
                    self.view.backgroundColor = UIColor(red: (218/255.0), green: (112/255.0), blue: (214/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (199/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                } else if Theme == 13 {
                    //yellow
                    self.view.backgroundColor = UIColor(red: (218/255.0), green: (112/255.0), blue: (214/255.0), alpha: 1.0)
                    normalFont()
                    StartWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor.yellow
                    
                    titleLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    TBSettingsLabel.textColor = UIColor.black
                    TBSettingslabelTop.textColor = UIColor.black
                    
                }
            }
            
            
            
            
            
            
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        
        if noAds == false {
            if popup.isReady {
                popup.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
        
        
        if noAds == false {
            
            // addBannerViewToView(bannerView)
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: SliderWeak.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
        
        
        
    }
    
    func normalFont() {
        namesWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        StartWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        TBSettingsLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        TBSettingslabelTop.font = UIFont (name: "Helvetica Neue", size: 23)!
        titleLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        settingsColonLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var TBSettingsLabel: UILabel!
    
    @IBOutlet weak var StartWeak: UIButton!
    @IBOutlet weak var SliderWeak: UISlider!
    @IBOutlet weak var SettingsSliderWeak: UILabel!
    @IBOutlet weak var TBSettingslabelTop: UILabel!
    @IBAction func TBSettingSlider(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        
        TBSettingsLabel.text = "\(currentValue)"
        
        NOTBP = Int("\(currentValue)")!
    }
    
    
    
    
    @IBOutlet weak var settingsColonLabel: UILabel!
    
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    
    @IBOutlet weak var namesWeak: UIButton!
    
    
    
    
    
}
