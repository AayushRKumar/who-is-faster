//
//  SettingCWViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 9/22/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
var NOW = 2
//Number of words
var CWCount = 0
var Words = [String]()
var NumbersForWords = [Int]()
class SettingCWViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         Forever.invalidate()
        normalFont()
        NOWLabel.textColor = UIColor.black
        TopSettingsLabel.textColor = UIColor.black
        settingsColonLabel.textColor = UIColor.black
        titleLabel.textColor = UIColor.black
        self.view.backgroundColor = UIColor.blue
        namesWeak.backgroundColor = UIColor.orange
        StartWeak.backgroundColor = UIColor.purple
        if hasTheme == true {
            namesWeak.titleLabel?.adjustsFontSizeToFitWidth = true
            StartWeak.titleLabel?.adjustsFontSizeToFitWidth = true
            TopSettingsLabel.adjustsFontSizeToFitWidth = true
            NOWLabel.adjustsFontSizeToFitWidth = true
            titleLabel.adjustsFontSizeToFitWidth = true
            settingsColonLabel.adjustsFontSizeToFitWidth = true
            
            if CustomTheme == true {
                normalFont()
                self.view.backgroundColor = PrimaryColor
                NOWLabel.textColor = TextColor
                TopSettingsLabel.textColor = TextColor
                settingsColonLabel.textColor = TextColor
                titleLabel.textColor = TextColor
                namesWeak.backgroundColor = SecondaryColor
                StartWeak.backgroundColor = SecondaryColor
            } else {
                if Theme == 0 {
                    //default
                    normalFont()
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    self.view.backgroundColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor.orange
                    StartWeak.backgroundColor = UIColor.purple
                    
                } else if Theme == 1 {
                    //dark
                    namesWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                    TopSettingsLabel.font = UIFont (name: "Marker Felt", size: 20)!
                    NOWLabel.font = UIFont (name: "Marker Felt", size: 30)!
                    titleLabel.font = UIFont (name: "Marker Felt", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    self.view.backgroundColor = UIColor(red: (105/255.0), green: (105/255.0), blue: (105/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor.black
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    StartWeak.backgroundColor = UIColor(red: (192/255.0), green: (192/255.0), blue: (192/255.0), alpha: 1.0)
                } else if Theme == 2 {
                    //chalk
                    namesWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                    TopSettingsLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    NOWLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    titleLabel.font = UIFont (name: "Chalkduster", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    StartWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.white
                    namesWeak.backgroundColor = UIColor.green
                    TopSettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    titleLabel.textColor = UIColor.white
                } else if Theme == 3 {
                    //anicent
                    namesWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                    TopSettingsLabel.font = UIFont (name: "Papyrus", size: 30)!
                    NOWLabel.font = UIFont (name: "Papyrus", size: 30)!
                    titleLabel.font = UIFont (name: "Papyrus", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Papyrus", size: 25)!
                    self.view.backgroundColor = UIColor(red: (139/255.0), green: (69/255.0), blue: (19/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.white
                    TopSettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    titleLabel.textColor = UIColor.white
                    StartWeak.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                } else if Theme == 4 {
                    //notes
                    namesWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    StartWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                    TopSettingsLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    NOWLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    titleLabel.font = UIFont (name: "Noteworthy", size: 30)!
                    settingsColonLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor(red: (218/255.0), green: (165/255.0), blue: (32/255.0), alpha: 1.0)
                } else if Theme == 5 {
                    //space
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    NOWLabel.textColor = UIColor.white
                    TopSettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    titleLabel.textColor = UIColor.white
                    namesWeak.backgroundColor = UIColor.black
                    StartWeak.backgroundColor = UIColor.black
                } else if Theme == 6 {
                    //ocean
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    NOWLabel.textColor = UIColor.blue
                    TopSettingsLabel.textColor = UIColor.blue
                    settingsColonLabel.textColor = UIColor.blue
                    titleLabel.textColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.blue
                } else if Theme == 7 {
                    //red
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor(red: (128/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    self.view.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (128/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                } else if Theme == 9 {
                    //green
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.green
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (106/255.0), green: (90/255.0), blue: (205/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor.purple
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                    
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.orange
                    StartWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 12 {
                    //pink
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (199/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    StartWeak.backgroundColor = UIColor(red: (238/255.0), green: (130/255.0), blue: (230/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                    NOWLabel.textColor = UIColor.black
                    TopSettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    titleLabel.textColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.yellow
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    StartWeak.backgroundColor = UIColor(red: (184/255.0), green: (134/255.0), blue: (11/255.0), alpha: 1.0)
                }
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if noAds == false {
            if popup.isReady {
                popup.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if noAds == false {
            
            
            // addBannerViewToView(bannerView)
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: SliderWeak.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StartWeak.layer.cornerRadius = 10
        StartWeak.clipsToBounds = true
        namesWeak.layer.cornerRadius = 10
        namesWeak.clipsToBounds = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
       
        Words = []
      
        TopSettingsLabel.adjustsFontSizeToFitWidth = true
        NOWLabel.adjustsFontSizeToFitWidth = true
         titleLabel.adjustsFontSizeToFitWidth = true
        if doingMD == true {
            StartWeak.isHidden = true
            namesWeak.isHidden = true
        }

       
    }
    func normalFont() {
        namesWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        StartWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        TopSettingsLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        NOWLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        titleLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        settingsColonLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
   
    @IBOutlet weak var SliderWeak: UISlider!
    
    @IBOutlet weak var StartWeak: UIButton!
    
    
    @IBOutlet weak var namesWeak: UIButton!
    @IBOutlet weak var TopSettingsLabel: UILabel!
    
    
    @IBOutlet weak var settingsColonLabel: UILabel!
    
    @IBOutlet weak var NOWLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBAction func Slider(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        
        NOWLabel.text = "\(currentValue)"
        
        NOW = Int("\(currentValue)")!
    }
    
    @IBAction func Start(_ sender: UIButton) {
       
       
    }
    
    
    
    
    
    
    
    
    
    
    
}
