//
//  ViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 6/20/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved
//trial gone after math
//


import UIKit
import GoogleMobileAds
var N1 = [Int]()
var N2 = [Int]()
var RandomOp = [Int]()
var NOQ = 2
var SettingCount = 0
var OpCount = 1
var a = "a"
var isMath = false
var CountMath = 0
var ExtraCount = 1
var GoBack = false
 var ChangingOp = false
var RangeMath = 10
var MultiTimer = Timer()
var CFT = 0.0
var P1Time = 1000000.0
var P2Time = 1000000.0
var TotalTime = 1
var HMW = 0.0
//How Many Wrong 
var TrialRun = false
var Player1Name = "Player 1"
var Player2Name = "Player 2"
var ChooseName = false
var SecondPlayer = false
var NOQH = 0
//NOQ Happend
 var popup: GADInterstitial!
class ViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        StartWeak.layer.cornerRadius = 10
        StartWeak.clipsToBounds = true
        ChooseNamesWeak.layer.cornerRadius = 10
        ChooseNamesWeak.clipsToBounds = true
       isMath = true
        GoBackSettingsWeak.isHidden = true
        
        if doingMD == true {
            if StartWeak != nil {
            StartWeak.isHidden = true
                
        }
            if ChooseNamesWeak != nil {
                ChooseNamesWeak.isHidden = true
            }
        }
       
        for _ in 0..<50 {
            RandomOp.append(Int(arc4random_uniform(4 - 1) + 1))
            
        }
          self.navigationController?.setNavigationBarHidden(false, animated: true)
      
        if Slider2Weak != nil {
     Slider2Weak.isHidden = true
        }
        if ChooseOperationWeak != nil {
        ChooseOperationWeak.isHidden = true
        }
        if NOQNumber != nil && NOQTitle != nil && TitleLabel != nil {
        NOQNumber.adjustsFontSizeToFitWidth = true
        NOQTitle.adjustsFontSizeToFitWidth = true
        TitleLabel.adjustsFontSizeToFitWidth = true
        }
    }
    
    
 
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if NOQTitle != nil && NOQNumber != nil && TitleLabel != nil && settingsColonLabel != nil && ChooseNamesWeak != nil && StartWeak != nil {
            NOQTitle.adjustsFontSizeToFitWidth = true
            NOQNumber.adjustsFontSizeToFitWidth = true
            TitleLabel.adjustsFontSizeToFitWidth = true
            settingsColonLabel.adjustsFontSizeToFitWidth = true
            ChooseNamesWeak.titleLabel?.adjustsFontSizeToFitWidth = true
            StartWeak.titleLabel?.adjustsFontSizeToFitWidth = true
            normalFont()
            self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
            Forever.invalidate()
            NOQTitle.textColor = UIColor.black
            NOQNumber.textColor = UIColor.black
            TitleLabel.textColor = UIColor.black
            settingsColonLabel.textColor = UIColor.black
            ChooseNamesWeak.backgroundColor = UIColor.orange
            StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (255/255.0), blue: (255/255.0), alpha: 1.0)
            if hasTheme == true {
                if CustomTheme == true {
                    normalFont()
                    self.view.backgroundColor = PrimaryColor
                    NOQTitle.textColor = TextColor
                    NOQNumber.textColor = TextColor
                    TitleLabel.textColor = TextColor
                    settingsColonLabel.textColor = TextColor
                    ChooseNamesWeak.backgroundColor = SecondaryColor
                    StartWeak.backgroundColor = SecondaryColor
                } else {
                    if Theme == 0 {
                        //default
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        ChooseNamesWeak.backgroundColor = UIColor.orange
                        StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (255/255.0), blue: (255/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 1 {
                        //dark
                        StartWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                        ChooseNamesWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 27)!
                        NOQTitle.font = UIFont (name: "Marker Felt", size: 30)!
                        NOQNumber.font = UIFont (name: "Marker Felt", size: 30)!
                        settingsColonLabel.font = UIFont (name: "Marker Felt", size: 30)!
                        TitleLabel.font = UIFont (name: "Marker Felt", size: 30)!
                        self.view.backgroundColor = UIColor.darkGray
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        ChooseNamesWeak.backgroundColor = UIColor.black
                        StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 2 {
                        //chalk
                        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                        StartWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                        ChooseNamesWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 27)!
                        NOQTitle.font = UIFont (name: "Chalkduster", size: 30)!
                        NOQNumber.font = UIFont (name: "Chalkduster", size: 30)!
                        settingsColonLabel.font = UIFont (name: "Chalkduster", size: 30)!
                        TitleLabel.font = UIFont (name: "Chalkduster", size: 30)!
                        NOQTitle.textColor = UIColor.white
                        NOQNumber.textColor = UIColor.white
                        TitleLabel.textColor = UIColor.white
                        settingsColonLabel.textColor = UIColor.white
                        ChooseNamesWeak.backgroundColor = UIColor.green
                        StartWeak.backgroundColor = UIColor(red: (128/255.0), green: (128/255.0), blue: (0/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.white, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.white, for: .normal)
                    } else if Theme == 3 {
                        //anicent
                        StartWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                        ChooseNamesWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 27)!
                        NOQTitle.font = UIFont (name: "Papyrus", size: 30)!
                        NOQNumber.font = UIFont (name: "Papyrus", size: 30)!
                        settingsColonLabel.font = UIFont (name: "Papyrus", size: 30)!
                        TitleLabel.font = UIFont (name: "Papyrus", size: 30)!
                        self.view.backgroundColor = UIColor(red: (205/255.0), green: (133/255.0), blue: (63/255.0), alpha: 1.0)
                        NOQTitle.textColor = UIColor.white
                        NOQNumber.textColor = UIColor.white
                        TitleLabel.textColor = UIColor.white
                        settingsColonLabel.textColor = UIColor.white
                        ChooseNamesWeak.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (189/255.0), green: (183/255.0), blue: (107/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.brown, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.brown, for: .normal)
                    } else if Theme == 4 {
                        //notes
                        StartWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                        ChooseNamesWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 27)!
                        NOQTitle.font = UIFont (name: "Noteworthy", size: 30)!
                        NOQNumber.font = UIFont (name: "Noteworthy", size: 30)!
                        settingsColonLabel.font = UIFont (name: "Noteworthy", size: 30)!
                        TitleLabel.font = UIFont (name: "Noteworthy", size: 30)!
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                        ChooseNamesWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 5 {
                        //space
                        normalFont()
                        NOQTitle.textColor = UIColor.white
                        NOQNumber.textColor = UIColor.white
                        TitleLabel.textColor = UIColor.white
                        settingsColonLabel.textColor = UIColor.white
                        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                        ChooseNamesWeak.backgroundColor = UIColor.black
                        StartWeak.backgroundColor = UIColor.black
                        SettingsWeak.setTitleColor(UIColor.white, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.white, for: .normal)
                    } else if Theme == 6 {
                        //ocean
                        normalFont()
                        NOQTitle.textColor = UIColor.blue
                        NOQNumber.textColor = UIColor.blue
                        TitleLabel.textColor = UIColor.blue
                        settingsColonLabel.textColor = UIColor.blue
                        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                        ChooseNamesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (139/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.blue, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.blue, for: .normal)
                    } else if Theme == 7 {
                        //red
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (255/255.0), green: (99/255.0), blue: (71/255.0), alpha: 1.0)
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        ChooseNamesWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (145/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 8 {
                        //blue
                        normalFont()
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        
                        self.view.backgroundColor = UIColor(red: (30/255.0), green: (144/255.0), blue: (255/255.0), alpha: 1.0)
                        ChooseNamesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 9 {
                        //green
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (0/255.0), green: (128/255.0), blue: (0/255.0), alpha: 1.0)
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        ChooseNamesWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (50/255.0), green: (205/255.0), blue: (50/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 10 {
                        //purple
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
                        ChooseNamesWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (123/255.0), green: (104/255.0), blue: (238/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 11 {
                        //orange
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (255/255.0), green: (165/255.0), blue: (0/255.0), alpha: 1.0)
                        ChooseNamesWeak.backgroundColor = UIColor.orange
                        StartWeak.backgroundColor = UIColor(red: (0255255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 12 {
                        //pink
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (255/255.0), green: (192/255.0), blue: (203/255.0), alpha: 1.0)
                        ChooseNamesWeak.backgroundColor = UIColor(red: (199/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                        StartWeak.backgroundColor = UIColor(red: (238/255.0), green: (130/255.0), blue: (238/255.0), alpha: 1.0)
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    } else if Theme == 13 {
                        //yellow
                        NOQTitle.textColor = UIColor.black
                        NOQNumber.textColor = UIColor.black
                        TitleLabel.textColor = UIColor.black
                        settingsColonLabel.textColor = UIColor.black
                        
                        StartWeak.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (100/255.0), alpha: 1.0)
                        normalFont()
                        self.view.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                        ChooseNamesWeak.backgroundColor = UIColor.yellow
                        SettingsWeak.setTitleColor(UIColor.black, for: .normal)
                        GoBackSettingsWeak.setTitleColor(UIColor.black, for: .normal)
                    }
                }
                
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if noAds == false {
            if popup.isReady {
                popup.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if noAds == false {
            
            
            
            
            // addBannerViewToView(bannerView)
            
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: SettingsWeak.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
            
            
            
            
            
            
        }
        
    }
    
    
    
    
    
    
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    ///////////////////////////

    @IBOutlet weak var TitleLabel: UILabel!
    
    
    @IBOutlet weak var MathWeak: UIButton!

    
    @IBOutlet weak var HelpWeak: UIButton!
    
    @IBOutlet weak var ChooseNamesWeak: UIButton!
    
    
    
    
    
    @IBOutlet weak var SliderWeak: UISlider!
    
    @IBOutlet weak var Number1: UILabel!
    
    @IBOutlet weak var NOQTitle: UILabel!
    
    @IBOutlet weak var NOQNumber: UILabel!
    
   
    @IBOutlet weak var ChooseOperationWeak: UISegmentedControl!
    
    @IBOutlet weak var Slider2Weak: UISlider!
    
    
    @IBOutlet weak var settingsColonLabel: UILabel!
    
    
   
    @IBOutlet weak var StartWeak: UIButton!
    
    
    
    @IBOutlet weak var SettingsWeak: UIButton!
    
    @IBOutlet weak var GoBackSettingsWeak: UIButton!
    
   
    
    func normalFont() {
        if StartWeak != nil && ChooseNamesWeak != nil && NOQTitle != nil && NOQNumber != nil && settingsColonLabel != nil && TitleLabel != nil{
        StartWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        ChooseNamesWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 27)!
        
        NOQTitle.font = UIFont (name: "Helvetica Neue", size: 30)!
         NOQNumber.font = UIFont (name: "Helvetica Neue", size: 30)!
         settingsColonLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
         TitleLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        }
    }
    
    
    @IBAction func StartMath(_ sender: UIButton) {
        N1.removeAll()
        N2.removeAll()
        Append()
        
    }
    
    
    
    
    
   
   
  
    func TitleColorsOne() {
        TitleLabel.textColor = P1Color
        
        TitleLabel.text = "\(Player1Name) in"
        if P1isShadow == true {
        TitleLabel.layer.shadowColor = P1Color.cgColor
        TitleLabel.layer.shadowOffset = CGSize(5,0)
        TitleLabel.layer.shadowRadius = 2
        TitleLabel.layer.shadowOpacity = 0.25
        }
    }
    func TitleColorsTwo() {
        TitleLabel.textColor = P2Color
        
        TitleLabel.text = "\(Player2Name) in"
        if P2isShadow == true {
        TitleLabel.layer.shadowColor = P2Color.cgColor
        TitleLabel.layer.shadowOffset = CGSize(5,0)
        TitleLabel.layer.shadowRadius = 2
        TitleLabel.layer.shadowOpacity = 0.25
        }
    }
    func Append() {
        for _ in 0..<50 {
            N1.append(Int(arc4random_uniform(UInt32(RangeMath))))
            N2.append(Int(arc4random_uniform(UInt32(RangeMath))))
        }
    }
   
    @IBAction func Slider(_ sender: UISlider) {
       
        
            let currentValue = Int(sender.value)
            
            
               NOQNumber.text = "\(currentValue)"
            
            NOQ = Int("\(currentValue)")!
     
        }

    @IBAction func Settings(_ sender: UIButton) {
        if SettingCount == 0{
            SliderWeak.isHidden = true
            Slider2Weak.isHidden = false
            NOQTitle.text = "Range"
            NOQNumber.text = "0 - \(RangeMath)"
            SettingCount = 1
       GoBackSettingsWeak.isHidden = false
            
            
        } else if SettingCount == 1 {
            Slider2Weak.isHidden = true
            ChooseOperationWeak.isHidden = false
            NOQTitle.text = "Choose Operation"
            NOQNumber.text = "-"
            SettingsWeak.isHidden = true
            SettingCount = 2
        }
        
    }
        
        
    @IBAction func Slider2(_ sender: UISlider) {
        
        let currentValue = Int(sender.value)
        
        NOQNumber.text = "Range: 0 - \(currentValue)"
        RangeMath = Int("\(currentValue)")!
        
    }
    
    @IBAction func ChooseOperation(_ sender: UISegmentedControl) {
       
        if (ChooseOperationWeak.selectedSegmentIndex == 0) {
            OpCount = 1
            OPFAV = "X"
            ChangingOp = false
        } else if (ChooseOperationWeak.selectedSegmentIndex == 1 ) {
            OpCount = 2
            OPFAV = "+"
            ChangingOp = false
        } else if (ChooseOperationWeak.selectedSegmentIndex == 2 ) {
            OpCount = 3
            OPFAV = "x"
            ChangingOp = false
        } else if (ChooseOperationWeak.selectedSegmentIndex == 3) {
            OpCount = (Int(arc4random_uniform(3 - 1) + 1))
           OPFAV = "Random"
            ChangingOp = false
        } else if (ChooseOperationWeak.selectedSegmentIndex == 4) {
            OPFAV = "Changing"
            ChangingOp = true
            
            for _ in 0..<50 {
                RandomOp.append(Int(arc4random_uniform(3) + 1))
                
            }
        }
        
        
    }
    
    
    
    @IBAction func GoBackSettings(_ sender: UIButton) {
        if SettingCount == 1{
            Slider2Weak.isHidden = true
            SliderWeak.isHidden = false
            NOQTitle.text = "Number of Questions"
            NOQNumber.text = "\(NOQ)"
            GoBackSettingsWeak.isHidden = true
            SettingCount = 0
        } else if SettingCount == 2 {
            ChooseOperationWeak.isHidden = true
            Slider2Weak.isHidden = false
            NOQTitle.text = "Range"
            NOQNumber.text = "0 - \(RangeMath)"
            SettingsWeak.isHidden = false
            SettingCount = 1
            
        }
            
        }
  
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}




    
    
    

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    

        
        

    
    

    
    
    
    
    
    
    



