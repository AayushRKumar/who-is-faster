//
//  CopyWordViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 9/22/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileAds
var NOWH = 0

var sendDoneMessage:MDPlayerItem!


class CopyWordViewController: UIViewController, GADInterstitialDelegate {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print(P1Color)
        TFWeak.enablesReturnKeyAutomatically = true
        
        SecondPlayerWeak.layer.cornerRadius = 10
        SecondPlayerWeak.clipsToBounds = true
        if doingMD == false {
        NumbersForWords.removeAll()
        CWAppend()
        WordAppend()
        }
        CFT = 0
        P1Time = 10000.0
        P2Time = 100000.0
        P3Time = 100000.0
        P4Time = 100000.0
        P5Time = 10000.0
        P6Time = 10000.0
        P7Time = 100000.0
        P8Time = 10000.0
        P9Time = 1000.0
        P10Time = 10000.0
       NOPH = 0
        NOWH = 0
        MultiTimeLabel.adjustsFontSizeToFitWidth = true
        P1TimeLabel.adjustsFontSizeToFitWidth = true
        P2TimeLabel.adjustsFontSizeToFitWidth = true
        Player.adjustsFontSizeToFitWidth = true
        RWLabel.adjustsFontSizeToFitWidth = true
        timeLabel.adjustsFontSizeToFitWidth = true
        CDNameLabel.adjustsFontSizeToFitWidth = true
        CDCountLabel.adjustsFontSizeToFitWidth = true
        SecondPlayerWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        WordLabel.adjustsFontSizeToFitWidth = true
        TFWeak.adjustsFontSizeToFitWidth = true
        
        
        
        if hasTheme == true || hasBundle == true {
            if CustomTheme == true {
                
                CWNormalFont()
                self.view.backgroundColor = PrimaryColor
                SecondPlayerWeak.setTitleColor(SecondaryColor, for: .normal)
                TFWeak.backgroundColor = SecondaryColor
                MultiTimeLabel.textColor = TextColor
                P1TimeLabel.textColor = TextColor
                P2TimeLabel.textColor = TextColor
                Player.textColor = TextColor
                RWLabel.textColor = TextColor
                timeLabel.textColor = TextColor
                CDNameLabel.textColor = TextColor
                CDCountLabel.textColor = TextColor
                SecondPlayerWeak.setTitleColor(TextColor, for: .normal)
                WordLabel.textColor = TextColor
                TFWeak.textColor = TextColor
                TFWeak.backgroundColor = SecondaryColor
                TFWeak.textColor = UIColor.black
            } else {
            if Theme == 0 {
               CWNormalColor()
               CWNormalFont()
                view.backgroundColor = UIColor.white
                SecondPlayerWeak.backgroundColor = UIColor.green
                TFWeak.textColor = UIColor.black
            } else if Theme == 1 {
                MultiTimeLabel.font = UIFont (name: "Marker Felt", size: 18)
                P1TimeLabel.font = UIFont (name: "Marker Felt", size: 22)
                P2TimeLabel.font = UIFont (name: "Marker Felt", size: 22)
                Player.font = UIFont (name: "Marker Felt", size: 36)
                RWLabel.font = UIFont (name: "Marker Felt", size: 33)
                CDNameLabel.font = UIFont (name: "Marker Felt", size: 24)
                CDCountLabel.font = UIFont (name: "Marker Felt", size: 24)
                timeLabel.font = UIFont (name: "Marker Felt", size: 28)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 30)
                WordLabel.font = UIFont (name: "Marker Felt", size: 49)
                TFWeak.font = UIFont (name: "Marker Felt", size: 19)
                TFWeak.backgroundColor = UIColor.lightGray
                view.backgroundColor = UIColor.darkGray
                SecondPlayerWeak.backgroundColor = UIColor.gray
                TFWeak.textColor = UIColor.black
                CWNormalColor()
            } else if Theme == 2 {
                //Chalk Board
                MultiTimeLabel.font = UIFont (name: "Chalkduster", size: 18)
                P1TimeLabel.font = UIFont (name: "Chalkduster", size: 22)
                P2TimeLabel.font = UIFont (name: "Chalkduster", size: 22)
                Player.font = UIFont (name: "Chalkduster", size: 36)
                RWLabel.font = UIFont (name: "Chalkduster", size: 33)
                timeLabel.font = UIFont (name: "Chalkduster", size: 28)
                CDNameLabel.font = UIFont (name: "Chalkduster", size: 24)
                CDCountLabel.font = UIFont (name: "Chalkduster", size: 24)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 30)
                WordLabel.font = UIFont (name: "Chalkduster", size: 49)
                TFWeak.font = UIFont (name: "Chalkduster", size: 19)
                TFWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
                 self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                SecondPlayerWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
               CWWhiteColor()
                TFWeak.textColor = UIColor.black
            } else if Theme == 3 {
                //ANICENT
                TFWeak.textColor = UIColor.black
                CWNormalColor()
                MultiTimeLabel.font = UIFont (name: "Papyrus", size: 18)
                P1TimeLabel.font = UIFont (name: "Papyrus", size: 22)
                P2TimeLabel.font = UIFont (name: "Papyrus", size: 22)
                Player.font = UIFont (name: "Papyrus", size: 36)
                RWLabel.font = UIFont (name: "Papyrus", size: 33)
                timeLabel.font = UIFont (name: "Papyrus", size: 28)
                CDNameLabel.font = UIFont (name: "Papyrus", size: 24)
                CDCountLabel.font = UIFont (name: "Papyrus", size: 24)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 30)
                WordLabel.font = UIFont (name: "Papyrus", size: 49)
                TFWeak.font = UIFont (name: "Papyrus", size: 19)
                 TFWeak.backgroundColor = UIColor(red: (126/255.0), green: (76/255.0), blue: (18/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.brown
                SecondPlayerWeak.backgroundColor = UIColor.white
                
            } else if Theme == 4 {
                //NOTES
                TFWeak.textColor = UIColor.black
                CWNormalColor()
                MultiTimeLabel.font = UIFont (name: "Noteworthy", size: 18)
                P1TimeLabel.font = UIFont (name: "Noteworthy", size: 22)
                P2TimeLabel.font = UIFont (name: "Noteworthy", size: 22)
                Player.font = UIFont (name: "Noteworthy", size: 36)
                RWLabel.font = UIFont (name: "Noteworthy", size: 33)
                timeLabel.font = UIFont (name: "Noteworthy", size: 28)
                CDNameLabel.font = UIFont (name: "Noteworthy", size: 24)
                CDCountLabel.font = UIFont (name: "Noteworthy", size: 24)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 30)
                WordLabel.font = UIFont (name: "Noteworthy", size: 49)
                TFWeak.font = UIFont (name: "Noteworthy", size: 19)
                TFWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)

                SecondPlayerWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
            } else if Theme == 5 {
                CWWhiteColor()
                CWNormalFont()
                TFWeak.textColor = UIColor.white
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                TFWeak.textColor = UIColor.white
                TFWeak.backgroundColor = UIColor.black
                SecondPlayerWeak.backgroundColor = UIColor.white
            } else if Theme == 6 {
                CWWhiteColor()
                CWNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                TFWeak.backgroundColor = UIColor.white
                TFWeak.backgroundColor = UIColor.blue
                SecondPlayerWeak.backgroundColor = UIColor.white
                 TFWeak.textColor = UIColor.white
            }  else if Theme == 7 {
                CWNormalFont()
                CWNormalColor()
                TFWeak.backgroundColor = UIColor.red
                view.backgroundColor = UIColor.red
                SecondPlayerWeak.backgroundColor = UIColor(red: (149/255.0), green: (32/255.0), blue: (38/255.0), alpha: 1.0)
                TFWeak.textColor = UIColor.black
            } else if Theme == 8 {
                CWNormalFont()
                TFWeak.textColor = UIColor.black
               CWNormalColor()
                view.backgroundColor = UIColor.blue
                TFWeak.backgroundColor = UIColor.blue
                SecondPlayerWeak.backgroundColor = UIColor(red: (28/255.0), green: (88/255.0), blue: (190/255.0), alpha: 1.0)
            } else if Theme == 9 {
                CWNormalFont()
                view.backgroundColor = UIColor(red: (28/255.0), green: (255/255.0), blue: (98/255.0), alpha: 1.0)
                TFWeak.textColor = UIColor.black
                    CWNormalColor()
                TFWeak.backgroundColor = UIColor.green
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (255/255.0), blue: (152/255.0), alpha: 1.0)
            } else if Theme == 10 {
                CWNormalFont()
                CWNormalColor()
                TFWeak.textColor = UIColor.black
                view.backgroundColor = UIColor.purple
                TFWeak.backgroundColor = UIColor.purple
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (0/255.0), blue: (239/255.0), alpha: 1.0)
            } else if Theme == 11 {
                CWNormalFont()
                CWNormalColor()
                TFWeak.textColor = UIColor.black
                view.backgroundColor = UIColor.orange
                TFWeak.backgroundColor = UIColor.orange
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (117/255.0), blue: (0/255.0), alpha: 1.0)
            } else if Theme == 12 {
                CWNormalFont()
                CWNormalColor()
                TFWeak.textColor = UIColor.black
                view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                TFWeak.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
            } else if Theme == 13 {
                CWNormalFont()
                CWNormalColor()
                TFWeak.textColor = UIColor.black
                TFWeak.backgroundColor = UIColor.yellow
                view.backgroundColor = UIColor.yellow
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (231/255.0), blue: (61/255.0), alpha: 1.0)
            }
            
            }
            
        }
        
        let FileLocation = Bundle.main.path(forResource: "Correct-answer", ofType: ".mp4")
        
        do {
            CorrectSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: FileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
        let WrongFileLocation = Bundle.main.path(forResource: "Wrong-answer", ofType: ".mp4")
        
        do {
            WrongSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: WrongFileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }

       
        
        MultiTimeLabel.isHidden = true
  self.navigationController?.setNavigationBarHidden(true, animated: true)
        TFWeak.autocorrectionType = .no
        TFWeak.textColor = P1Color
      WordLabel.text = Words[NumbersForWords[CWCount]]
       self.addDoneButtonOnKeyboard()
        P1TimeLabel.isHidden = true
        RWLabel.text = ""
        P2TimeLabel.isHidden = true
        SecondPlayer = false
        P1TimeLabel.textColor = P1Color
        if P1isShadow == true {
            P1TimeLabel.layer.shadowColor = P1Color.cgColor
            P1TimeLabel.layer.shadowOffset = CGSize(5,0)
            P1TimeLabel.layer.shadowRadius = 2
            P1TimeLabel.layer.shadowOpacity = 0.25
            
        } else {
            P1TimeLabel.layer.shadowColor = P1Color.cgColor
            P1TimeLabel.layer.shadowOffset = CGSize(0,0)
            P1TimeLabel.layer.shadowRadius = 0
            P1TimeLabel.layer.shadowOpacity = 0.0
            
        }
        P2TimeLabel.textColor = P2Color
        if P2isShadow == true {
            P2TimeLabel.layer.shadowColor = P2Color.cgColor
            P2TimeLabel.layer.shadowOffset = CGSize(5,0)
            P2TimeLabel.layer.shadowRadius = 2
            P2TimeLabel.layer.shadowOpacity = 0.25
            
        } else {
            P2TimeLabel.layer.shadowColor = P2Color.cgColor
            P2TimeLabel.layer.shadowOffset = CGSize(0,0)
            P2TimeLabel.layer.shadowRadius = 0
            P2TimeLabel.layer.shadowOpacity = 0.0
        }
        SecondPlayer = false
        TFWeak.textColor = P1Color
        if doingMD == true && isHost == true {
            Player.text = hostName
        } else {
        Player.text = Player1Name
        }
        Player.textColor = P1Color
        if P1isShadow == true {
            Player.layer.shadowColor = P1Color.cgColor
            Player.layer.shadowOffset = CGSize(5,0)
            Player.layer.shadowRadius = 2
            Player.layer.shadowOpacity = 0.25
            
        } else {
            Player.layer.shadowColor = P1Color.cgColor
            Player.layer.shadowOffset = CGSize(0,0)
            Player.layer.shadowRadius = 0
            Player.layer.shadowOpacity = 0.0
        }
        
        CountDownCount = 6
        CDCountLabel.text = "Loading"
        SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
        
        SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
        if P2isShadow == true {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
            SecondPlayerWeak.layer.shadowRadius = 2
            SecondPlayerWeak.layer.shadowOpacity = 0.25
        } else {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
            SecondPlayerWeak.layer.shadowRadius = 0
            SecondPlayerWeak.layer.shadowOpacity = 0.0
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if doingMD == true && isHost == true {
            CDNameLabel.text = "\(hostName) in"
        } else {
        CDNameLabel.text = "\(Player1Name) in"
        }
        CDNameLabel.textColor = P1Color
        if P1isShadow == true {
            CDNameLabel.layer.shadowColor = P1Color.cgColor
            CDNameLabel.layer.shadowOffset = CGSize(5,0)
            CDNameLabel.layer.shadowRadius = 2
            CDNameLabel.layer.shadowOpacity = 0.25
            
        } else {
            CDNameLabel.layer.shadowColor = P1Color.cgColor
            CDNameLabel.layer.shadowOffset = CGSize(0,0)
            CDNameLabel.layer.shadowRadius = 0
            CDNameLabel.layer.shadowOpacity = 0.0
        }
        
        CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CWCountDownFunc), userInfo: nil, repeats: true)
        
        
        
       
      

        
        
        RWLabel.isHidden = true
        P1TimeLabel.isHidden = true
        P2TimeLabel.isHidden = true
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
        Player.isHidden = true
        SecondPlayerWeak.isHidden = true
        WordLabel.isHidden = true
        TFWeak.isHidden = true
        timeLabel.isHidden = true
        
        
        NOPH = 1
        
        
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        
        popup = GADInterstitial(adUnitID: "ca-app-pub-2312248651171588/1230141252")
        popup.delegate = self
        popup.load(GADRequest())
        
        return popup
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goMake"{
            
            _ = segue.destination as? MakeGameViewController
               
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var CDNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var ExitWeak: UIButton!
    @IBOutlet weak var PauseWeak: UIButton!
    @IBOutlet weak var Player: UILabel!
    @IBOutlet weak var P1TimeLabel: UILabel!
    @IBOutlet weak var P2TimeLabel: UILabel!
    @IBOutlet weak var CDCountLabel: UILabel!
    @IBOutlet weak var RWLabel: UILabel!
    
    @IBOutlet weak var MultiTimeLabel: UILabel!
    @IBOutlet weak var WordLabel: UILabel!
    @IBOutlet weak var TFWeak: UITextField!
    @IBOutlet weak var SecondPlayerWeak: UIButton!
    
    
    
    
    
    
    func WordAppend() {
        for _ in 0..<100 {
            Words.append(randomWord())
        }
        print(Words)
    }
    func CWAppend() {
        for _ in 0..<50 {
            NumbersForWords.append(Int(arc4random_uniform(UInt32(100))))
        }
    }
    func randomWord(wordLength: Int = 6) -> String {
        
        let kCons = 1
        let kVows = 2
        
        var cons: [String] = [
            // single consonants. Beware of Q, it"s often awkward in words
            "b", "c", "d", "f", "g", "h", "j", "k", "l", "m",
            "n", "p", "r", "s", "t", "v", "w", "x", "z",
            // possible combinations excluding those which cannot start a word
            "pt", "gl", "gr", "ch", "ph", "ps", "sh", "st", "th", "wh"
        ]
        
        // consonant combinations that cannot start a word
        let cons_cant_start: [String] = [
            "ck", "cm",
            "dr", "ds",
            "ft",
            "gh", "gn",
            "kr", "ks",
            "ls", "lt", "lr",
            "mp", "mt", "ms",
            "ng", "ns",
            "rd", "rg", "rs", "rt",
            "ss",
            "ts", "tch"
        ]
        
        var vows : [String] = [
            // single vowels
            "a", "e", "i", "o", "u", "y",
            // vowel combinations your language allows
            "ee", "oa", "oo",
            ]
        
        // start by vowel or consonant ?
        var current = (Int(arc4random_uniform(2)) == 1 ? kCons : kVows );
        
        var word : String = ""
        while ( word.count < wordLength ){
            // After first letter, use all consonant combos
            if word.count == 2 {
                cons = cons + cons_cant_start
            }
            
            // random sign from either $cons or $vows
            var rnd: String = "";
            var index: Int;
            if current == kCons {
                index = Int(arc4random_uniform(UInt32(cons.count)))
                rnd = cons[index]
            }else if current == kVows {
                index = Int(arc4random_uniform(UInt32(vows.count)))
                rnd = vows[index]
            }
            
            
            // check if random sign fits in word length
        //    var tempWord = "\(word)\(rnd)"
            if( word.count <= wordLength ) {
                word = "\(word)\(rnd)"
                // alternate sounds
                current = ( current == kCons ) ? kVows : kCons;
            }
            
            
            //PLZ WORLD PLZZ
        }
        
        
        
        return word
    }
    
    
    
    
    
    func CWNormalFont() {
        MultiTimeLabel.font = UIFont (name: "Helvetica Neue", size: 18)
        P1TimeLabel.font = UIFont (name: "Helvetica Neue", size: 22)
        P2TimeLabel.font = UIFont (name: "Helvetica Neue", size: 22)
        Player.font = UIFont (name: "Helvetica Neue", size: 36)
        RWLabel.font = UIFont (name: "Helvetica Neue", size: 33)
        timeLabel.font = UIFont (name: "Helvetica Neue", size: 28)
        CDNameLabel.font = UIFont (name: "Helvetica Neue", size: 24)
        CDCountLabel.font = UIFont (name: "Helvetica Neue", size: 24)
        SecondPlayerWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 30)
        WordLabel.font = UIFont (name: "Helvetica Neue", size: 49)
        TFWeak.font = UIFont (name: "Helvetica Neue", size: 19)
    }
    func CWNormalColor() {
        MultiTimeLabel.textColor = UIColor.black
        P1TimeLabel.textColor = UIColor.black
        P2TimeLabel.textColor = UIColor.black
        Player.textColor = UIColor.black
        RWLabel.textColor = UIColor.black
        timeLabel.textColor = UIColor.black
        CDNameLabel.textColor = UIColor.black
        CDCountLabel.textColor = UIColor.black
        SecondPlayerWeak.setTitleColor(UIColor.black, for: .normal)
        WordLabel.textColor = UIColor.black
        TFWeak.textColor = UIColor.black

    }
    func CWWhiteColor() {
        MultiTimeLabel.textColor = UIColor.white
        P1TimeLabel.textColor = UIColor.white
        P2TimeLabel.textColor = UIColor.white
        Player.textColor = UIColor.white
        RWLabel.textColor = UIColor.white
        timeLabel.textColor = UIColor.white
        CDNameLabel.textColor = UIColor.white
        CDCountLabel.textColor = UIColor.white
        Player.textColor = UIColor.white
        SecondPlayerWeak.setTitleColor(UIColor.white, for: .normal)
        WordLabel.textColor = UIColor.white
        TFWeak.textColor = UIColor.white
        Player.textColor = UIColor.white
    }
    func MultiPlayerHide() {
        
        CFT = (CFT * 100).rounded() / 100
        MultiTimer.invalidate()
        TFWeak.isHidden = true
        Player.isHidden = true
        SecondPlayerWeak.isHidden = false
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
    }
    
    @objc func CWUpdateCounter() {
        CFT = CFT + 0.1
        timeLabel.text = String(format: "%.1f", CFT)
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if self.TFWeak != nil {
            self.TFWeak.inputAccessoryView = doneToolbar
        }
    }
    @objc func CWCountDownFunc() {
        if hasTenPlayerPack == true || hasBundle == true  {
            if CountDownCount == 1 {
                
               
                RWLabel.isHidden = false
                CDCountLabel.isHidden = true
                CDNameLabel.isHidden = true
                timeLabel.isHidden = false
                
                TFWeak.isHidden = false
                MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CWUpdateCounter), userInfo: nil, repeats: true)
                
                CountDownTimer.invalidate()
                self.TFWeak.becomeFirstResponder()
                
                WordLabel.isHidden = false
                Player.isHidden = false
                if doingMD == true {
                PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                } else if doingMD == false {
                    PauseWeak.isHidden = false
                    ExitWeak.isHidden = false
                }
                
                if NOPH == 1 {
                    if doingMD == true && isHost == true {
                        Player.text = hostName
                    } else {
                    Player.text = "\(Player1Name)"
                    }
                    Player.textColor = P1Color
                    if P1isShadow == true {
                        Player.layer.shadowColor = P1Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P1Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    Player.text = "\(Player2Name)"
                    Player.textColor = P2Color
                    if P2isShadow == true {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 3 {
                    Player.text = "\(Player3Name)"
                    Player.textColor = P3Color
                    if P3isShadow == true {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    Player.text = "\(Player4Name)"
                    Player.textColor = P4Color
                    if P4isShadow == true {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    Player.text = "\(Player5Name)"
                    Player.textColor = P5Color
                    if P5isShadow == true {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    Player.text = "\(Player6Name)"
                    Player.textColor = P6Color
                    if P6isShadow == true {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    Player.text = "\(Player7Name)"
                    Player.textColor = P7Color
                    if P7isShadow == true {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    Player.text = "\(Player8Name)"
                    Player.textColor = P8Color
                    if P8isShadow == true {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    Player.text = "\(Player9Name)"
                    Player.textColor = P9Color
                    if P9isShadow == true {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 10 {
                    Player.text = "\(Player10Name)"
                    Player.textColor = P10Color
                    if P10isShadow == true {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                }
                CDCountLabel.text = "0"
            
                
            } else {
                CountDownCount += -1
                CDCountLabel.text = String(CountDownCount)
            }
            
            
        } else if hasTenPlayerPack == false {
        if CountDownCount == 1 {
            CountDownTimer.invalidate()
            RWLabel.isHidden = false
           
            RWLabel.text = ""
            CDCountLabel.isHidden = true
            CDNameLabel.isHidden = true
            CountDownCount = 6
            WordLabel.isHidden = false
            Player.isHidden = false
            if doingMD == false {
                PauseWeak.isHidden = false
                
                ExitWeak.isHidden = false
            } else if doingMD == true {
                PauseWeak.isHidden = true
                ExitWeak.isHidden = true
            }
            
            self.TFWeak.becomeFirstResponder()
            TFWeak.isHidden = false
            timeLabel.isHidden = false
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CWUpdateCounter), userInfo: nil, repeats: true)
        } else {
            CountDownCount += -1
            CDCountLabel.text = String(CountDownCount)
        }
        }
    }
    func Check() {
        if hasTenPlayerPack == true {
            if NOP == NOPH && NOW == CWCount {
                
                TFWeak.isHidden = true
                TFWeak.resignFirstResponder()
                CFT = (CFT * 100).rounded() / 100
                if NOP == 10 {
                    P10Time = CFT + HMW
                } else if NOP == 9 {
                    P9Time = CFT + HMW
                } else if NOP == 8 {
                    P8Time = CFT + HMW
                } else if NOP == 7 {
                    P7Time = CFT + HMW
                } else if NOP == 6 {
                    P6Time = CFT + HMW
                } else if NOP == 5 {
                    P5Time = CFT + HMW
                } else if NOP == 4 {
                    P4Time = CFT + HMW
                } else if NOP == 3 {
                    P3Time = CFT + HMW
                } else if NOP == 2 {
                    P2Time = CFT + HMW
                }
                
                MultiTimeLabel.isHidden = false
                
                if NOP == 2 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time)"
                } else if NOP == 3 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time)"
                } else if NOP == 4 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time)"
                } else if NOP == 5 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time)"
                } else if NOP == 6 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time)"
                } else if NOP == 7 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time)"
                } else if NOP == 8 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time)"
                } else if NOP == 9 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time)"
                } else if NOP == 10 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time) | \(Player10Name)'s time was \(P10Time)"
                }
                
               MultiTimer.invalidate()
                PauseWeak.isHidden = true
                RWLabel.isHidden = true
                timeLabel.isHidden = true
                WordLabel.isHidden = false
                 scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
                let min = scores.min()
                popup = createAndLoadInterstitial()

                if min == P1Time {
                    WordLabel.textColor = P1Color
                    if P1isShadow == true {
                        WordLabel.layer.shadowColor = P1Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P1Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player1Name) wins!"
                    
                    
                } else if min == P2Time {
                    WordLabel.textColor = P2Color
                    if P2isShadow == true {
                        WordLabel.layer.shadowColor = P2Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P2Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player2Name) wins!"
                    
                } else if min == P3Time {
                    WordLabel.textColor = P3Color
                    if P3isShadow == true {
                        WordLabel.layer.shadowColor = P3Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P3Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player3Name) wins!"
                    
                } else if min == P4Time {
                    WordLabel.textColor = P4Color
                    if P4isShadow == true {
                        WordLabel.layer.shadowColor = P4Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P4Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player4Name) wins!"
                    
                } else if min == P5Time {
                    WordLabel.textColor = P5Color
                    if P5isShadow == true {
                        WordLabel.layer.shadowColor = P5Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P5Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player5Name) wins!"
                } else if min == P6Time {
                    WordLabel.textColor = P6Color
                    if P6isShadow == true {
                        WordLabel.layer.shadowColor = P6Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P6Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player6Name) wins!"
                }  else if min == P7Time {
                    WordLabel.textColor = P7Color
                    if P7isShadow == true {
                        WordLabel.layer.shadowColor = P7Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P7Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player7Name) wins!"
                }  else if min == P8Time {
                    WordLabel.textColor = P8Color
                    if P8isShadow == true {
                        WordLabel.layer.shadowColor = P8Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P8Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player8Name) wins!"
                }  else if min == P9Time {
                    WordLabel.textColor = P9Color
                    if P9isShadow == true {
                        WordLabel.layer.shadowColor = P9Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P9Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player9Name) wins!"
                }  else if min == P10Time {
                    WordLabel.textColor = P10Color
                    if P10isShadow == true {
                        WordLabel.layer.shadowColor = P10Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(5,0)
                        WordLabel.layer.shadowRadius = 2
                        WordLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        WordLabel.layer.shadowColor = P10Color.cgColor
                        WordLabel.layer.shadowOffset = CGSize(0,0)
                        WordLabel.layer.shadowRadius = 0
                        WordLabel.layer.shadowOpacity = 0.0
                    }
                    WordLabel.text = "\(Player10Name) wins!"
                    
                }
                CWCount = 0
                if isTournament == false {
                SecondPlayerWeak.setTitle("Rematch", for: .normal)
                } else {
                    SecondPlayerWeak.isHidden = true
                }
                ExitWeak.isHidden = true
                
                WordLabel.isHidden = false
                SecondPlayerWeak.isHidden = false
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                
                
                
            } else if NOW == CWCount {
                CFT = (CFT * 100).rounded() / 100
                MultiTimer.invalidate()
                if doingMD == true {
                    P1Time = CFT + HMW
                   
                    SeeingTimes = true
                   if isHost == true {
                       self.performSegue(withIdentifier: "goMake", sender: self)
                   } else {
                       self.performSegue(withIdentifier: "goInvite", sender: self)
                    }
                  
                }
                TFWeak.resignFirstResponder()
                WordLabel.isHidden = true
                CWCount = 0
                RWLabel.isHidden = true
                PauseWeak.isHidden = true
                TFWeak.isHidden = true
                Player.isHidden = true
                ExitWeak.isHidden = true
                if NOPH == 1 {
                    P1Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
                    CDNameLabel.text = "\(Player2Name) in"
                    CDNameLabel.textColor = P2Color
                    if P2isShadow == true {
                        CDNameLabel.layer.shadowColor = P2Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(5,0)
                        CDNameLabel.layer.shadowRadius = 2
                        CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P2Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    P2Time = CFT + HMW
                    MultiPlayerHide()
                    
                    SecondPlayerWeak.setTitle("\(Player3Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P3Color, for: .normal)
                    CDNameLabel.text = "\(Player3Name) in"
                    
                   
                     CDNameLabel.textColor = P3Color
                    if P3isShadow == true {
                         CDNameLabel.layer.shadowColor = P3Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P3Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 3 {
                    P3Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player4Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P4Color, for: .normal)
                     CDNameLabel.text = "\(Player4Name) in"
                     CDNameLabel.textColor = P4Color
                    if P4isShadow == true {
                         CDNameLabel.layer.shadowColor = P4Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P4Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    P4Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player5Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P5Color, for: .normal)
                     CDNameLabel.text = "\(Player5Name) in"
                     CDNameLabel.textColor = P5Color
                    if P5isShadow == true {
                         CDNameLabel.layer.shadowColor = P5Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P5Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    P5Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player6Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P6Color, for: .normal)
                     CDNameLabel.text = "\(Player6Name) in"
                     CDNameLabel.textColor = P6Color
                    if P6isShadow == true {
                         CDNameLabel.layer.shadowColor = P6Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P6Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    P6Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player7Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P7Color, for: .normal)
                     CDNameLabel.text = "\(Player7Name) in"
                     CDNameLabel.textColor = P7Color
                    if P7isShadow == true {
                         CDNameLabel.layer.shadowColor = P7Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P7Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    P7Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player8Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P8Color, for: .normal)
                     CDNameLabel.text = "\(Player8Name) in"
                     CDNameLabel.textColor = P8Color
                    if P8isShadow == true {
                         CDNameLabel.layer.shadowColor = P8Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P8Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    P8Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player9Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P9Color, for: .normal)
                     CDNameLabel.text = "\(Player9Name) in"
                     CDNameLabel.textColor = P9Color
                    if P9isShadow == true {
                         CDNameLabel.layer.shadowColor = P9Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P9Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    P9Time = CFT + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player10Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P10Color, for: .normal)
                     CDNameLabel.text = "\(Player10Name) in"
                     CDNameLabel.textColor = P10Color
                    if P10isShadow == true {
                         CDNameLabel.layer.shadowColor = P10Color.cgColor
                         CDNameLabel.layer.shadowOffset = CGSize(5,0)
                         CDNameLabel.layer.shadowRadius = 2
                         CDNameLabel.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDNameLabel.layer.shadowColor = P10Color.cgColor
                        CDNameLabel.layer.shadowOffset = CGSize(0,0)
                        CDNameLabel.layer.shadowRadius = 0
                        CDNameLabel.layer.shadowOpacity = 0.0
                    }
                }
                
                
                
                SecondPlayerWeak.isHidden = false
                
                CountDownCount = 6
              NOWH = 0
                
               
                  NOPH += 1
                CFT = 0
            }
        } else if hasTenPlayerPack == false {
        if CWCount == NOW {
            if SecondPlayer ==  true {
                
                CFT = (CFT * 100).rounded() / 100
                P2Time = CFT + HMW
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                PauseWeak.isHidden = true
                ExitWeak.isHidden = true
               
                
                MultiTimer.invalidate()
                TFWeak.isHidden = true
                Player.isHidden = true
                timeLabel.isHidden = true
                P1TimeLabel.isHidden = false
                SecondPlayerWeak.isHidden = false
                P2TimeLabel.isHidden = false
                    WordLabel.isHidden = false
                CFT = 0
                NOWH = 0
                RWLabel.isHidden = true
                if isTournament == false {
                    SecondPlayerWeak.setTitle("Rematch", for: .normal)
                } else {
                    SecondPlayerWeak.isHidden = true
                }
                NOWH = 0
                popup = createAndLoadInterstitial()

                TFWeak.resignFirstResponder()
                P1TimeLabel.text = "\(Player1Name)'s time was \(P1Time)"
                P2TimeLabel.text = "\(Player2Name)'s time was \(P2Time)"
                print(P1Time)
                
                WordLabel.isHidden = false
                if P1Time > P2Time {
                    print("P2 wins")
                    WordLabel.text = "\(Player2Name) wins!"
                   
                } else if P1Time < P2Time {
                     print("P1 wins")
                     WordLabel.text = "\(Player1Name) wins!"
                
                } else if P1Time == P2Time {
                     WordLabel.text = "Tie!"
                }
               
                CWCount = 0
                
            } else if SecondPlayer == false {
                
          CFT = (CFT * 100).rounded() / 100
                if doingMD == true {
                    P1Time = CFT + HMW
                    
                    SeeingTimes = true
                   if isHost == true {
                       self.performSegue(withIdentifier: "goMake", sender: self)
                   } else {
                       self.performSegue(withIdentifier: "goInvite", sender: self)
                   }
                    
                }
                
                
                
                
           MultiTimer.invalidate()
            SecondPlayerWeak.isHidden = false
            PauseWeak.isHidden = true
            ExitWeak.isHidden = true
            TFWeak.isHidden = true
            WordLabel.isHidden = true
            Player.isHidden = true
            TFWeak.resignFirstResponder()
            RWLabel.isHidden = true
                P1Time = CFT + HMW
            }
            
        }
        }
    }
    @objc func doneButtonAction() {
        if TFWeak.text == WordLabel.text {
            print(CWCount)
            CWCount += 1
             WordLabel.text = Words[NumbersForWords[CWCount]]
            
           
              print(CWCount)
            Check()
           
            NOWH += 1
            TFWeak.text = ""
            
            RWLabel.text = "Right"
            CorrectSoundPlayer.play()
            RWLabel.textColor = UIColor.green
        } else if TFWeak.text != WordLabel.text {
            RWLabel.text = "Wrong"
            WrongSoundPlayer.play()
            RWLabel.textColor = UIColor.red
            HMW += 1
            timeLabel.text = "\(CFT) + \(HMW)"
            TFWeak.text = ""
            
        }
    }
    @IBAction func TF(_ sender: UITextField) {
    }
    
    
    @IBAction func Pause(_ sender: UIButton) {
        if SecondPlayer == true {
            PRN = Player2Name
        } else if SecondPlayer == false {
            PRN = Player1Name
        }
       MultiTimer.invalidate()
        WordLabel.isHidden = true
        let Alert = UIAlertController(title: "Paused", message: "The Game has been paused | Number of Words \(NOWH) out of \(NOW)", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Countinue", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.CWUpdateCounter), userInfo: nil, repeats: true)
           self.WordLabel.isHidden = false
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        
        
        
    }
    
    
    @IBAction func Exit(_ sender: UIButton) {
        MultiTimer.invalidate()
        
        let Alert = UIAlertController(title: "Quit?", message: "Are you sure you want to quit?", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Quit", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
           NumbersForWords.removeAll()
            for _ in 0..<50 {
                NumbersForWords.append(Int(arc4random_uniform(UInt32(100))))
            }
            CWCount = 0
            SecondPlayer = false
            CFT = 0.0
            P1Time = 0
            P2Time = 0
            self.TFWeak.text = ""
            
            _ = self.navigationController?.popViewController(animated: true)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.CWUpdateCounter), userInfo: nil, repeats: true)
        })
        Alert.addAction(DissmissButton)
        Alert.addAction(cancel)
        
        self.present(Alert, animated: true, completion: nil)
    }
    
    @IBAction func SecondPlayerStrong(_ sender: UIButton) {
        if SecondPlayerWeak.currentTitle == "Rematch" {
            
            if hasTenPlayerPack == true || hasBundle == true  {
                NOPH = 1
                MultiTimeLabel.isHidden = true
            }
            HMW = 0
            CFT = 0
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            TFWeak.autocorrectionType = .no
            WordLabel.text = Words[NumbersForWords[CWCount]]
            self.addDoneButtonOnKeyboard()
            P1TimeLabel.isHidden = true
            P2TimeLabel.isHidden = true
            SecondPlayer = false
            P1TimeLabel.textColor = P1Color
            if P1isShadow == true {
                P1TimeLabel.layer.shadowColor = P1Color.cgColor
                P1TimeLabel.layer.shadowOffset = CGSize(5,0)
                P1TimeLabel.layer.shadowRadius = 2
                P1TimeLabel.layer.shadowOpacity = 0.25
                
            } else {
                P1TimeLabel.layer.shadowColor = P1Color.cgColor
                P1TimeLabel.layer.shadowOffset = CGSize(0,0)
                P1TimeLabel.layer.shadowRadius = 0
                P1TimeLabel.layer.shadowOpacity = 0.0
            }
            P2TimeLabel.textColor = P2Color
            if P2isShadow == true {
                P2TimeLabel.layer.shadowColor = P2Color.cgColor
                P2TimeLabel.layer.shadowOffset = CGSize(5,0)
                P2TimeLabel.layer.shadowRadius = 2
                P2TimeLabel.layer.shadowOpacity = 0.25
                
            } else {
                P2TimeLabel.layer.shadowColor = P2Color.cgColor
                P2TimeLabel.layer.shadowOffset = CGSize(0,0)
                P2TimeLabel.layer.shadowRadius = 0
                P2TimeLabel.layer.shadowOpacity = 0.0
            }
            SecondPlayer = false
            TFWeak.textColor = P1Color
            Player.text = Player1Name
            Player.textColor = P1Color
            if P1isShadow == true {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(5,0)
                Player.layer.shadowRadius = 2
                Player.layer.shadowOpacity = 0.25
                
            } else {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(0,0)
                Player.layer.shadowRadius = 0
                Player.layer.shadowOpacity = 0.0
            }
            SecondPlayer = false
            CDNameLabel.isHidden = false
            CDCountLabel.isHidden = false
            CWCount = 0
            NumbersForWords.removeAll()
            
            CountDownCount = 6
            CDCountLabel.text = "Loading"
            SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
            SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
            if P2isShadow == true {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
                SecondPlayerWeak.layer.shadowRadius = 2
                SecondPlayerWeak.layer.shadowOpacity = 0.25
            } else {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
                SecondPlayerWeak.layer.shadowRadius = 0
                SecondPlayerWeak.layer.shadowOpacity = 0.0
            }
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            CDNameLabel.text = "\(Player1Name) in"
            CDNameLabel.textColor = P1Color
            if P1isShadow == true {
                CDNameLabel.layer.shadowColor = P1Color.cgColor
                CDNameLabel.layer.shadowOffset = CGSize(5,0)
                CDNameLabel.layer.shadowRadius = 2
                CDNameLabel.layer.shadowOpacity = 0.25
                
            } else {
                CDNameLabel.layer.shadowColor = P1Color.cgColor
                CDNameLabel.layer.shadowOffset = CGSize(0,0)
                CDNameLabel.layer.shadowRadius = 0
                CDNameLabel.layer.shadowOpacity = 0.0
            }
            
            CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CWCountDownFunc), userInfo: nil, repeats: true)
            
            for _ in 0..<50 {
                NumbersForWords.append(Int(arc4random_uniform(UInt32(100))))
            }
            
            RWLabel.isHidden = true
            P1TimeLabel.isHidden = true
            P2TimeLabel.isHidden = true
            PauseWeak.isHidden = true
            ExitWeak.isHidden = true
            Player.isHidden = true
            SecondPlayerWeak.isHidden = true
            WordLabel.isHidden = true
            TFWeak.isHidden = true
            timeLabel.isHidden = true
            
            
        } else {
        SecondPlayer = true
        SecondPlayerWeak.isHidden = true
        timeLabel.isHidden = true
        CDNameLabel.isHidden = false
            NOWH = 0
        CDCountLabel.isHidden = false
        CFT = 0
        CDCountLabel.text = "Loading"
        CWCount = 0
            WordLabel.text = String(Words[NumbersForWords[CWCount]])
            if hasTenPlayerPack == false {
                CDNameLabel.text = "\(Player2Name) in"
                  CDNameLabel.textColor = P2Color
                if P2isShadow == true {
                    CDNameLabel.layer.shadowColor = P2Color.cgColor
                    CDNameLabel.layer.shadowOffset = CGSize(5,0)
                    CDNameLabel.layer.shadowRadius = 2
                    CDNameLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDNameLabel.layer.shadowColor = P2Color.cgColor
                    CDNameLabel.layer.shadowOffset = CGSize(0,0)
                    CDNameLabel.layer.shadowRadius = 0
                    CDNameLabel.layer.shadowOpacity = 0.0
                }
                 TFWeak.textColor = P2Color
            }
            
      
       
        
            HMW = 0
          CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CWCountDownFunc), userInfo: nil, repeats: true)
        Player.text = Player2Name
        Player.textColor = P2Color
        if P2isShadow == true {
            Player.layer.shadowColor = P2Color.cgColor
            Player.layer.shadowOffset = CGSize(5,0)
            Player.layer.shadowRadius = 2
            Player.layer.shadowOpacity = 0.25
            
        } else {
            Player.layer.shadowColor = P2Color.cgColor
            Player.layer.shadowOffset = CGSize(0,0)
            Player.layer.shadowRadius = 0
            Player.layer.shadowOpacity = 0.0
            }
        
        
        }
        
        
    }
    
    
   // @IBAction func SwipeDown(_ sender: UISwipeGestureRecognizer) {
   //     TFWeak.resignFirstResponder()
   // }
    @IBAction func pat(_ sender: Any) {
        
    }
    
    @IBAction func TFDidEndOnExit(_ sender: UITextField) {
        
        if TFWeak.text == WordLabel.text {
             CWCount += 1
              WordLabel.text = Words[NumbersForWords[CWCount]]
         
            print("KEYBOARD")
           
             Check()
            NOWH += 1
            TFWeak.text = ""
          
            RWLabel.text = "Right"
            CorrectSoundPlayer.play()
            RWLabel.textColor = UIColor.green
        } else if TFWeak.text != WordLabel.text {
            RWLabel.text = "Wrong"
            WrongSoundPlayer.play()
            RWLabel.textColor = UIColor.red
            HMW += 1
            timeLabel.text = "\(CFT) + \(HMW)"
            TFWeak.text = ""
            
        }
    }
    
    
   
    
 
    
    
    
    
    
    
    
    
    
}
