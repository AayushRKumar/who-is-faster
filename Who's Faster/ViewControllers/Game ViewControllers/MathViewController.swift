//
//  MathViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 8/13/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import MultipeerConnectivity
import GoogleMobileAds

var CountDownTimer = Timer()
var CountDownCount = 6
var P3Time = 1000000.0
var P4Time = 1000000.0
var P5Time = 1000000.0
var P6Time = 1000000.0
var P7Time = 1000000.0
var P8Time = 1000000.0
var P9Time = 1000000.0
var P10Time = 1000000.0
var NOP = 2
//Number of Players
var NOPH = 0
var OPFAV = "X"
  var CorrectSoundPlayer:AVAudioPlayer = AVAudioPlayer()
 var WrongSoundPlayer:AVAudioPlayer = AVAudioPlayer()
//Operation for alert view
class MathViewController: UIViewController, GADInterstitialDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("p2 shadow \(P2isShadow)")
        CFT = 0
        SecondPlayerWeak.layer.cornerRadius = 10
        SecondPlayerWeak.clipsToBounds = true
        NegativeWeak.layer.cornerRadius = 10
        NegativeWeak.clipsToBounds = true
        NOQH = 0
        CDLabel.adjustsFontSizeToFitWidth = true
        CDNumber.adjustsFontSizeToFitWidth = true
        Player.adjustsFontSizeToFitWidth = true
        Timelabel.adjustsFontSizeToFitWidth = true
        Number1.adjustsFontSizeToFitWidth = true
        Number2.adjustsFontSizeToFitWidth = true
        Equals.adjustsFontSizeToFitWidth = true
        Operation.adjustsFontSizeToFitWidth = true
        P1TimeLabel.adjustsFontSizeToFitWidth = true
        P2TimeLabel.adjustsFontSizeToFitWidth = true
        MultiTimeLabel.adjustsFontSizeToFitWidth = true
        SecondPlayerWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        RWLabel.adjustsFontSizeToFitWidth = true
        NegativeWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        TF.adjustsFontSizeToFitWidth = true
        
    
        
        P1Time = 10000.0
        P2Time = 100000.0
        P3Time = 100000.0
        P4Time = 100000.0
        P5Time = 10000.0
        P6Time = 10000.0
        P7Time = 100000.0
        P8Time = 10000.0
        P9Time = 1000.0
        P10Time = 10000.0
        
        
        if hasTheme == true || hasBundle == true  {
            if CustomTheme == true {
                MathNormalFont()
                self.view.backgroundColor = PrimaryColor
                CDLabel.textColor = TextColor
                CDNumber.textColor = TextColor
                Player.textColor = TextColor
                Timelabel.textColor = TextColor
                Number1.textColor = TextColor
                Number2.textColor = TextColor
                Equals.textColor = TextColor
                Operation.textColor = TextColor
                P1TimeLabel.textColor = TextColor
                P2TimeLabel.textColor = TextColor
                Player.textColor = TextColor
                MultiTimeLabel.textColor = TextColor
                RWLabel.textColor = TextColor
                NegativeWeak.setTitleColor(SecondaryColor, for: .normal)
                SecondPlayerWeak.setTitleColor(SecondaryColor, for: .normal)
                TF.backgroundColor = SecondaryColor
                TF.textColor = UIColor.black
            } else {
            if Theme == 0 {
                MathNormalFont()
                MathNormalColor()
                NegativeWeak.backgroundColor = UIColor.orange
                view.backgroundColor = UIColor.white
                TF.textColor = UIColor.black
            } else if Theme == 1 {
                MathNormalColor()
                CDLabel.font = UIFont (name: "Marker Felt", size: 26)
                CDNumber.font = UIFont (name: "Marker Felt", size: 26)
                Player.font = UIFont (name: "Marker Felt", size: 29)
                Timelabel.font = UIFont (name: "Marker Felt", size: 35)
                Number1.font = UIFont (name: "Marker Felt", size: 50)
                Number2.font = UIFont (name: "Marker Felt", size: 50)
                Equals.font = UIFont (name: "Marker Felt", size: 50)
                Operation.font = UIFont (name: "Marker Felt", size: 38)
                P1TimeLabel.font = UIFont (name: "Marker Felt", size: 23)
                P2TimeLabel.font = UIFont (name: "Marker Felt", size: 23)
                MultiTimeLabel.font = UIFont (name: "Marker Felt", size: 18)
                SecondPlayerWeak.backgroundColor = UIColor.gray
                RWLabel.font = UIFont (name: "Marker Felt", size: 35)
                NegativeWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 47)!
                TF.font = UIFont (name: "Marker Felt", size: 36)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 25)!
                NegativeWeak.backgroundColor = UIColor.gray
                TF.backgroundColor = UIColor.lightGray
                view.backgroundColor = UIColor.darkGray
                TF.textColor = UIColor.black
            } else if Theme == 2 {
                
                CDLabel.font = UIFont (name: "Chalkduster", size: 26)
                CDNumber.font = UIFont (name: "Chalkduster", size: 26)
                Player.font = UIFont (name: "Chalkduster", size: 29)
                Timelabel.font = UIFont (name: "Chalkduster", size: 35)
                Number1.font = UIFont (name: "Chalkduster", size: 50)
                Number2.font = UIFont (name: "Chalkduster", size: 50)
                Equals.font = UIFont (name: "Chalkduster", size: 50)
                Operation.font = UIFont (name: "Chalkduster", size: 38)
                P1TimeLabel.font = UIFont (name: "Chalkduster", size: 23)
                P2TimeLabel.font = UIFont (name: "Chalkduster", size: 23)
                
                MultiTimeLabel.font = UIFont (name: "Chalkduster", size: 18)
                RWLabel.font = UIFont (name: "Chalkduster", size: 35)
                NegativeWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 47)!
                TF.font = UIFont (name: "Chalkduster", size: 36)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 25)!
                NegativeWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
               
                SecondPlayerWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
               MathWhiteColor()
                TF.textColor = UIColor.black
            } else if Theme == 3 {
                MathNormalColor()
                CDLabel.font = UIFont (name: "Papyrus", size: 26)
                CDNumber.font = UIFont (name: "Papyrus", size: 26)
                 Player.font = UIFont (name: "Papyrus", size: 29)
                 Timelabel.font = UIFont (name: "Papyrus", size: 35)
                Number1.font = UIFont (name: "Papyrus", size: 50)
                 Number2.font = UIFont (name: "Papyrus", size: 50)
                 Equals.font = UIFont (name: "Papyrus", size: 50)
                 Operation.font = UIFont (name: "Papyrus", size: 38)
                 P1TimeLabel.font = UIFont (name: "Papyrus", size: 23)
                P2TimeLabel.font = UIFont (name: "Papyrus", size: 23)
                SecondPlayerWeak.backgroundColor = UIColor.white
                MultiTimeLabel.font = UIFont (name: "Papyrus", size: 18)
                RWLabel.font = UIFont (name: "Papyrus", size: 35)
                NegativeWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 47)!
                TF.font = UIFont (name: "Papyrus", size: 36)
                 SecondPlayerWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 25)!
                NegativeWeak.backgroundColor = UIColor.brown
                TF.backgroundColor = UIColor(red: (126/255.0), green: (76/255.0), blue: (18/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.brown
                TF.textColor = UIColor.black
            } else if Theme == 4 {
                //Notes
                MathNormalColor()
                CDLabel.font = UIFont (name: "Noteworthy", size: 26)
                CDNumber.font = UIFont (name: "Noteworthy", size: 26)
                Player.font = UIFont (name: "Noteworthy", size: 29)
                Timelabel.font = UIFont (name: "Noteworthy", size: 35)
                Number1.font = UIFont (name: "Noteworthy", size: 50)
                Number2.font = UIFont (name: "Noteworthy", size: 50)
                Equals.font = UIFont (name: "Noteworthy", size: 50)
                Operation.font = UIFont (name: "Noteworthy", size: 38)
                P1TimeLabel.font = UIFont (name: "Noteworthy", size: 23)
                P2TimeLabel.font = UIFont (name: "Noteworthy", size: 23)
                
                MultiTimeLabel.font = UIFont (name: "Noteworthy", size: 18)
                RWLabel.font = UIFont (name: "Noteworthy", size: 35)
                NegativeWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 47)!
                TF.font = UIFont (name: "Noteworthy", size: 36)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 25)!
                NegativeWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                
                SecondPlayerWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 5 {
                //space
                MathWhiteColor()
                MathNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                TF.textColor = UIColor.white
                TF.backgroundColor = UIColor.black
                SecondPlayerWeak.backgroundColor = UIColor.white
            } else if Theme == 6 {
                MathWhiteColor()
                MathNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                TF.textColor = UIColor.white
                TF.backgroundColor = UIColor.blue
                SecondPlayerWeak.backgroundColor = UIColor.white
                TF.textColor = UIColor.black
            } else if Theme == 7 {
               MathNormalFont()
                MathNormalColor()
                NegativeWeak.backgroundColor = UIColor(red: (225/255.0), green: (61/255.0), blue: (71/255.0), alpha: 1.0)
                TF.backgroundColor = UIColor.red
                view.backgroundColor = UIColor.red
                SecondPlayerWeak.backgroundColor = UIColor(red: (149/255.0), green: (32/255.0), blue: (38/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 8 {
                MathNormalFont()
                MathNormalColor()
                NegativeWeak.backgroundColor = UIColor(red: (28/255.0), green: (146/255.0), blue: (190/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.blue
                TF.backgroundColor = UIColor.blue
                SecondPlayerWeak.backgroundColor = UIColor(red: (28/255.0), green: (88/255.0), blue: (190/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 9 {
                MathNormalFont()
                MathNormalColor()
                NegativeWeak.backgroundColor = UIColor(red: (28/255.0), green: (255/255.0), blue: (0/255.0), alpha: 1.0)
                view.backgroundColor = UIColor(red: (28/255.0), green: (255/255.0), blue: (98/255.0), alpha: 1.0)
                TF.backgroundColor = UIColor.green
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (255/255.0), blue: (152/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 10 {
                MathNormalFont()
                MathNormalColor()
                TF.backgroundColor = UIColor.purple
                NegativeWeak.backgroundColor = UIColor(red: (182/255.0), green: (144/255.0), blue: (255/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.purple
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (0/255.0), blue: (239/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 11 {
                MathNormalFont()
                MathNormalColor()
                TF.backgroundColor = UIColor.orange
                NegativeWeak.backgroundColor = UIColor(red: (255/255.0), green: (92/255.0), blue: (0/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.orange
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (117/255.0), blue: (0/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 12 {
                MathNormalFont()
                MathNormalColor()
                TF.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                NegativeWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            } else if Theme == 13 {
                MathNormalFont()
                MathNormalColor()
               TF.backgroundColor = UIColor.yellow
                NegativeWeak.backgroundColor = UIColor(red: (255/255.0), green: (92/255.0), blue: (0/255.0), alpha: 1.0)
                view.backgroundColor = UIColor.yellow
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (231/255.0), blue: (61/255.0), alpha: 1.0)
                TF.textColor = UIColor.black
            }
            }
        }
        
        
        
        
        
        
        
        let FileLocation = Bundle.main.path(forResource: "Correct-answer", ofType: ".mp4")
        do {
            CorrectSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: FileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
        let WrongFileLocation = Bundle.main.path(forResource: "Wrong-answer", ofType: ".mp4")
        
        do {
           WrongSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: WrongFileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
      
       NOPH = 1
        CDLabel.textColor = UIColor.black
        CountMath = 0
        P1TimeLabel.isHidden = true
        P2TimeLabel.isHidden = true
        P1TimeLabel.textColor = P1Color
        if P1isShadow == true {
            P1TimeLabel.layer.shadowColor = P1Color.cgColor
            P1TimeLabel.layer.shadowOffset = CGSize(5,0)
            P1TimeLabel.layer.shadowRadius = 2
            P1TimeLabel.layer.shadowOpacity = 0.25
            
        } else {
            P2TimeLabel.layer.shadowColor = P1Color.cgColor
            P2TimeLabel.layer.shadowOffset = CGSize(0,0)
            P2TimeLabel.layer.shadowRadius = 0
            P2TimeLabel.layer.shadowOpacity = 0.0
        }
        P2TimeLabel.textColor = P2Color
        if P2isShadow == true {
            P2TimeLabel.layer.shadowColor = P2Color.cgColor
            P2TimeLabel.layer.shadowOffset = CGSize(5,0)
            P2TimeLabel.layer.shadowRadius = 2
            P2TimeLabel.layer.shadowOpacity = 0.25
            
        } else {
            P2TimeLabel.layer.shadowColor = P2Color.cgColor
            P2TimeLabel.layer.shadowOffset = CGSize(0,0)
            P2TimeLabel.layer.shadowRadius = 0
            P2TimeLabel.layer.shadowOpacity = 0.0
        }
        SecondPlayer = false
        MultiTimeLabel.isHidden = true
     TF.textColor = P1Color
        if doingMD == true && isHost == true {
            Player.text = hostName
            
        } else {
        Player.text = Player1Name
        }
        Player.textColor = P1Color
        if P1isShadow == true {
            Player.layer.shadowColor = P1Color.cgColor
            Player.layer.shadowOffset = CGSize(5,0)
            Player.layer.shadowRadius = 2
            Player.layer.shadowOpacity = 0.25
            
        } else {
            Player.layer.shadowColor = P1Color.cgColor
            Player.layer.shadowOffset = CGSize(0,0)
            Player.layer.shadowRadius = 0
            Player.layer.shadowOpacity = 0.0
        }
        
       CountDownCount = 6
        CDNumber.text = "Loading"
        SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
        SecondPlayerWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        
        SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
        if P2isShadow == true {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
            SecondPlayerWeak.layer.shadowRadius = 2
            SecondPlayerWeak.layer.shadowOpacity = 0.25
        } else {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
            SecondPlayerWeak.layer.shadowRadius = 0
            SecondPlayerWeak.layer.shadowOpacity = 0.0
        }
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        if doingMD == true && isHost == true {
            CDLabel.text = "\(hostName) in"
        } else {
        CDLabel.text = "\(Player1Name) in"
        }
        CDLabel.textColor = P1Color
        if P1isShadow == true {
            CDLabel.layer.shadowColor = P1Color.cgColor
            CDLabel.layer.shadowOffset = CGSize(5,0)
            CDLabel.layer.shadowRadius = 2
            CDLabel.layer.shadowOpacity = 0.25
            
        } else {
            CDLabel.layer.shadowColor = P1Color.cgColor
            CDLabel.layer.shadowOffset = CGSize(0,0)
            CDLabel.layer.shadowRadius = 0
            CDLabel.layer.shadowOpacity = 0.0
        }
        
        CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CountDownFunc), userInfo: nil, repeats: true)
        
        
       
        
        
      
        if SecondPlayerWeak != nil {
        SecondPlayerWeak.isHidden = true
        }
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.addDoneButtonOnKeyboard()
        if doingMD == false {
        Append()
        }
        if Number1 != nil {
        Number1.text = String(N1[CountMath])
        }
        if Number2 != nil {
        Number2.text = String(N2[CountMath])
        }
        DisplayOp()
       
 Number1.isHidden = true
        Number2.isHidden = true
        Operation.isHidden = true
        Equals.isHidden = true
        Timelabel.isHidden = true
        RWLabel.isHidden = true
        TF.isHidden = true
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
        Player.isHidden = true
        
        NegativeWeak.isHidden = true
        // Do any additional setup after loading the view.
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        
        popup = GADInterstitial(adUnitID: "ca-app-pub-2312248651171588/1230141252")
        popup.delegate = self
        popup.load(GADRequest())
        
        return popup
    }
    @IBOutlet weak var PauseWeak: UIButton!
    @IBOutlet weak var ExitWeak: UIButton!
    @IBOutlet weak var TF: UITextField!
    @IBOutlet weak var Player: UILabel!
    @IBOutlet weak var Timelabel: UILabel!
    @IBOutlet weak var RWLabel: UILabel!
    @IBOutlet weak var Number1: UILabel!
  
    @IBOutlet weak var P2TimeLabel: UILabel!
    @IBOutlet weak var P1TimeLabel: UILabel!
    @IBOutlet weak var CDLabel: UILabel!
    @IBOutlet weak var Operation: UILabel!
    
    @IBOutlet weak var MultiTimeLabel: UILabel!
    @IBOutlet weak var CDNumber: UILabel!
    @IBOutlet weak var Number2: UILabel!
    @IBOutlet weak var NegativeWeak: UIButton!
    @IBOutlet weak var SecondPlayerWeak: UIButton!
    @IBOutlet weak var Equals: UILabel!
    @objc func UpdateCounter() {
        CFT = CFT + 0.1
        Timelabel.text = String(format: "%.1f", CFT)
        //CFT += 0.1
       
        //Timelabel.text = String(CFT)
         
        
        
    }
    
    func moveLabelsDown() {
       
        
    }
    func MathNormalFont() {
        CDLabel.font = UIFont (name: "Helvetica Neue", size: 26)
        CDNumber.font = UIFont (name: "Helvetica Neue", size: 26)
        Player.font = UIFont (name: "Helvetica Neue", size: 29)
        Timelabel.font = UIFont (name: "Helvetica Neue", size: 35)
        Number1.font = UIFont (name: "Helvetica Neue", size: 50)
        Number2.font = UIFont (name: "Helvetica Neue", size: 50)
        Equals.font = UIFont (name: "Helvetica Neue", size: 50)
        Operation.font = UIFont (name: "Helvetica Neue", size: 38)
        P1TimeLabel.font = UIFont (name: "Helvetica Neue", size: 23)
        P2TimeLabel.font = UIFont (name: "Helvetica Neue", size: 23)
        MultiTimeLabel.font = UIFont (name: "Helvetica Neue", size: 18)
        RWLabel.font = UIFont (name: "Helvetica Neue", size: 35)
        NegativeWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 47)!
        TF.font = UIFont (name: "Helvetica Neue", size: 36)
        SecondPlayerWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 25)!
    }
    func MathNormalColor() {
        CDLabel.textColor = UIColor.black
        CDNumber.textColor = UIColor.black
        Player.textColor = UIColor.black
        Timelabel.textColor = UIColor.black
        Number1.textColor = UIColor.black
        Number2.textColor = UIColor.black
        Equals.textColor = UIColor.black
        Operation.textColor = UIColor.black
        P1TimeLabel.textColor = UIColor.black
        P2TimeLabel.textColor = UIColor.black
        
        MultiTimeLabel.textColor = UIColor.black
        RWLabel.textColor = UIColor.black
        NegativeWeak.setTitleColor(UIColor.black, for: .normal)
        SecondPlayerWeak.setTitleColor(UIColor.black, for: .normal)
    }
    func MathWhiteColor() {
        CDLabel.textColor = UIColor.white
        CDNumber.textColor = UIColor.white
        Player.textColor = UIColor.white
        Timelabel.textColor = UIColor.white
        Number1.textColor = UIColor.white
        Number2.textColor = UIColor.white
        Equals.textColor = UIColor.white
        Operation.textColor = UIColor.white
        P1TimeLabel.textColor = UIColor.white
        P2TimeLabel.textColor = UIColor.white
        Player.textColor = UIColor.white
        MultiTimeLabel.textColor = UIColor.white
        RWLabel.textColor = UIColor.white
        NegativeWeak.setTitleColor(UIColor.white, for: .normal)
        SecondPlayerWeak.setTitleColor(UIColor.white, for: .normal)
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if self.TF != nil {
        self.TF.inputAccessoryView = doneToolbar
        }
    }
   
    func TrialDone() {
       
        ExitWeak.isHidden = true
        TF.resignFirstResponder()
        TrialRun = false
      
        PauseWeak.isHidden = true
        Player.textColor = UIColor.black
        Player.layer.shadowColor = UIColor.white.cgColor
        Player.isHidden = true
        
    }
    func MultiPlayerHide() {
        
        CFT = (CFT * 100).rounded() / 100
        MultiTimer.invalidate()
        TF.isHidden = true
        Player.isHidden = true
        SecondPlayerWeak.isHidden = false
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
        Number1.isHidden = true
        Number2.isHidden = true
        Operation.isHidden = true
        Equals.isHidden = true
        NegativeWeak.isHidden = true
    }
    //--------------------------------------------------------------------------------------------------------------------
    func DisplayOp() {
        if OpCount == 1 {
            if Operation != nil {
            Operation.text = "X"
            }
        } else if OpCount == 2 {
            Operation.text = "+"
        } else if OpCount == 3 {
            Operation.text = "-"
        }
    }
    func PremiumFunc() {
        if NOP == NOPH && NOQ == CountMath {
            TF.isHidden = true
            CFT = (CFT * 100).rounded() / 100
            if NOP == 10 {
                P10Time = CFT + HMW
            } else if NOP == 9 {
                P9Time = CFT + HMW
            } else if NOP == 8 {
                P8Time = CFT + HMW
            } else if NOP == 7 {
                P7Time = CFT + HMW
            } else if NOP == 6 {
                P6Time = CFT + HMW
            } else if NOP == 5 {
                P5Time = CFT + HMW
            } else if NOP == 4 {
                P4Time = CFT + HMW
            } else if NOP == 3 {
                P3Time = CFT + HMW
            } else if NOP == 2 {
                P2Time = CFT + HMW
            }
            
            MultiTimeLabel.isHidden = false
            
            if NOP == 2 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time)"
            } else if NOP == 3 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time)"
            } else if NOP == 4 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time)"
            } else if NOP == 5 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time)"
            } else if NOP == 6 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time)"
            } else if NOP == 7 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time)"
            } else if NOP == 8 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time)"
            } else if NOP == 9 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time)"
            } else if NOP == 10 {
                MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time) | \(Player10Name)'s time was \(P10Time)"
            }
            popup = createAndLoadInterstitial()
            MultiTimer.invalidate()
            PauseWeak.isHidden = true
            TF.resignFirstResponder()
            RWLabel.isHidden = true
            Number1.isHidden = true
            Operation.isHidden = true
            Equals.isHidden = true
            NegativeWeak.isHidden = true
            Number2.isHidden = true
            Timelabel.isHidden = true
             scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
            let min = scores.min()
            popup = createAndLoadInterstitial()

            if min == P1Time {
                CDLabel.textColor = P1Color
                if P1isShadow == true {
                    CDLabel.layer.shadowColor = P1Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P1Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player1Name) wins!"
            } else if min == P2Time {
                CDLabel.textColor = P2Color
                if P2isShadow == true {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5, 0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0, 0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                }
                CDLabel.text = "\(Player2Name) wins!"
            } else if min == P3Time {
                CDLabel.textColor = P3Color
                if P3isShadow == true {
                    CDLabel.layer.shadowColor = P3Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P3Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player3Name) wins!"
            } else if min == P4Time {
                CDLabel.textColor = P4Color
                if P4isShadow == true {
                    CDLabel.layer.shadowColor = P4Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P4Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player4Name) wins!"
            } else if min == P5Time {
                CDLabel.textColor = P5Color
                if P5isShadow == true {
                    CDLabel.layer.shadowColor = P5Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P5Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player5Name) wins!"
            } else if min == P6Time {
                CDLabel.textColor = P6Color
                if P6isShadow == true {
                    CDLabel.layer.shadowColor = P6Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P6Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player6Name) wins!"
            }  else if min == P7Time {
                CDLabel.textColor = P7Color
                if P7isShadow == true {
                    CDLabel.layer.shadowColor = P7Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P7Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player7Name) wins!"
            }  else if min == P8Time {
                CDLabel.textColor = P8Color
                if P8isShadow == true {
                    CDLabel.layer.shadowColor = P8Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P8Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player8Name) wins!"
            }  else if min == P9Time {
                CDLabel.textColor = P9Color
                if P9isShadow == true {
                    CDLabel.layer.shadowColor = P9Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P9Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player9Name) wins!"
            }  else if min == P10Time {
                CDLabel.textColor = P10Color
                if P10isShadow == true {
                    CDLabel.layer.shadowColor = P10Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P10Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                CDLabel.text = "\(Player10Name) wins!"
                
            }
            if isTournament == false {
                SecondPlayerWeak.setTitle("Rematch", for: .normal)
            } else {
                SecondPlayerWeak.isHidden = true
            }
            ExitWeak.isHidden = true
            
            CDLabel.isHidden = false
            SecondPlayerWeak.isHidden = false
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            
            
            
        } else if NOQ == CountMath {
            CFT = (CFT * 100).rounded() / 100
            MultiTimer.invalidate()
            TF.resignFirstResponder()
            CDLabel.isHidden = true
            CountMath = 0
            Number1.isHidden = true
            Number2.isHidden = true
            Operation.isHidden = true
            Equals.isHidden = true
            RWLabel.isHidden = true
            PauseWeak.isHidden = true
            TF.isHidden = true
            Player.isHidden = true
             NOPH += 1
            ExitWeak.isHidden = true
            if NOPH == 2 {
                P1Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
                CDLabel.text = "\(Player2Name) in"
                CDLabel.textColor = P2Color
                if P2isShadow == true {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                }
                TF.textColor = P2Color
            } else if NOPH == 3 {
                P2Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player3Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P3Color, for: .normal)
                CDLabel.text = "\(Player3Name) in"
            
                CDLabel.textColor = P3Color
               TF.textColor = P3Color
                if P3isShadow == true {
                    CDLabel.layer.shadowColor = P3Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P3Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
            } else if NOPH == 4 {
                P3Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player4Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P4Color, for: .normal)
                CDLabel.text = "\(Player4Name) in"
                CDLabel.textColor = P4Color
                if P4isShadow == true {
                    CDLabel.layer.shadowColor = P4Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P4Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P4Color
            } else if NOPH == 5 {
                P4Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player5Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P5Color, for: .normal)
                CDLabel.text = "\(Player5Name) in"
                CDLabel.textColor = P5Color
                if P5isShadow == true {
                    CDLabel.layer.shadowColor = P5Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P5Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P5Color
            } else if NOPH == 6 {
                P5Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player6Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P6Color, for: .normal)
                CDLabel.text = "\(Player6Name) in"
                CDLabel.textColor = P6Color
                if P6isShadow == true {
                    CDLabel.layer.shadowColor = P6Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P6Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P6Color
            } else if NOPH == 7 {
                P6Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player7Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P7Color, for: .normal)
                CDLabel.text = "\(Player7Name) in"
                CDLabel.textColor = P7Color
                if P7isShadow == true {
                    CDLabel.layer.shadowColor = P7Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P7Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P7Color
            } else if NOPH == 8 {
                P7Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player8Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P8Color, for: .normal)
                CDLabel.text = "\(Player8Name) in"
                CDLabel.textColor = P8Color
                if P8isShadow == true {
                    CDLabel.layer.shadowColor = P8Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P8Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P8Color
            } else if NOPH == 9 {
                P8Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player9Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P9Color, for: .normal)
                CDLabel.text = "\(Player9Name) in"
                CDLabel.textColor = P9Color
                if P9isShadow == true {
                    CDLabel.layer.shadowColor = P9Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P9Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P9Color
            } else if NOPH == 10 {
                P9Time = CFT + HMW
                MultiPlayerHide()
                SecondPlayerWeak.setTitle("\(Player10Name)'s turn", for: .normal)
                SecondPlayerWeak.setTitleColor(P10Color, for: .normal)
                CDLabel.text = "\(Player10Name) in"
                CDLabel.textColor = P10Color
                if P10isShadow == true {
                    CDLabel.layer.shadowColor = P10Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P10Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                    
                }
                   TF.textColor = P10Color
            }
            
            
            
            SecondPlayerWeak.isHidden = false
            
            CountDownCount = 6
            NOTBH = 0
            
           
            
            CFT = 0
        }
    }
    func Append() {
        for _ in 0..<50 {
            N1.append(Int(arc4random_uniform(UInt32(RangeMath))))
            N2.append(Int(arc4random_uniform(UInt32(RangeMath))))
        }
    }
    
    func TrialStart() {
        Append()
        
        PauseWeak.isHidden = false
        TrialRun = true
        P1Settings()
        CountMath = 0
        CFT = 0
        Timelabel.text = (String(CFT))
        MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
        Number1.text = String(N1[0])
        Number2.text = String(N2[0])
        DisplayOp()
    }
    
    func WhoWon() {
           CFT = (CFT * 100).rounded() / 100
        SecondPlayerWeak.layer.shadowColor = UIColor.white.cgColor
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        P2Time = CFT + HMW
        
        Number1.isHidden = true
        Number2.isHidden = true
        ExitWeak.isHidden = true
        SecondPlayerWeak.isHidden = false
        Operation.isHidden = true
        Equals.isHidden = true
        PauseWeak.isHidden = true
        Timelabel.isHidden = true
        RWLabel.isHidden = true
        TF.isHidden = true
            Player.isHidden = true
        P1TimeLabel.isHidden = false
        P2TimeLabel.isHidden = false
        if isTournament == false {
            SecondPlayerWeak.setTitle("Rematch", for: .normal)
        } else {
            SecondPlayerWeak.isHidden = true
        }
        CDLabel.isHidden = false
        popup = createAndLoadInterstitial()
      P1TimeLabel.text = "\(Player1Name)'s time was \(P1Time)"
        P2TimeLabel.text = "\(Player2Name)'s time was \(P2Time)"
        NegativeWeak.isHidden = true
        RWLabel.isHidden = true
        popup = createAndLoadInterstitial()
        
        var changer = 1
        for _ in 1 ... 100 {
            if P2TimeLabel.font != P1TimeLabel.font {
                    changer += 1
                P1TimeLabel.font.withSize(CGFloat(changer))
            }
        }
        
        if P1Time > P2Time {
            CDLabel.text = "\(Player2Name) wins!"
            CDLabel.textColor = P2Color
            if P2isShadow == true {
                CDLabel.layer.shadowColor = P2Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(5,0)
                CDLabel.layer.shadowRadius = 2
                CDLabel.layer.shadowOpacity = 0.25
            } else {
                CDLabel.layer.shadowColor = P2Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(0,0)
                CDLabel.layer.shadowRadius = 0
                CDLabel.layer.shadowOpacity = 0.0
            }
        } else if P1Time < P2Time {
            CDLabel.text = "\(Player1Name) wins!"
            CDLabel.textColor = P1Color
            if P1isShadow == true {
                CDLabel.layer.shadowColor = P1Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(5,0)
                CDLabel.layer.shadowRadius = 2
                CDLabel.layer.shadowOpacity = 0.25
            } else {
                CDLabel.layer.shadowColor = P1Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(0,0)
                CDLabel.layer.shadowRadius = 0
                CDLabel.layer.shadowOpacity = 0.0
            }
        } else if P1Time == P2Time {
            CDLabel.text = "Tie!"
            CDLabel.textColor = UIColor.black
            
        }
        
        GoBack = true
        TF.resignFirstResponder()
        MultiTimer.invalidate()
        Timelabel.isHidden = false
    }
    func P1Settings() {
        Player.textColor = P1Color
        if P1isShadow == true {
            Player.layer.shadowColor = P1Color.cgColor
            Player.layer.shadowOffset = CGSize(5,0)
            Player.layer.shadowRadius = 2
            Player.layer.shadowOpacity = 0.25
        }
    }
    @objc func CountDownFunc() {
        if hasTenPlayerPack == true || hasBundle == true  {
            if CountDownCount == 1 {
               Number1.isHidden = false
                NegativeWeak.isHidden = false
                Number2.isHidden = false
                Operation.isHidden = false
                Equals.isHidden = false
               
               
                TF.isHidden = false
                self.TF.becomeFirstResponder()
                CDNumber.isHidden = true
                CDLabel.isHidden = true
                Player.isHidden = false
                Timelabel.isHidden = false
                RWLabel.isHidden = false
                RWLabel.text = ""
                if doingMD == false {
                    PauseWeak.isHidden = false
                    
                    ExitWeak.isHidden = false
                } else if doingMD == true {
                    PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                }
                MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
                
                CountDownTimer.invalidate()
                if NOPH == 1 {
                    if doingMD == true && isHost == true {
                        Player.text = hostName
                    } else {
                    Player.text = "\(Player1Name)"
                    }
                    Player.textColor = P1Color
                    if P1isShadow == true {
                        Player.layer.shadowColor = P1Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    Player.text = "\(Player2Name)"
                    Player.textColor = P2Color
                    if P2isShadow == true {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 3 {
                    Player.text = "\(Player3Name)"
                    Player.textColor = P3Color
                    if P3isShadow == true {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    Player.text = "\(Player4Name)"
                    Player.textColor = P4Color
                    if P4isShadow == true {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    Player.text = "\(Player5Name)"
                    Player.textColor = P5Color
                    if P5isShadow == true {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    Player.text = "\(Player6Name)"
                    Player.textColor = P6Color
                    if P6isShadow == true {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    Player.text = "\(Player7Name)"
                    Player.textColor = P7Color
                    if P7isShadow == true {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    Player.text = "\(Player8Name)"
                    Player.textColor = P8Color
                    if P8isShadow == true {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    Player.text = "\(Player9Name)"
                    Player.textColor = P9Color
                    if P9isShadow == true {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 10 {
                    Player.text = "\(Player10Name)"
                    Player.textColor = P10Color
                    if P10isShadow == true {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                }
                Timelabel.text = "0"
                
                
            } else {
                CountDownCount += -1
                CDNumber.text = String(CountDownCount)
            }
        } else if hasTenPlayerPack == false {
        if CountDownCount == 1 {
            CountDownTimer.invalidate()
            Number1.isHidden = false
            Number2.isHidden = false
            Operation.isHidden = false
            Equals.isHidden = false
            Timelabel.isHidden = false
            RWLabel.isHidden = false
            TF.isHidden = false
            if doingMD == false {
                PauseWeak.isHidden = false
                
                ExitWeak.isHidden = false
            } else if doingMD == true {
                PauseWeak.isHidden = true
                ExitWeak.isHidden = true
            }
            Player.isHidden = false
            self.TF.becomeFirstResponder()
            NegativeWeak.isHidden = false
            CDLabel.isHidden = true
            CDNumber.isHidden = true
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
            
        } else {
            CountDownCount += -1
            CDNumber.text = String(CountDownCount)
        }
        }
    }
//--------------------------
    
    @objc func doneButtonAction()
    {
        a = TF.text!
      
        
        if OpCount == 1 {
            if N1[CountMath] * N2[CountMath] == Int(a) {
                if ChangingOp == true {
                    OpCount = RandomOp[CountMath]
                    
                    
                    DisplayOp()
                }
               
                RWLabel.text = "Right"
                CorrectSoundPlayer.play()
                RWLabel.textColor = UIColor.green
                if hasTheme == true || hasBundle == true  {
                    if Theme == 1 {
                        RWLabel.textColor = UIColor.gray
                    }
                }
                NOQH += 1
                CountMath += 1
                Number1.text = String(N1[CountMath])
                Number2.text = String(N2[CountMath])
                TF.text = ""
                if CountMath == NOQ {
                    if doingMD == true {
                        P1Time = CFT + HMW
                         SeeingTimes = true
                        if isHost == true {
                            self.performSegue(withIdentifier: "goMake", sender: self)
                } else {
                            self.performSegue(withIdentifier: "goInvite", sender: self)
                        }
                        
                    }
                    if hasTenPlayerPack == true || hasBundle == true  {
                        PremiumFunc()
                        
                    }  else if SecondPlayer == false {
                        SPFHide()
                        
                    } else if SecondPlayer == true {
                        WhoWon()
                        
                    }
                }
            }
            else if N1[CountMath] * N2[CountMath] != Int(a) {
                Wrong()
            }
            
        }
        else if OpCount == 2 {
            if N1[CountMath] + N2[CountMath] == Int(a) {
                if ChangingOp == true {
                    OpCount = RandomOp[CountMath + 1]
                    DisplayOp()
                    
                    
                }
                RWLabel.text = "Right"
                CorrectSoundPlayer.play()
                if ChangingOp == true {
                    OpCount = RandomOp[CountMath]
                    
                    
                    DisplayOp()
                }
                RWLabel.textColor = UIColor.green
                if hasTheme == true || hasBundle == true  {
                    if Theme == 1 {
                        RWLabel.textColor = UIColor.gray
                    }
                }
                CountMath += 1
                Number1.text = String(N1[CountMath])
                Number2.text = String(N2[CountMath])
                TF.text = ""
                NOQH += 1
                if CountMath == NOQ {
                    if doingMD == true {
                        P1Time = CFT + HMW
                        SeeingTimes = true
                        if isHost == true {
                           self.performSegue(withIdentifier: "goMake", sender: self)
                       } else {
                           self.performSegue(withIdentifier: "goInvite", sender: self)
                       }
                        
                    }
                    if hasTenPlayerPack == true || hasBundle == true  {
                        PremiumFunc()
                    }
                    else if SecondPlayer == false {
                        SPFHide()
                    } else if SecondPlayer == true {
                        WhoWon()
                    }
                }
            }
            else if N1[CountMath] + N2[CountMath] != Int(a) {
                Wrong()
                
            }
        }else if OpCount == 3 {
            if N1[CountMath] - N2[CountMath] == Int(a) {
                if ChangingOp == true {
                    OpCount = RandomOp[CountMath + 1]
                    
                    
                    DisplayOp()
                }
                RWLabel.text = "Right"
                CorrectSoundPlayer.play()
                if ChangingOp == true {
                    OpCount = RandomOp[CountMath]
                    
                    DisplayOp()
                }
                RWLabel.textColor = UIColor.green
                if hasTheme == true || hasBundle == true  {
                    if Theme == 1 {
                        RWLabel.textColor = UIColor.gray
                    }
                }
                CountMath += 1
                NOQH += 1
                Number1.text = String(N1[CountMath])
                Number2.text = String(N2[CountMath])
                TF.text = ""
                if CountMath == NOQ {
                    if doingMD == true {
                        P1Time = CFT + HMW
                        SeeingTimes = true
                        if isHost == true {
                            self.performSegue(withIdentifier: "goMake", sender: self)
                       } else {
                            self.performSegue(withIdentifier: "goInvite", sender: self)
                        }
                        
                    }
                    if hasTenPlayerPack == true || hasBundle == true  {
                        PremiumFunc()
                    }
                    else if SecondPlayer == false {
                        SPFHide()
                        
                    } else if SecondPlayer == true {
                        WhoWon()
                    }
                }
               
            } else if N1[CountMath] - N2[CountMath] != Int(a) {
                Wrong()
            }
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    
    func SecondStart() {
        SecondPlayer = true
        RWLabel.text = ""
        Player.textColor = P2Color
        if isTournament == false {
        ExitWeak.isHidden = false
        }
        if P2isShadow == true {
            Player.layer.shadowColor = P2Color.cgColor
            Player.layer.shadowOffset = CGSize(5,0)
            Player.layer.shadowRadius = 2
            Player.layer.shadowOpacity = 0.25
        } else {
            Player.layer.shadowColor = P2Color.cgColor
            Player.layer.shadowOffset = CGSize(0,0)
            Player.layer.shadowRadius = 0
            Player.layer.shadowOpacity = 0.0
        }
        SecondPlayerWeak.isHidden = true
        PauseWeak.isHidden = false
        Number1.isHidden = false
        Number2.isHidden = false
        NegativeWeak.isHidden = false
        Operation.isHidden = false
       
        Timelabel.isHidden = false
        Equals.isHidden = false
        Player.isHidden = false
        Player.text = "\(Player2Name)"
        TF.isHidden = false
        CountMath = 0
        Number1.text = String(N1[0])
        Number2.text = String(N2[0])
        TF.text = ""
        CFT = 0
        Timelabel.text = String(CFT)
        MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
    }
    
    func Wrong() {
        RWLabel.text = "Wrong"
        WrongSoundPlayer.play()
        RWLabel.textColor = UIColor.red
        if hasTheme == true || hasBundle == true  {
            if Theme == 1 {
                RWLabel.textColor = UIColor.black
            }
        }
        TF.text = ""
        HMW += 1
        Timelabel.text = "\(CFT) + \(HMW)"
    }
   
    func SPFHide() {
        //Second Player False Hide
           CFT = (CFT * 100).rounded() / 100
        Player.isHidden = true
        Number1.isHidden = true
        Number2.isHidden = true
        Operation.isHidden = true
        ExitWeak.isHidden = true
        Equals.isHidden = true
        PauseWeak.isHidden = true
        TF.isHidden = true
        SecondPlayerWeak.isHidden = false
        NegativeWeak.isHidden = true
        MultiTimer.invalidate()
        TF.resignFirstResponder()
        P1Time = CFT + HMW
        Timelabel.text = "Time: \(P1Time) "
        HMW = 0
        
        if P2isShadow == true {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
            SecondPlayerWeak.layer.shadowRadius = 2
            SecondPlayerWeak.layer.shadowOpacity = 0.25
        } else {
            SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
            SecondPlayerWeak.layer.shadowRadius = 0
            SecondPlayerWeak.layer.shadowOpacity = 0.0
        }
        }
    @IBAction func Pause(_ sender: UIButton) {
        MultiTimer.invalidate()
        Number1.isHidden = true
        Number2.isHidden = true
        Operation.isHidden = true
        Equals.isHidden = true
        
        if SecondPlayer == true {
            PRN = Player2Name
        } else if SecondPlayer == false {
            PRN = Player1Name
        }
        let Alert = UIAlertController(title: "Paused", message: "The Game has been paused | Number of Questions \(NOQH) out of \(NOQ) | Range is 1 - \(RangeMath) | Operation is \(OPFAV)", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Countinue", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateCounter), userInfo: nil, repeats: true)
            self.Number1.isHidden = false
            self.Number2.isHidden = false
            self.Operation.isHidden = false
            self.Equals.isHidden = false
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        }
    @IBAction func Exit(_ sender: UIButton) {
        MultiTimer.invalidate()
       
        let Alert = UIAlertController(title: "Quit?", message: "Are you sure you want to quit?", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Quit", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
          
            RandomOp = [Int]()
            NOQ = 2
            SettingCount = 0
            OpCount = 1
            a = "a"
            CountMath = 0
            ExtraCount = 1
            GoBack = false
            ChangingOp = false
            RangeMath = 10
            MultiTimer = Timer()
           
            CFT = 0
            P1Time = 1
            P2Time = 1
            TotalTime = 1
            HMW = 0
            TrialRun = false
            Player1Name = "Player 1"
            Player2Name = "Player 2"
            ChooseName = false
            SecondPlayer = false
            
            _ = self.navigationController?.popViewController(animated: true)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            MultiTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.UpdateCounter), userInfo: nil, repeats: true)
        })
        Alert.addAction(DissmissButton)
        Alert.addAction(cancel)
        
        self.present(Alert, animated: true, completion: nil)
    }
    @IBAction func Negative(_ sender: UIButton) {
        a = TF.text!
        TF.text = "-\(a)"
    }
    
    @IBAction func ChooseSecondPlayer(_ sender: UIButton) {
        if SecondPlayerWeak.currentTitle == "Rematch" {
            N1.removeAll()
            N2.removeAll()
            TF.textColor = P1Color
            Append()
            if hasTenPlayerPack == true || hasBundle == true  {
                NOPH = 1
                MultiTimeLabel.isHidden = true
            }
            P1Time = 0
            P2Time = 0
            NOQH = 0
            Player.isHidden = true
            CountMath = 0
            CFT = 0
               self.navigationController?.setNavigationBarHidden(true, animated: true)
            CountDownCount = 6
            SecondPlayerWeak.isHidden = true
            P1TimeLabel.isHidden = true
            P2TimeLabel.isHidden = true
            CDNumber.isHidden = false
            Number1.text = String(N1[0])
            Number2.text = String(N2[0])
            
            Timelabel.isHidden = true
            Player.text = Player1Name
            Player.textColor = P1Color
            if P1isShadow == true {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(5,0)
                Player.layer.shadowRadius = 2
                Player.layer.shadowOpacity = 0.25
                
            } else {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(0,0)
                Player.layer.shadowRadius = 0
                Player.layer.shadowOpacity = 0.0
            }
            SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
            SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
            if P2isShadow == true {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
                SecondPlayerWeak.layer.shadowRadius = 2
                SecondPlayerWeak.layer.shadowOpacity = 0.25
            } else {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
                SecondPlayerWeak.layer.shadowRadius = 0
                SecondPlayerWeak.layer.shadowOpacity = 0.0
            }
            
            CDLabel.text = Player1Name
            CDLabel.textColor = P1Color
            if P1isShadow == true {
                CDLabel.layer.shadowColor = P1Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(5,0)
                CDLabel.layer.shadowRadius = 2
                CDLabel.layer.shadowOpacity = 0.25
                
            } else {
                CDLabel.layer.shadowColor = P1Color.cgColor
                CDLabel.layer.shadowOffset = CGSize(0,0)
                CDLabel.layer.shadowRadius = 0
                CDLabel.layer.shadowOpacity = 0.0
            }
            CDNumber.text = "Loading"
            CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CountDownFunc), userInfo: nil, repeats: true)
            SecondPlayer = false
            
        } else {
        
            if hasTenPlayerPack == false {
                CDLabel.text = "\(Player2Name) in"
                CDLabel.textColor = P2Color
                Player.text = Player2Name
                Player.textColor = P2Color
                TF.textColor = P1Color
                if P2isShadow == true {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(5,0)
                    CDLabel.layer.shadowRadius = 2
                    CDLabel.layer.shadowOpacity = 0.25
                    
                } else {
                    CDLabel.layer.shadowColor = P2Color.cgColor
                    CDLabel.layer.shadowOffset = CGSize(0,0)
                    CDLabel.layer.shadowRadius = 0
                    CDLabel.layer.shadowOpacity = 0.0
                }
                if P2isShadow == true {
                    Player.layer.shadowColor = P2Color.cgColor
                    Player.layer.shadowOffset = CGSize(5,0)
                    Player.layer.shadowRadius = 2
                    Player.layer.shadowOpacity = 0.25
                    
                } else {
                    Player.layer.shadowColor = P2Color.cgColor
                    Player.layer.shadowOffset = CGSize(0,0)
                    Player.layer.shadowRadius = 0
                    Player.layer.shadowOpacity = 0.0
                }
                 TF.textColor = P2Color
                //MOVED EVERYTHING BELOW PLAYER@NAME IN FROM EMPTY VOID CRAP
            }
       
        NOQH = 0
            
            
         
            SecondPlayerWeak.isHidden = true
            Timelabel.isHidden = true
            RWLabel.isHidden = true
          CDLabel.isHidden = false
            CDNumber.isHidden = false
            CDNumber.text = "Loading"
            RWLabel.textColor = UIColor.black
            CountDownCount = 6
              CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CountDownFunc), userInfo: nil, repeats: true)
            CountMath = 0
            CFT = 0
            Timelabel.text = "0"
            SecondPlayer = true
           Number1.text = String(N1[CountMath])
            Number2.text = String(N2[CountMath])
            DisplayOp()
        }
        
        
        
        
        }
    
    
    
   
    
    
    
    @IBAction func SwipeDown(_ sender: Any) {
        TF.resignFirstResponder()
    }
    
    
    
}



