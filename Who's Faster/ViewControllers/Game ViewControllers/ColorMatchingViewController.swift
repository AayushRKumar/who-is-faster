//
//  FourthViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 7/22/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import AVFoundation
var CMCDCount = 6
import GoogleMobileAds

var NORH = 0
    //Number of Rounds Happened
var isCM = false
var SecondSet = false
var CMCDTimer = Timer()
var Colors1 = [UIColor]()
var Colors2 = [UIColor]()
var PRN = "Player 1"
var randomOrderSetA = [Int]()

var randomOrderSetB = [Int]()
var zeroToFour = [0, 1, 2, 3, 4]
var RandomOrderFinderInt = 0
//Player right now
var HP1P = "Not Played"
//HAS P1 Played

var HMD = 0
//How many done
var CMTimer = Timer()
//Color Matching Timer
var CMTimerCount = 0.0
var B1Color = UIColor.white
var B2Color = UIColor.white
var SecondB = false
var FirstB = ""
class FourthViewController: UIViewController, GADInterstitialDelegate {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    override func viewDidLoad() {
        
        
       
        super.viewDidLoad()
        let buttonWidth = (screenWidth / 5)
        let buttonHeight = (screenHeight / 12)
        B1aW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B1aW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        B2aW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B2aW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        B1bW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B1bW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        B2bW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B2bW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        B1cW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B1cW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        B2cW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B2cW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        B1dW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B1dW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        B2dW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B2dW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        B1eW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B1eW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        B2eW.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        B2eW.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        RandomOrderFinderInt = 0
        if doingMD == false {
        makeRandomOrders()
        }
        SecondPlayerWeak.layer.cornerRadius = 10
        SecondPlayerWeak.clipsToBounds = true
        CDTitle.adjustsFontSizeToFitWidth = true
        CDNumber.adjustsFontSizeToFitWidth = true
        MultiTimeLabel.adjustsFontSizeToFitWidth = true
        P1CMTimeLabel.adjustsFontSizeToFitWidth = true
        P2CMTimeLabel.adjustsFontSizeToFitWidth = true
        Player.adjustsFontSizeToFitWidth = true
        RWLabel.adjustsFontSizeToFitWidth = true
        SecondPlayerWeak.titleLabel?.adjustsFontSizeToFitWidth = true
       
        P1Time = 10000.0
        P2Time = 100000.0
        P3Time = 100000.0
        P4Time = 100000.0
        P5Time = 10000.0
        P6Time = 10000.0
        P7Time = 100000.0
        P8Time = 10000.0
        P9Time = 1000.0
        P10Time = 10000.0
    CMCDCount = 6
        NOPH = 1
        NORH = 0
        CMTimerCount = 0
        if hasTheme == true || hasBundle == true  {
            if CustomTheme == true {
                CMNormalFont()
                self.view.backgroundColor = PrimaryColor
                CDTitle.textColor = TextColor
                CDNumber.textColor = TextColor
                CDNumber.textColor = TextColor
                MultiTimeLabel.textColor = TextColor
                
                P1CMTimeLabel.textColor = TextColor
                P2CMTimeLabel.textColor = TextColor
                Player.textColor = TextColor
                RWLabel.textColor = TextColor
                SecondPlayerWeak.setTitleColor(SecondaryColor, for: .normal)
            } else {
            if Theme == 0 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.white
                SecondPlayerWeak.backgroundColor = UIColor.green
            } else if Theme == 1 {
                CMNormalColor()
                CDTitle.font = UIFont (name: "Marker Felt", size: 30)
                CDNumber.font = UIFont (name: "Marker Felt", size: 26)
                CDNumber.font = UIFont (name: "Marker Felt", size: 26)
                MultiTimeLabel.font = UIFont (name: "Marker Felt", size: 18)
                P1CMTimeLabel.font = UIFont (name: "Marker Felt", size: 24)
                P2CMTimeLabel.font = UIFont (name: "Marker Felt", size: 25)
                Player.font = UIFont (name: "Marker Felt", size: 24)
                RWLabel.font = UIFont (name: "Marker Felt", size: 27)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 30)
                view.backgroundColor = UIColor.darkGray
                SecondPlayerWeak.backgroundColor = UIColor.gray
                
                
            } else if Theme == 2 {
                CDTitle.font = UIFont (name: "Chalkduster", size: 30)
                CDNumber.font = UIFont (name: "Chalkduster", size: 26)
                CDNumber.font = UIFont (name: "Chalkduster", size: 26)
                MultiTimeLabel.font = UIFont (name: "Chalkduster", size: 18)
                P1CMTimeLabel.font = UIFont (name: "Chalkduster", size: 24)
                P2CMTimeLabel.font = UIFont (name: "Chalkduster", size: 25)
                Player.font = UIFont (name: "Chalkduster", size: 24)
                RWLabel.font = UIFont (name: "Chalkduster", size: 27)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 30)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                
                SecondPlayerWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
               CMWhiteColor()
            } else if Theme == 3 {
                CMNormalColor()
                CDTitle.font = UIFont (name: "Papyrus", size: 30)
                CDNumber.font = UIFont (name: "Papyrus", size: 26)
                CDNumber.font = UIFont (name: "Papyrus", size: 26)
                MultiTimeLabel.font = UIFont (name: "Papyrus", size: 18)
                P1CMTimeLabel.font = UIFont (name: "Papyrus", size: 24)
                P2CMTimeLabel.font = UIFont (name: "Papyrus", size: 25)
                Player.font = UIFont (name: "Papyrus", size: 24)
                RWLabel.font = UIFont (name: "Papyrus", size: 27)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 30)
                view.backgroundColor = UIColor.brown
                SecondPlayerWeak.backgroundColor = UIColor.white
                
            } else if Theme == 4 {
                CMNormalColor()
                CDTitle.font = UIFont (name: "Noteworthy", size: 30)
                CDNumber.font = UIFont (name: "Noteworthy", size: 26)
                CDNumber.font = UIFont (name: "Noteworthy", size: 26)
                MultiTimeLabel.font = UIFont (name: "Noteworthy", size: 18)
                P1CMTimeLabel.font = UIFont (name: "Noteworthy", size: 24)
                P2CMTimeLabel.font = UIFont (name: "Noteworthy", size: 25)
                Player.font = UIFont (name: "Noteworthy", size: 24)
                RWLabel.font = UIFont (name: "Noteworthy", size: 27)
                SecondPlayerWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 30)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                
                SecondPlayerWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
            } else if Theme == 5 {
                CMWhiteColor()
                CMNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                
                SecondPlayerWeak.backgroundColor = UIColor.white
            } else if Theme == 6 {
                CMWhiteColor()
                CMNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                
                SecondPlayerWeak.backgroundColor = UIColor.white
            } else if Theme == 7 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.red
                SecondPlayerWeak.backgroundColor = UIColor(red: (149/255.0), green: (32/255.0), blue: (38/255.0), alpha: 1.0)
            } else if Theme == 8 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.blue
                SecondPlayerWeak.backgroundColor = UIColor(red: (28/255.0), green: (88/255.0), blue: (190/255.0), alpha: 1.0)
            } else if Theme == 9 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor(red: (28/255.0), green: (255/255.0), blue: (98/255.0), alpha: 1.0)
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (255/255.0), blue: (152/255.0), alpha: 1.0)
            } else if Theme == 10 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.purple
                SecondPlayerWeak.backgroundColor = UIColor(red: (182/255.0), green: (0/255.0), blue: (239/255.0), alpha: 1.0)
            } else if Theme == 11 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.orange
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (117/255.0), blue: (0/255.0), alpha: 1.0)
            } else if Theme == 12 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
            } else if Theme == 13 {
                CMNormalFont()
                CMNormalColor()
                view.backgroundColor = UIColor.yellow
                SecondPlayerWeak.backgroundColor = UIColor(red: (255/255.0), green: (231/255.0), blue: (61/255.0), alpha: 1.0)
            }
            }
        }
        
        
        
        
        
        
        let FileLocation = Bundle.main.path(forResource: "Correct-answer", ofType: ".mp4")
        
        do {
            CorrectSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: FileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
        let WrongFileLocation = Bundle.main.path(forResource: "Wrong-answer", ofType: ".mp4")
        
        do {
            WrongSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: WrongFileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
        NOPH = 1
        
        SecondPlayer = false
        P2CMTimeLabel.isHidden = true
        P1CMTimeLabel.isHidden = true
        MultiTimeLabel.isHidden = true
        P1CMTimeLabel.textColor = P1Color
        P2CMTimeLabel.textColor = P2Color
        if P1isShadow == true {
            P1CMTimeLabel.layer.shadowColor = P1Color.cgColor
            P1CMTimeLabel.layer.shadowOffset = CGSize(5,0)
            P1CMTimeLabel.layer.shadowRadius = 2
            P1CMTimeLabel.layer.shadowOpacity = 0.25
        } else {
            P1CMTimeLabel.layer.shadowColor = P1Color.cgColor
            P1CMTimeLabel.layer.shadowOffset = CGSize(0,0)
            P1CMTimeLabel.layer.shadowRadius = 0
            P1CMTimeLabel.layer.shadowOpacity = 0.0
        }
        
        if P2isShadow == true {
            P2CMTimeLabel.layer.shadowColor = P2Color.cgColor
            P2CMTimeLabel.layer.shadowOffset = CGSize(5,0)
            P2CMTimeLabel.layer.shadowRadius = 2
            P2CMTimeLabel.layer.shadowOpacity = 0.25
        } else {
            P2CMTimeLabel.layer.shadowColor = P2Color.cgColor
            P2CMTimeLabel.layer.shadowOffset = CGSize(0,0)
            P2CMTimeLabel.layer.shadowRadius = 0
            P2CMTimeLabel.layer.shadowOpacity = 0.0
        }
        CMCDTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CMCDFunc), userInfo: nil, repeats: true)
        HideButtons()
        ChooseColors()
        SecondSet = false
        isCM = true
        RWLabel.isHidden = true
        ExitWeak.isHidden = true
        RWLabel.text = ""
        Player.isHidden = true
        if doingMD == true && isHost == true {
            CDTitle.text = "\(hostName) in"
        } else {
        CDTitle.text = "\(Player1Name) in"
        }
        CDTitle.textColor = P1Color
        if P1isShadow == true {
            CDTitle.layer.shadowColor = P1Color.cgColor
            CDTitle.layer.shadowOffset = CGSize(5,0)
            CDTitle.layer.shadowRadius = 2
            CDTitle.layer.shadowOpacity = 0.25
        } else {
            CDTitle.layer.shadowColor = P1Color.cgColor
            CDTitle.layer.shadowOffset = CGSize(0,0)
            CDTitle.layer.shadowRadius = 0
            CDTitle.layer.shadowOpacity = 0.0
        }
        PauseWeak.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       
        SecondPlayerWeak.isHidden = true
    
        B1aW.setTitle("", for: .normal)
        B1bW.setTitle("", for: .normal)
        B1cW.setTitle("", for: .normal)
        B1dW.setTitle("", for: .normal)
        B1eW.setTitle("", for: .normal)
        B2aW.setTitle("", for: .normal)
        B2bW.setTitle("", for: .normal)
        B2cW.setTitle("", for: .normal)
        B2dW.setTitle("", for: .normal)
        B2eW.setTitle("", for: .normal)
      HMW = 0
        
       
    }
    func makeRandomOrders() {
        
        var Round1A = [0, 1, 2, 3, 4].shuffled()
        var Round1B = [0, 1, 2, 3, 4].shuffled()
        var Round2A = [0, 1, 2, 3, 4].shuffled()
        var Round2B = [0, 1, 2, 3, 4].shuffled()
        var Round3A = [0, 1, 2, 3, 4].shuffled()
        var Round3B = [0, 1, 2, 3, 4].shuffled()
        var Round4A = [0, 1, 2, 3, 4].shuffled()
        var Round4B = [0, 1, 2, 3, 4].shuffled()
        var Round5A = [0, 1, 2, 3, 4].shuffled()
        var Round5B = [0, 1, 2, 3, 4].shuffled()
        var Round6A = [0, 1, 2, 3, 4].shuffled()
        var Round6B = [0, 1, 2, 3, 4].shuffled()
        var Round7A = [0, 1, 2, 3, 4].shuffled()
        var Round7B = [0, 1, 2, 3, 4].shuffled()
        var Round8A = [0, 1, 2, 3, 4].shuffled()
        var Round8B = [0, 1, 2, 3, 4].shuffled()
        var Round9A = [0, 1, 2, 3, 4].shuffled()
        var Round9B = [0, 1, 2, 3, 4].shuffled()
        var Round10A = [0, 1, 2, 3, 4].shuffled()
        var Round10B = [0, 1, 2, 3, 4].shuffled()
        for _ in 1...5 {
             randomOrderSetA.append(Round1A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round2A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round3A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round4A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round5A.remove(at: 0))
        };for _ in 1...5 {
            randomOrderSetA.append(Round6A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round7A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round8A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round9A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetA.append(Round10A.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round1B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round2B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round3B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round4B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round5B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round6B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round7B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round8B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round9B.remove(at: 0))
        }
        for _ in 1...5 {
            randomOrderSetB.append(Round10B.remove(at: 0))
        }
        print(randomOrderSetA)
        print(randomOrderSetB)
        //start again at second player
        // just have 2, 0,1 , 3, 4 NEW ROUND, 2, 0, 1, 3, 4
        //THINK ABOUT MULTILE ROUNDS AND U HAVE TO SAVE DA NUMBERS
       
    }
    func MultiPlayerHide() {
        
        CMTimerCount = (CMTimerCount * 100).rounded() / 100
        CMTimer.invalidate()
        HideButtons()
        SecondPlayerWeak.isHidden = true
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
    }
    @objc func CMTimeFunc() {
        CMTimerCount = CMTimerCount + 0.1
        CDNumber.text = String(format: "%.1f", CMTimerCount)
      
    }
    func CMNormalFont() {
        CDTitle.font = UIFont (name: "Helvetica Neue", size: 30)
        CDNumber.font = UIFont (name: "Helvetica Neue", size: 26)
        CDNumber.font = UIFont (name: "Helvetica Neue", size: 26)
        MultiTimeLabel.font = UIFont (name: "Helvetica Neue", size: 18)
        
        P1CMTimeLabel.font = UIFont (name: "Helvetica Neue", size: 24)
        P2CMTimeLabel.font = UIFont (name: "Helvetica Neue", size: 25)
        Player.font = UIFont (name: "Helvetica Neue", size: 24)
        RWLabel.font = UIFont (name: "Helvetica Neue", size: 27)
        SecondPlayerWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 30)
    }
    func CMNormalColor() {
        CDTitle.textColor = UIColor.black
        CDNumber.textColor = UIColor.black
        CDNumber.textColor = UIColor.black
        MultiTimeLabel.textColor = UIColor.black
        
        P1CMTimeLabel.textColor = UIColor.black
        P2CMTimeLabel.textColor = UIColor.black
        Player.textColor = UIColor.black
        RWLabel.textColor = UIColor.black
        SecondPlayerWeak.setTitleColor(UIColor.black, for: .normal)
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        
        popup = GADInterstitial(adUnitID: "ca-app-pub-2312248651171588/1230141252")
        popup.delegate = self
        popup.load(GADRequest())
        
        return popup
    }
    func CMWhiteColor() {
        CDTitle.textColor = UIColor.white
        CDNumber.textColor = UIColor.white
        CDNumber.textColor = UIColor.white
        MultiTimeLabel.textColor = UIColor.white
        
        P1CMTimeLabel.textColor = UIColor.white
        P2CMTimeLabel.textColor = UIColor.white
        Player.textColor = UIColor.white
        RWLabel.textColor = UIColor.white
        SecondPlayerWeak.setTitleColor(UIColor.white, for: .normal)
    }
    @objc func CMCDFunc() {
        if hasTenPlayerPack == true || hasBundle == true  {
            if CMCDCount == 1 {
                
                ShowButtons()
                RWLabel.isHidden = false
                CDTitle.isHidden = true
                CDTitle.isHidden = true
               
                CMCDCount = 6
               
                CMTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CMTimeFunc), userInfo: nil, repeats: true)
                
               CMCDTimer.invalidate()
               
                Player.isHidden = false
                if doingMD == false {
                    PauseWeak.isHidden = false
                    
                    ExitWeak.isHidden = false
                } else if doingMD == true {
                    PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                }
              
                
                if NOPH == 1 {
                    if doingMD == true && isHost == true {
                        Player.text = hostName
                    } else {
                    Player.text = "\(Player1Name)"
                    }
                    Player.textColor = P1Color
                    if P1isShadow == true {
                        Player.layer.shadowColor = P1Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P1Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    Player.text = "\(Player2Name)"
                    Player.textColor = P2Color
                    if P2isShadow == true {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P2Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 3 {
                    Player.text = "\(Player3Name)"
                    Player.textColor = P3Color
                    if P3isShadow == true {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P3Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    Player.text = "\(Player4Name)"
                    Player.textColor = P4Color
                    if P4isShadow == true {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P4Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    Player.text = "\(Player5Name)"
                    Player.textColor = P5Color
                    if P5isShadow == true {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P5Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    Player.text = "\(Player6Name)"
                    Player.textColor = P6Color
                    if P6isShadow == true {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P6Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    Player.text = "\(Player7Name)"
                    Player.textColor = P7Color
                    if P7isShadow == true {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P7Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    Player.text = "\(Player8Name)"
                    Player.textColor = P8Color
                    if P8isShadow == true {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P8Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    Player.text = "\(Player9Name)"
                    Player.textColor = P9Color
                    if P9isShadow == true {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P9Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 10 {
                    Player.text = "\(Player10Name)"
                    Player.textColor = P10Color
                    if P10isShadow == true {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(5,0)
                        Player.layer.shadowRadius = 2
                        Player.layer.shadowOpacity = 0.25
                        
                    } else {
                        Player.layer.shadowColor = P10Color.cgColor
                        Player.layer.shadowOffset = CGSize(0,0)
                        Player.layer.shadowRadius = 0
                        Player.layer.shadowOpacity = 0.0
                    }
                }
                CDNumber.text = "0"
                
                
            } else {
                CMCDCount += -1
                CDNumber.text = String(CMCDCount)
            }
            
            
            
            
        } else if hasTenPlayerPack == false {
        if SecondPlayer == true {
            
            if CMCDCount == 1 {
                CDNumber.text = "0"
                if doingMD == false {
                    PauseWeak.isHidden = false
                    
                    ExitWeak.isHidden = false
                } else if doingMD == true {
                    PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                }
                RWLabel.isHidden = false
                CDNumber.text = String(CMTimerCount)
                RWLabel.text = ""
              
                Player.text = "\(Player2Name)"
                Player.textColor = P2Color
               Player.isHidden = false
                if P2isShadow == true {
                    Player.layer.shadowColor = P2Color.cgColor
                    Player.layer.shadowOffset = CGSize(5,0)
                    Player.layer.shadowRadius = 2
                    Player.layer.shadowOpacity = 0.25
                } else {
                    Player.layer.shadowColor = P2Color.cgColor
                    Player.layer.shadowOffset = CGSize(0,0)
                    Player.layer.shadowRadius = 0
                    Player.layer.shadowOpacity = 0.0
                }
                CDTitle.textColor = UIColor.black
                CDTitle.layer.shadowColor = UIColor.white.cgColor
                NORH = 0
                HMD = 0
                ShowButtons()
                CMCDTimer.invalidate()
                CDTitle.text = "Time"
                CMCDCount = 6
                CMTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CMTimeFunc), userInfo: nil, repeats: true)
             
                
            } else {
                CMCDCount += -1
                CDNumber.text = String(CMCDCount)
                
            }
            
            
            
        } else if SecondPlayer == false {
       
        if CMCDCount == 1 {
            CDNumber.text = "0"
            if isTournament == false {
            ExitWeak.isHidden = false
            }
            RWLabel.isHidden = false
            PauseWeak.isHidden = false
            Player.isHidden = false
            if doingMD == true && isHost == true {
                Player.text = hostName
            } else {
            Player.text = "\(Player1Name)"
            }
            Player.textColor = P1Color
            if P1isShadow == true {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(5,0)
                Player.layer.shadowRadius = 2
                Player.layer.shadowOpacity = 0.25
            } else {
                Player.layer.shadowColor = P1Color.cgColor
                Player.layer.shadowOffset = CGSize(0,0)
                Player.layer.shadowRadius = 0
                Player.layer.shadowOpacity = 0.0
            }
            CDTitle.textColor = UIColor.black
            CDTitle.layer.shadowColor = UIColor.white.cgColor
      ShowButtons()
            CMTimerCount = 0
         CMCDTimer.invalidate()
          CDTitle.text = "Time"
            CMCDCount = 6
            CMTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(CMTimeFunc), userInfo: nil, repeats: true)
          
            
        } else {
             CMCDCount += -1
            CDNumber.text = String(CMCDCount)
           
        }
        }
        }
        
        
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func EC() {
        
    }
    
    func RowCheck() {
        //DUMMY 
    }
    func HideOnesJM() {
    //Hide Ones Just Images
        if FirstB == "1a" {
           
            B1aW.setImage(nil, for: .normal)
        } else if FirstB == "1b"{

            B1bW.setImage(nil, for: .normal)
        } else if FirstB == "1c" {
            B1cW.setImage(nil, for: .normal)
         
        } else if FirstB == "1d" {
            B1dW.setImage(nil, for: .normal)
            
        } else if FirstB == "1e" {
            B1eW.setImage(nil, for: .normal)
           
        }
    }
    func HideTwosJM() {
        if FirstB == "2a" {
            B2aW.setImage(nil, for: .normal)
          
        } else if FirstB == "2b"{
            B2bW.setImage(nil, for: .normal)
         
        } else if FirstB == "2c" {
            B2cW.setImage(nil, for: .normal)
            
        } else if FirstB == "2d" {
            B2dW.setImage(nil, for: .normal)
           
        } else if FirstB == "2e" {
            B2eW.setImage(nil, for: .normal)
           
        }
    }
    func HideOnes() {
        if FirstB == "1a" {
            B1aW.isHidden = true
             B1aW.setImage(nil, for: .normal)
        } else if FirstB == "1b"{
            B1bW.isHidden = true
              B1bW.setImage(nil, for: .normal)
        } else if FirstB == "1c" {
              B1cW.setImage(nil, for: .normal)
            B1cW.isHidden = true
        } else if FirstB == "1d" {
              B1dW.setImage(nil, for: .normal)
            B1dW.isHidden = true
        } else if FirstB == "1e" {
              B1eW.setImage(nil, for: .normal)
            B1eW.isHidden = true
        }
    }
    func HideTwos() {
        if FirstB == "2a" {
              B2aW.setImage(nil, for: .normal)
            B2aW.isHidden = true
        } else if FirstB == "2b"{
              B2bW.setImage(nil, for: .normal)
            B2bW.isHidden = true
        } else if FirstB == "2c" {
              B2cW.setImage(nil, for: .normal)
            B2cW.isHidden = true
        } else if FirstB == "2d" {
              B2dW.setImage(nil, for: .normal)
            B2dW.isHidden = true
        } else if FirstB == "2e" {
              B2eW.setImage(nil, for: .normal)
            B2eW.isHidden = true
        }
    }
    func HideButtons() {
        B1aW.isHidden = true
        B1bW.isHidden = true
        B1cW.isHidden = true
        B1dW.isHidden = true
        B1eW.isHidden = true
        B2aW.isHidden = true
        B2bW.isHidden = true
        B2cW.isHidden = true
        B2dW.isHidden = true
        B2eW.isHidden = true
    }
    func ShowButtons() {
        B1aW.isHidden = false
        B1bW.isHidden = false
        B1cW.isHidden = false
        B1dW.isHidden = false
        B1eW.isHidden = false
        B2aW.isHidden = false
        B2bW.isHidden = false
        B2cW.isHidden = false
        B2dW.isHidden = false
        B2eW.isHidden = false
    }
    
    func Check() {
        if hasTenPlayerPack == true || hasBundle == true {
            if B1Color == B2Color {
                RWLabel.textColor = UIColor.green
                RWLabel.text = "Right"
                CorrectSoundPlayer.play()
            }
            if HMD == 5 && NORH != NOR {
               HMD = 0
                ShowButtons()
                ChooseColors()
                SecondSet = false
                NORH += 1
            }
            
            if NOP == NOPH && NOR == NORH {
                
              MultiTimer.invalidate()
                HideButtons()
                Player.isHidden = true
                CMTimerCount = (CMTimerCount * 100).rounded() / 100
                if NOP == 10 {
                    P10Time = CMTimerCount + HMW
                } else if NOP == 9 {
                    P9Time = CMTimerCount + HMW
                } else if NOP == 8 {
                    P8Time = CMTimerCount + HMW
                } else if NOP == 7 {
                    P7Time = CMTimerCount + HMW
                } else if NOP == 6 {
                    P6Time = CMTimerCount + HMW
                } else if NOP == 5 {
                    P5Time = CMTimerCount + HMW
                } else if NOP == 4 {
                    P4Time = CMTimerCount + HMW
                } else if NOP == 3 {
                    P3Time = CMTimerCount + HMW
                } else if NOP == 2 {
                    P2Time = CMTimerCount + HMW
                }
                
                MultiTimeLabel.isHidden = false
                
                if NOP == 2 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time)"
                } else if NOP == 3 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time)"
                } else if NOP == 4 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time)"
                } else if NOP == 5 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time)"
                } else if NOP == 6 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time)"
                } else if NOP == 7 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time)"
                } else if NOP == 8 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time)"
                } else if NOP == 9 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time)"
                } else if NOP == 10 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time) | \(Player10Name)'s time was \(P10Time)"
                }
                
                MultiTimer.invalidate()
                PauseWeak.isHidden = true
                RWLabel.isHidden = true
                CDNumber.isHidden = true
                CDTitle.isHidden = false
                scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
                let min = scores.min()
                popup = createAndLoadInterstitial()

                if min == P1Time {
                    CDTitle.textColor = P1Color
                    if P1isShadow == true {
                        CDTitle.layer.shadowColor = P1Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P1Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player1Name) wins!"
                } else if min == P2Time {
                    CDTitle.textColor = P2Color
                    if P2isShadow == true {
                        CDTitle.layer.shadowColor = P2Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P2Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player2Name) wins!"
                } else if min == P3Time {
                    CDTitle.textColor = P3Color
                    if P3isShadow == true {
                        CDTitle.layer.shadowColor = P3Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P3Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player3Name) wins!"
                } else if min == P4Time {
                    CDTitle.textColor = P4Color
                    if P4isShadow == true {
                        CDTitle.layer.shadowColor = P4Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P4Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player4Name) wins!"
                } else if min == P5Time {
                    CDTitle.textColor = P5Color
                    if P5isShadow == true {
                        CDTitle.layer.shadowColor = P5Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P5Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player5Name) wins!"
                } else if min == P6Time {
                    CDTitle.textColor = P6Color
                    if P6isShadow == true {
                        CDTitle.layer.shadowColor = P6Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P6Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player6Name) wins!"
                }  else if min == P7Time {
                    CDTitle.textColor = P7Color
                    if P7isShadow == true {
                        CDTitle.layer.shadowColor = P7Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P7Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player7Name) wins!"
                }  else if min == P8Time {
                    CDTitle.textColor = P8Color
                    if P8isShadow == true {
                        CDTitle.layer.shadowColor = P8Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P8Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player8Name) wins!"
                }  else if min == P9Time {
                    CDTitle.textColor = P9Color
                    if P9isShadow == true {
                        CDTitle.layer.shadowColor = P9Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P9Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player9Name) wins!"
                }  else if min == P10Time {
                    CDTitle.textColor = P10Color
                    if P10isShadow == true {
                        CDTitle.layer.shadowColor = P10Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P10Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                    CDTitle.text = "\(Player10Name) wins!"
                    
                }
                if isTournament == false {
                    SecondPlayerWeak.setTitle("Rematch", for: .normal)
                } else {
                    SecondPlayerWeak.isHidden = true
                }
                randomOrderSetB.removeAll()
                randomOrderSetA.removeAll()
                ExitWeak.isHidden = true
                
                CDTitle.isHidden = false
                SecondPlayerWeak.isHidden = false
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                
                
                
            } else if NOR == NORH {
                CMTimerCount = (CMTimerCount * 100).rounded() / 100
                if doingMD == true {
                    P1Time = CFT + HMW
                    SeeingTimes = true
                    if isHost == true {
                        self.performSegue(withIdentifier: "goMake", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "goInvite", sender: self)
                    }
                    
                }
                MultiTimer.invalidate()
                
                HideButtons()
                CDTitle.isHidden = true
                HMD = 0
                RWLabel.isHidden = true
                PauseWeak.isHidden = true
                
                Player.isHidden = true
                ExitWeak.isHidden = true
                if NOPH == 1 {
                    P1Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
                    CDTitle.text = "\(Player2Name) in"
                    CDTitle.textColor = P2Color
                    if P2isShadow == true {
                        CDTitle.layer.shadowColor = P2Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P2Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    P2Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    
                    SecondPlayerWeak.setTitle("\(Player3Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P3Color, for: .normal)
                    CDTitle.text = "\(Player3Name) in"
                    
                    
                    CDTitle.textColor = P3Color
                    if P3isShadow == true {
                        CDTitle.layer.shadowColor = P3Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                       
                            CDTitle.layer.shadowColor = P3Color.cgColor
                            CDTitle.layer.shadowOffset = CGSize(0,0)
                            CDTitle.layer.shadowRadius = 0
                            CDTitle.layer.shadowOpacity = 0.0
                        
                    }
                } else if NOPH == 3 {
                    P3Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player4Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P4Color, for: .normal)
                    CDTitle.text = "\(Player4Name) in"
                    CDTitle.textColor = P4Color
                    if P4isShadow == true {
                        CDTitle.layer.shadowColor = P4Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P4Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    P4Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player5Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P5Color, for: .normal)
                    CDTitle.text = "\(Player5Name) in"
                    CDTitle.textColor = P5Color
                    if P5isShadow == true {
                        CDTitle.layer.shadowColor = P5Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P5Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    P5Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player6Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P6Color, for: .normal)
                    CDTitle.text = "\(Player6Name) in"
                    CDTitle.textColor = P6Color
                    if P6isShadow == true {
                        CDTitle.layer.shadowColor = P6Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P6Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    P6Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player7Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P7Color, for: .normal)
                    CDTitle.text = "\(Player7Name) in"
                    CDTitle.textColor = P7Color
                    if P7isShadow == true {
                        CDTitle.layer.shadowColor = P7Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P7Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    P7Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player8Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P8Color, for: .normal)
                    CDTitle.text = "\(Player8Name) in"
                    CDTitle.textColor = P8Color
                    if P8isShadow == true {
                        CDTitle.layer.shadowColor = P8Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P8Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    P8Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player9Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P9Color, for: .normal)
                    CDTitle.text = "\(Player9Name) in"
                    CDTitle.textColor = P9Color
                    if P9isShadow == true {
                        CDTitle.layer.shadowColor = P9Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P9Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    P9Time = CMTimerCount + HMW
                    MultiPlayerHide()
                    SecondPlayerWeak.setTitle("\(Player10Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P10Color, for: .normal)
                    CDTitle.text = "\(Player10Name) in"
                    CDTitle.textColor = P10Color
                    if P10isShadow == true {
                        CDTitle.layer.shadowColor = P10Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        
                    } else {
                        CDTitle.layer.shadowColor = P10Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(0,0)
                        CDTitle.layer.shadowRadius = 0
                        CDTitle.layer.shadowOpacity = 0.0
                    }
                }
                
                
                
                SecondPlayerWeak.isHidden = false
                
                CMCDCount = 6
                NOWH = 0
                
                HMW = 0
                NOPH += 1
               
                CMTimerCount = 0
            }
            
            
            
            
            
            
            
            
            
            
            
            
        } else if hasTenPlayerPack == false {
        if B1Color == B2Color {
            RWLabel.textColor = UIColor.green
            RWLabel.text = "Right"
            CorrectSoundPlayer.play()
        }
        if HMD == 5 {
             if NORH != NOR {
                ShowButtons()
                SecondSet = false
                ChooseColors()
                
                NORH += 1
                //  Check()
                HMD = 0
                
                
                
            }
            if SecondPlayer == false {
                if NORH == NOR {
                       CMTimerCount = (CMTimerCount * 100).rounded() / 100
                    if doingMD == true {
                        P1Time = CFT + HMW
                         SeeingTimes = true
                        if isHost == true {
                            self.performSegue(withIdentifier: "goMake", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "goInvite", sender: self)
                        }
                        
                    }
                    ExitWeak.isHidden = true
                    RWLabel.isHidden = true
                    CMTimer.invalidate()
                    P1Time = CMTimerCount + HMW
                    
                    SecondPlayerWeak.isHidden = false
                       SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
                    SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
                    if P2isShadow == true {
                        SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                        SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
                        SecondPlayerWeak.layer.shadowRadius = 2
                        SecondPlayerWeak.layer.shadowOpacity = 0.25
                    } else {
                        SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                        SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
                        SecondPlayerWeak.layer.shadowRadius = 0
                        SecondPlayerWeak.layer.shadowOpacity = 0.0
                    }
                    HideButtons()
                    PauseWeak.isHidden = true
                }
            } else if SecondPlayer == true {
             
               
                if NORH == NOR {
                    
                    ExitWeak.isHidden = true
                    RWLabel.isHidden = true
                  Player.isHidden = true
                    PauseWeak.isHidden = true
                   self.navigationController?.setNavigationBarHidden(false, animated: true)
                    SecondSet = false
                SecondPlayer = false
                      CMTimerCount = (CMTimerCount * 100).rounded() / 100
                    CMTimer.invalidate()
                    P2Time = CMTimerCount + HMW
                    HideButtons()
                    if isTournament == false {
                        SecondPlayerWeak.setTitle("Rematch", for: .normal)
                    } else {
                        SecondPlayerWeak.isHidden = true
                    }
                    CDTitle.isHidden = false
                    CDNumber.isHidden = true
                    SecondPlayerWeak.isHidden = false
                    P1CMTimeLabel.text = "\(Player1Name)'s time is \(P1Time)"
                    P2CMTimeLabel.text = "\(Player2Name)'s time is \(P2Time)"
                    P1CMTimeLabel.isHidden = false
                    P2CMTimeLabel.isHidden = false
                    popup = createAndLoadInterstitial()
                    randomOrderSetB.removeAll()
                    randomOrderSetA.removeAll()
                    if P1Time > P2Time {
                        CDTitle.text = "\(Player2Name) wins!"
                        CDTitle.textColor = P2Color
                        if P2isShadow {
                        CDTitle.layer.shadowColor = P2Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        } else {
                            CDTitle.layer.shadowColor = P2Color.cgColor
                            CDTitle.layer.shadowOffset = CGSize(0,0)
                            CDTitle.layer.shadowRadius = 0
                            CDTitle.layer.shadowOpacity = 0.0
                        }
                    } else if P1Time < P2Time {
                        CDTitle.textColor = P1Color
                        CDTitle.text = "\(Player1Name) wins!"
                        if P1isShadow {
                        
                        CDTitle.layer.shadowColor = P1Color.cgColor
                        CDTitle.layer.shadowOffset = CGSize(5,0)
                        CDTitle.layer.shadowRadius = 2
                        CDTitle.layer.shadowOpacity = 0.25
                        }  else {
                            CDTitle.layer.shadowColor = P1Color.cgColor
                            CDTitle.layer.shadowOffset = CGSize(0,0)
                            CDTitle.layer.shadowRadius = 0
                            CDTitle.layer.shadowOpacity = 0.0
                        }
                    } else if P1Time == P2Time {
                        CDTitle.text = "Tie!"
                        CDTitle.textColor = UIColor.black
                        
                    }
                    HMD = 0
                    P1Time = 0
                    P2Time = 0
                }
              
            }
        }
        
        }
    }
    func WrongCM() {
        SecondB = false
        FirstB = ""
        RWLabel.textColor = UIColor.red
        RWLabel.text = "Wrong"
        WrongSoundPlayer.play()
        HMW += 1
        CDNumber.text = "\(CMTimerCount) + \(HMW)"
    }
    func ChooseColors() {
        if CustomTheme == true {
            switch PrimaryColor {
            case UIColor.blue:
                Colors1 = [UIColor.white, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
                Colors2 = [UIColor.white, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
                
            case UIColor.green:
                Colors1 = [UIColor.blue, UIColor.white, UIColor.yellow, UIColor.red, UIColor.orange]
                Colors2 = [UIColor.blue, UIColor.white, UIColor.yellow, UIColor.red, UIColor.orange]
                
            case UIColor.yellow:
                Colors1 = [UIColor.blue, UIColor.green, UIColor.white, UIColor.red, UIColor.orange]
                Colors2 = [UIColor.blue, UIColor.green, UIColor.white, UIColor.red, UIColor.orange]
                
            case UIColor.red:
                Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.white, UIColor.orange]
                Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.white, UIColor.orange]
                
            case UIColor.orange:
                Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.white]
                Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.white]
            default:
                Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
                Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
            }
        } else {
       switch Theme {
       
       case 7:
        Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.white, UIColor.orange]
        Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.white, UIColor.orange]
        
       case 8:
        Colors1 = [UIColor.white, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
        Colors2 = [UIColor.white, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
        
       case 9:
        Colors1 = [UIColor.blue, UIColor.white, UIColor.yellow, UIColor.red, UIColor.orange]
        Colors2 = [UIColor.blue, UIColor.white, UIColor.yellow, UIColor.red, UIColor.orange]
        
       case 11:
        Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.white]
        Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.white]
        
       case 13:
        Colors1 = [UIColor.blue, UIColor.green, UIColor.white, UIColor.red, UIColor.orange]
        Colors2 = [UIColor.blue, UIColor.green, UIColor.white, UIColor.red, UIColor.orange]
        
       default:
        Colors1 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
        Colors2 = [UIColor.blue, UIColor.green, UIColor.yellow, UIColor.red, UIColor.orange]
        }
        }
        
       // B1aW.backgroundColor = Colors1.randomItem()
       // B1bW.backgroundColor = Colors1.randomItem()/
     //   B1cW.backgroundColor = Colors1.randomItem()//
       // B1dW.backgroundColor = Colors1.randomItem()
       // B1eW.backgroundColor = Colors1.randomItem()
       // SecondSet = true
      //  B2aW.backgroundColor = Colors2.randomItem()
      //  B2bW.backgroundColor = Colors2.randomItem()
      //  B2cW.backgroundColor = Colors2.randomItem()
      //  B2dW.backgroundColor = Colors2.randomItem()
      //  B2eW.backgroundColor = Colors2.randomItem()
        print("-------------------------")
        print(RandomOrderFinderInt)
        B1aW.backgroundColor = Colors1[randomOrderSetA[RandomOrderFinderInt]]
         B2aW.backgroundColor = Colors2[randomOrderSetB[RandomOrderFinderInt]]
        RandomOrderFinderInt += 1
        B1bW.backgroundColor = Colors1[randomOrderSetA[RandomOrderFinderInt]]
        B2bW.backgroundColor = Colors2[randomOrderSetB[RandomOrderFinderInt]]
        
        RandomOrderFinderInt += 1
        B1cW.backgroundColor = Colors1[randomOrderSetA[RandomOrderFinderInt]]
        B2cW.backgroundColor = Colors2[randomOrderSetB[RandomOrderFinderInt]]
        RandomOrderFinderInt += 1
        B1dW.backgroundColor = Colors1[randomOrderSetA[RandomOrderFinderInt]]
        B2dW.backgroundColor = Colors2[randomOrderSetB[RandomOrderFinderInt]]
        RandomOrderFinderInt += 1
        B1eW.backgroundColor = Colors1[randomOrderSetA[RandomOrderFinderInt]]
        B2eW.backgroundColor = Colors2[randomOrderSetB[RandomOrderFinderInt]]
        RandomOrderFinderInt += 1
        
        
    }
    @IBOutlet weak var CDTitle: UILabel!
    // CD = CountDown
    @IBOutlet weak var CDNumber: UILabel!
    
    @IBOutlet weak var MultiTimeLabel: UILabel!
    
    @IBOutlet weak var RWLabel: UILabel!
    @IBOutlet weak var SecondPlayerWeak: UIButton!
    @IBOutlet weak var B1aW: UIButton!
    @IBOutlet weak var B1bW: UIButton!
    @IBOutlet weak var B1cW: UIButton!
    @IBOutlet weak var B1dW: UIButton!
    @IBOutlet weak var B1eW: UIButton!
    @IBOutlet weak var B2aW: UIButton!
    @IBOutlet weak var B2bW: UIButton!
    @IBOutlet weak var B2cW: UIButton!
    @IBOutlet weak var B2dW: UIButton!
    @IBOutlet weak var B2eW: UIButton!
    @IBOutlet weak var Player: UILabel!
    @IBOutlet weak var PauseWeak: UIButton!
    
    @IBOutlet weak var ExitWeak: UIButton!
    @IBOutlet weak var P1CMTimeLabel: UILabel!
    
    @IBOutlet weak var P2CMTimeLabel: UILabel!
    
    @IBAction func B1a(_ sender: UIButton) {
      RowCheck()
        
        if SecondB == true {
          
            if FirstB == "1a" {
             B1aW.setImage(nil, for: .normal)
               WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
                
               HMD += 1
                  SecondB = false
                B1aW.isHidden = true
               HideTwos()
            FirstB = ""
                Check()
            } else if B1Color != B2Color {
               HideTwosJM()
                   WrongCM()
               
            }
            }
        } else if SecondB == false {
             B1aW.setImage(UIImage(named: "Rectangle"), for: .normal)
          B1aW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "1a"
           
            SecondB = true
        }
         EC()
    }
    
    
    @IBAction func B1b(_ sender: UIButton) {
         RowCheck()
        
        if SecondB == true {
            
            if FirstB == "1b" {
                 B1bW.setImage(nil, for: .normal)
                  WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
              
                  HMD += 1
                  SecondB = false
                B1bW.isHidden = true
                 HideTwos()
                 FirstB = ""
                Check()
            }else if B1Color != B2Color {
               HideTwosJM()
                   WrongCM()
               
            }
            }
        } else if SecondB == false {
           
              B1bW.setImage(UIImage(named: "Rectangle"), for: .normal)
            
             B1bW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "1b"
             SecondB = true
        }
               EC()
    }
    
    
    @IBAction func B1c(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
           
            if FirstB == "1c" {
                 B1cW.setImage(nil, for: .normal)
                WrongCM()
                
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
              
                  HMD += 1
                  SecondB = false
                B1cW.isHidden = true
                 HideTwos()
                 FirstB = ""
                 Check()
              
            }else if B1Color != B2Color {
               HideTwosJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
           
              B1cW.setImage(UIImage(named: "Rectangle"), for: .normal)
          
            B1Color = sender.backgroundColor!
            FirstB = "1c"
             SecondB = true
        }
               EC()
    }
    
    @IBAction func B1d(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
            
            if FirstB == "1d" {
                 B1dW.setImage(nil, for: .normal)
                  WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                  HMD += 1
                  SecondB = false
                B1dW.isHidden = true
                 HideTwos()
                 FirstB = ""
               Check()
            }else if B1Color != B2Color {
               HideTwosJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
           
              B1dW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B1dW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "1d"
             SecondB = true
        }
               EC()
    }

    @IBAction func B1e(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
            
            if FirstB == "1e" {
                 B1eW.setImage(nil, for: .normal)
                  WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
             HMD += 1
                  SecondB = false
                B1eW.isHidden = true
               HideTwos()
                 FirstB = ""
                Check()
            }else if B1Color != B2Color {
               HideTwosJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
            
              B1eW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B1eW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "1e"
             SecondB = true
        }
               EC()
    }

    @IBAction func B2a(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
            
            if FirstB == "2a" {
                 B2aW.setImage(nil, for: .normal)
                 WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
               
                  HMD += 1
                  SecondB = false
                B2aW.isHidden = true
               HideOnes()
                 FirstB = ""
                Check()
            }else if B1Color != B2Color {
               HideOnesJM()
                   WrongCM()
                }
            }
        } else if SecondB == false {
        
              B2aW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B2aW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "2a"
             SecondB = true
        }
               EC()
    }
    @IBAction func B2b(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
        
            if FirstB == "2b" {
                 B2bW.setImage(nil, for: .normal)
                 WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
               
                  HMD += 1
                  SecondB = false
                B2bW.isHidden = true
                 HideOnes()
                 FirstB = ""
                 Check()
            } else if B1Color != B2Color {
               HideOnesJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
        
              B2bW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B2bW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "2b"
             SecondB = true
        }
               EC()
      
    }
    
    @IBAction func B2c(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
          
            if FirstB == "2c" {
                 B2cW.setImage(nil, for: .normal)
                   WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                  HMD += 1
                  SecondB = false
                B2cW.isHidden = true
                HideOnes()
                 FirstB = ""
                Check()
            }else if B1Color != B2Color {
              HideOnesJM()
                   WrongCM()
                }
            }
        } else if SecondB == false {
     
              B2cW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B2cW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "2c"
             SecondB = true
        }
               EC()
    }
    @IBAction func B2d(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
          
            if FirstB == "2d" {
                 B2dW.setImage(nil, for: .normal)
                 WrongCM()
              
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
               
             HMD += 1
                  SecondB = false
                B2dW.isHidden = true
                HideOnes()
                 FirstB = ""
              Check()
            }else if B1Color != B2Color {
               HideOnesJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
          
              B2dW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B2dW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "2d"
             SecondB = true
        }
               EC()
    }
    
    
    @IBAction func P2e(_ sender: UIButton) {
         RowCheck()
        if SecondB == true {
           
            if FirstB == "2e" {
                 B2eW.setImage(nil, for: .normal)
                  WrongCM()
            } else {
            B2Color = sender.backgroundColor!
            if B1Color == B2Color {
                
                
                  HMD += 1
               
                  SecondB = false
                B2eW.isHidden = true
                HideOnes()
                
                 FirstB = ""
               
                Check()
            }else if B1Color != B2Color {
                HideOnesJM()
                   WrongCM()
            }
            }
        } else if SecondB == false {
         
              B2eW.setImage(UIImage(named: "Rectangle"), for: .normal)
             B2eW.layer.borderColor = UIColor.black.cgColor
            B1Color = sender.backgroundColor!
            FirstB = "2e"
             SecondB = true
        }
               EC()
    }
    
 
    
    @IBAction func SecondPlayerStrong(_ sender: UIButton) {
        if SecondPlayerWeak.currentTitle == "Rematch" {
            HideButtons()
            randomOrderSetB.removeAll()
            randomOrderSetA.removeAll()
             makeRandomOrders()
            RandomOrderFinderInt = 0
            ChooseColors()
            P1Time = 0
            CMTimer.invalidate()
            CMTimerCount = 0
            
            if hasTenPlayerPack == true || hasBundle == true  {
                NOPH = 1
                MultiTimeLabel.isHidden = true
            }
            
              self.navigationController?.setNavigationBarHidden(true, animated: true)
            P2Time = 0
            NORH = 0
            CDNumber.isHidden = false
            CMCDCount = 6
             CMCDTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CMCDFunc), userInfo: nil, repeats: true)
            SecondPlayerWeak.isHidden = true
            P1CMTimeLabel.isHidden = true
            P2CMTimeLabel.isHidden = true
            SecondPlayer = false
            CDNumber.text = "Loading"
            CDTitle.text = "\(Player1Name) in"
            CDTitle.textColor = P1Color
            if P1isShadow == true {
                CDTitle.layer.shadowColor = P1Color.cgColor
                CDTitle.layer.shadowOffset = CGSize(5,0)
                CDTitle.layer.shadowRadius = 2
                CDTitle.layer.shadowOpacity = 0.25
            }  else {
                CDTitle.layer.shadowColor = P1Color.cgColor
                CDTitle.layer.shadowOffset = CGSize(0,0)
                CDTitle.layer.shadowRadius = 0
                CDTitle.layer.shadowOpacity = 0.0
            }
            SecondPlayerWeak.setTitle("\(Player2Name)'s turn", for: .normal)
            SecondPlayerWeak.setTitleColor(P2Color, for: .normal)
            if P2isShadow == true {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(5,0)
                SecondPlayerWeak.layer.shadowRadius = 2
                SecondPlayerWeak.layer.shadowOpacity = 0.25
            } else {
                SecondPlayerWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerWeak.layer.shadowOffset = CGSize(0,0)
                SecondPlayerWeak.layer.shadowRadius = 0
                SecondPlayerWeak.layer.shadowOpacity = 0.0
            }
           
        } else {
            RandomOrderFinderInt = 0
            ChooseColors()
                HMD = 0
            NORH = 0
      CMCDTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CMCDFunc), userInfo: nil, repeats: true)
        CDNumber.text = "Loading"
        SecondPlayer = true
        SecondPlayerWeak.isHidden = true
        CDTitle.isHidden = false
            
        CDNumber.isHidden = false
        Player.isHidden = true
            if hasTenPlayerPack == false {
        CDTitle.text = "\(Player2Name) in"
                  CDTitle.textColor = P2Color
                if P2isShadow == true {
                    CDTitle.layer.shadowColor = P2Color.cgColor
                    CDTitle.layer.shadowOffset = CGSize(5,0)
                    CDTitle.layer.shadowRadius = 2
                    CDTitle.layer.shadowOpacity = 0.25
                } else {
                    CDTitle.layer.shadowColor = P2Color.cgColor
                    CDTitle.layer.shadowOffset = CGSize(0,0)
                    CDTitle.layer.shadowRadius = 0
                    CDTitle.layer.shadowOpacity = 0.0
                }
            }
      
       
        CDNumber.text = "0"
        CMTimerCount = 0
        CMCDCount = 6
       
        }
    }
    @IBAction func Pause(_ sender: UIButton) {
        CMTimer.invalidate()
        if SecondPlayer == true {
            PRN = Player2Name
            HP1P = String(P1Time)
        } else if SecondPlayer == false {
            PRN = Player1Name
        }
        let Alert = UIAlertController(title: "Paused", message: "The Game has been paused | Number of Rounds \(NORH) out of \(NOR) ", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Countinue", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            CMTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.CMTimeFunc), userInfo: nil, repeats: true)
          
        })
        
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        
        
        
        
        
        
        
        
        
    }
    
    
    @IBAction func Exit(_ sender: UIButton) {
        CMTimer.invalidate()
        let Alert = UIAlertController(title: "Quit?", message: "Are you sure you want to quit?", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Quit", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
           Colors1.removeAll()
            Colors2.removeAll()
            _ = self.navigationController?.popViewController(animated: true)
              self.navigationController?.setNavigationBarHidden(false, animated: true)
           
          
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            CMTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.CMTimeFunc), userInfo: nil, repeats: true)
            
        })
        
        Alert.addAction(DissmissButton)
        Alert.addAction(cancel)
        
        self.present(Alert, animated: true, completion: nil)
        
    }
    
    

}
