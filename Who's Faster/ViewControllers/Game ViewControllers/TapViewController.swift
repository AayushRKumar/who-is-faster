//
//  TapViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 9/3/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import MultipeerConnectivity
import GoogleMobileAds
var NOTBH = 0
var scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
var TBTimer = Timer()
var WhooshSoundPlayer:AVAudioPlayer = AVAudioPlayer()
var TBArrayNumber = 0
var TBXoffSet = [UInt32]()
var TBYoffSet = [UInt32]()
//Number of times button happend
class TapViewController: UIViewController, GADInterstitialDelegate{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if doingMD == false {
        TBXoffSet.removeAll()
        TBYoffSet.removeAll()
        appendLocations()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(TBXoffSet)
        print(TBYoffSet)
        SecondPlayerTBWeak.layer.cornerRadius = 10
        SecondPlayerTBWeak.clipsToBounds = true
        TBLabel0.adjustsFontSizeToFitWidth = true
        TBLabel1.adjustsFontSizeToFitWidth = true
        TBLabel2.adjustsFontSizeToFitWidth = true
        MultiTimeLabel.adjustsFontSizeToFitWidth = true
        SecondPlayerTBWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        NOTBH = 0
        NOPH = 0
        CFT = 0
        P1Time = 10000.0
        P2Time = 100000.0
        P3Time = 100000.0
        P4Time = 100000.0
        P5Time = 10000.0
        P6Time = 10000.0
        P7Time = 100000.0
        P8Time = 10000.0
        P9Time = 1000.0
        P10Time = 10000.0
        scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
        if hasTheme == true || hasBundle == true  {
            if CustomTheme == true {
                TBNormalFont()
                self.view.backgroundColor = PrimaryColor
                TBLabel0.textColor = TextColor
                TBLabel2.textColor = TextColor
                TBLabel1.textColor = TextColor
                TBLabel0.textColor = TextColor
                SecondPlayerTBWeak.setTitleColor(SecondaryColor, for: .normal)
                MultiTimeLabel.textColor = TextColor
            } else {
            if Theme == 0 {
                TBNormalColor()
                 TBLabel0.font = UIFont (name: "Helvetica Neue", size: 23)
                TBLabel2.font = UIFont (name: "Helvetica Neue", size: 23)
                TBLabel0.font = UIFont (name: "Helvetica Neue", size: 26)
                SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)
                MultiTimeLabel.font = UIFont (name: "Helvetica Neue", size: 18)
                TBLabel1.font = UIFont (name: "Helvetica Neue", size: 26)
               SecondPlayerTBWeak.backgroundColor = UIColor.green
                view.backgroundColor = UIColor.white
            } else if Theme == 1 {
                TBNormalColor()
                TBLabel0.font = UIFont (name: "Marker Felt", size: 23)
                TBLabel2.font = UIFont (name: "Marker Felt", size: 23)
                TBLabel0.font = UIFont (name: "Marker Felt", size: 26)
                TBLabel1.font = UIFont (name: "Marker Felt", size: 26)
                SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 23)
                MultiTimeLabel.font = UIFont (name: "Marker Felt", size: 18)
                SecondPlayerTBWeak.backgroundColor = UIColor.gray
                view.backgroundColor = UIColor.darkGray
               
                
            } else if Theme == 2 {
                TBLabel0.font = UIFont (name: "Chalkduster", size: 23)
                TBLabel2.font = UIFont (name: "Chalkduster", size: 23)
                TBLabel1.font = UIFont (name: "Chalkduster", size: 26)
                TBLabel0.font = UIFont (name: "Chalkduster", size: 26)
                SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 23)
                MultiTimeLabel.font = UIFont (name: "Chalkduster", size: 18)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (0/255.0), green: (130/255.0), blue: (0/255.0), alpha: 1.0)
                TBWhiteColor()
            } else if Theme == 3 {
                TBNormalColor()
                TBLabel0.font = UIFont (name: "Papyrus", size: 23)
                TBLabel2.font = UIFont (name: "Papyrus", size: 23)
                TBLabel1.font = UIFont (name: "Papyrus", size: 26)
                TBLabel0.font = UIFont (name: "Papyrus", size: 26)
                SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 23)
                MultiTimeLabel.font = UIFont (name: "Papyrus", size: 18)
                SecondPlayerTBWeak.backgroundColor = UIColor.white
                view.backgroundColor = UIColor.brown
            } else if Theme == 4 {
                TBNormalColor()
                TBLabel0.font = UIFont (name: "Noteworthy", size: 23)
                TBLabel2.font = UIFont (name: "Noteworthy", size: 23)
                TBLabel1.font = UIFont (name: "Noteworthy", size: 26)
                TBLabel0.font = UIFont (name: "Noteworthy", size: 26)
                SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 23)
                MultiTimeLabel.font = UIFont (name: "Noteworthy", size: 18)
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (207/255.0), green: (189/255.0), blue: (11/255.0), alpha: 1.0)
            } else if Theme == 5 {
                TBWhiteColor()
                TBNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                
                SecondPlayerTBWeak.backgroundColor = UIColor.white
            } else if Theme == 6 {
                TBWhiteColor()
                TBNormalFont()
                self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                
                SecondPlayerTBWeak.backgroundColor = UIColor.white
            } else if Theme == 7 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor.red
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (149/255.0), green: (32/255.0), blue: (38/255.0), alpha: 1.0)
            } else if Theme == 8 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor.blue
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (28/255.0), green: (88/255.0), blue: (190/255.0), alpha: 1.0)
            } else if Theme == 9 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor(red: (28/255.0), green: (255/255.0), blue: (98/255.0), alpha: 1.0)
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (182/255.0), green: (255/255.0), blue: (152/255.0), alpha: 1.0)
            } else if Theme == 10 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor.purple
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (182/255.0), green: (0/255.0), blue: (239/255.0), alpha: 1.0)
            } else if Theme == 11 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor.orange
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (255/255.0), green: (117/255.0), blue: (0/255.0), alpha: 1.0)
            } else if Theme == 12 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
            } else if Theme == 13 {
                TBNormalFont()
                TBNormalColor()
                view.backgroundColor = UIColor.yellow
                SecondPlayerTBWeak.backgroundColor = UIColor(red: (255/255.0), green: (231/255.0), blue: (61/255.0), alpha: 1.0)
            }
            }
        }
        
        MultiTimeLabel.isHidden = true
        
        
        
        let FileLocation = Bundle.main.path(forResource: "Whoosh", ofType: ".mp4")
        
        do {
            WhooshSoundPlayer = try AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: FileLocation!) as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
            
        catch {
            print(error)
        }
        NOPH = 1
         SecondPlayerTBWeak.isHidden = true
       TapWeak.isHidden = true
        PauseWeak.isHidden = true
        
        SecondPlayer = false
        ExitWeak.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        CountDownCount = 6
        CFT = 0.0
         TapWeak.frame.origin = CGPoint(x: 95, y: 335)
        SecondPlayer = false
        CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TBCDFunc), userInfo: nil, repeats: true)
        if doingMD == true && isHost == true {
            TBLabel1.text = "\(hostName) in"
        } else {
        TBLabel1.text = "\(Player1Name) in"
        }
        TBLabel1.textColor = P1Color
        if P1isShadow == true {
            TBLabel1.layer.shadowColor = P1Color.cgColor
            TBLabel1.layer.shadowOffset = CGSize(5,0)
            TBLabel1.layer.shadowRadius = 2
            TBLabel1.layer.shadowOpacity = 0.25
            
        } else {
            TBLabel1.layer.shadowColor = P1Color.cgColor
            TBLabel1.layer.shadowOffset = CGSize(0,0)
            TBLabel1.layer.shadowRadius = 0
            TBLabel1.layer.shadowOpacity = 0.0
        }
        
        SecondPlayerTBWeak.setTitle("\(Player2Name)'s turn", for: .normal)
        SecondPlayerTBWeak.setTitleColor(P2Color, for: .normal)
        if P2isShadow == true {
            SecondPlayerTBWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerTBWeak.layer.shadowOffset = CGSize(5,0)
            SecondPlayerTBWeak.layer.shadowRadius = 2
            SecondPlayerTBWeak.layer.shadowOpacity = 0.25
            
        } else {
            SecondPlayerTBWeak.layer.shadowColor = P2Color.cgColor
            SecondPlayerTBWeak.layer.shadowOffset = CGSize(0,0)
            SecondPlayerTBWeak.layer.shadowRadius = 0
            SecondPlayerTBWeak.layer.shadowOpacity = 0.0
        }
        TBLabel0.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func TBNormalFont() {
        TBLabel0.font = UIFont (name: "Helvetica Neue", size: 23)
        TBLabel2.font = UIFont (name: "Helvetica Neue", size: 23)
        TBLabel0.font = UIFont (name: "Helvetica Neue", size: 26)
        SecondPlayerTBWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)
        MultiTimeLabel.font = UIFont (name: "Helvetica Neue", size: 18)
        TBLabel1.font = UIFont (name: "Helvetica Neue", size: 26)
    }
    func TBNormalColor() {
        TBLabel0.textColor = UIColor.black
        TBLabel2.textColor = UIColor.black
        TBLabel1.textColor = UIColor.black
        TBLabel0.textColor = UIColor.black
        SecondPlayerTBWeak.setTitleColor(UIColor.black, for: .normal)
        MultiTimeLabel.textColor = UIColor.black
    }
    func TBWhiteColor() {
        TBLabel0.textColor = UIColor.white
        TBLabel2.textColor = UIColor.white
        TBLabel1.textColor = UIColor.white
        TBLabel0.textColor = UIColor.white
        SecondPlayerTBWeak.setTitleColor(UIColor.white, for: .normal)
        MultiTimeLabel.textColor = UIColor.white
    }
    @objc func TBUpdateCounter() {
        CFT = CFT + 0.1
        TBLabel2.text = String(format: "%.1f", CFT)
        
    }
    @objc func TBCDFunc() {
        if hasTenPlayerPack == true || hasBundle == true  {
            if CountDownCount == 1 {
                TBArrayNumber = 0
                TapWeak.isHidden = false
                if doingMD == false {
                    PauseWeak.isHidden = false
                    
                    ExitWeak.isHidden = false
                } else if doingMD == true {
                    PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                }
                TBTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(TBUpdateCounter), userInfo: nil, repeats: true)
               
                CountDownTimer.invalidate()
                if NOPH == 1 {
                    if doingMD == true && isHost == true {
                        TBLabel1.text = hostName
                    } else {
                    TBLabel1.text = "\(Player1Name)"
                    }
                    TBLabel1.textColor = P1Color
                    if P1isShadow == true {
                        TBLabel1.layer.shadowColor = P1Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P1Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 2 {
                    TBLabel1.text = "\(Player2Name)"
                       TBLabel1.textColor = P2Color
                    if P2isShadow == true {
                        TBLabel1.layer.shadowColor = P2Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P2Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 3 {
                    TBLabel1.text = "\(Player3Name)"
                       TBLabel1.textColor = P3Color
                    if P3isShadow == true {
                        TBLabel1.layer.shadowColor = P3Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P3Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 4 {
                    TBLabel1.text = "\(Player4Name)"
                       TBLabel1.textColor = P4Color
                    if P4isShadow == true {
                        TBLabel1.layer.shadowColor = P4Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P4Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 5 {
                    TBLabel1.text = "\(Player5Name)"
                       TBLabel1.textColor = P5Color
                    if P5isShadow == true {
                        TBLabel1.layer.shadowColor = P5Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P5Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 6 {
                    TBLabel1.text = "\(Player6Name)"
                       TBLabel1.textColor = P6Color
                    if P6isShadow == true {
                        TBLabel1.layer.shadowColor = P6Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P6Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 7 {
                    TBLabel1.text = "\(Player7Name)"
                       TBLabel1.textColor = P7Color
                    if P7isShadow == true {
                        TBLabel1.layer.shadowColor = P7Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P7Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 8 {
                    TBLabel1.text = "\(Player8Name)"
                       TBLabel1.textColor = P8Color
                    if P8isShadow == true {
                        TBLabel1.layer.shadowColor = P8Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P8Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 9 {
                    TBLabel1.text = "\(Player9Name)"
                       TBLabel1.textColor = P9Color
                    if P9isShadow == true {
                        TBLabel1.layer.shadowColor = P9Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P9Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                } else if NOPH == 10 {
                    TBLabel1.text = "\(Player10Name)"
                       TBLabel1.textColor = P10Color
                    if P10isShadow == true {
                        TBLabel1.layer.shadowColor = P10Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(5,0)
                        TBLabel1.layer.shadowRadius = 2
                        TBLabel1.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel1.layer.shadowColor = P10Color.cgColor
                        TBLabel1.layer.shadowOffset = CGSize(0,0)
                        TBLabel1.layer.shadowRadius = 0
                        TBLabel1.layer.shadowOpacity = 0.0
                    }
                }
                TBLabel2.text = "0"
                
                
            } else {
                CountDownCount += -1
                TBLabel2.text = String(CountDownCount)
            }
        } else if hasTenPlayerPack == false {
        if CountDownCount == 1 {
            if doingMD == false {
                PauseWeak.isHidden = false
                
                ExitWeak.isHidden = false
            } else if doingMD == true {
                PauseWeak.isHidden = true
                ExitWeak.isHidden = true
            }
            TBTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(TBUpdateCounter), userInfo: nil, repeats: true)
           TBArrayNumber = 0
             CountDownTimer.invalidate()
            if SecondPlayer == false {
                if doingMD == true && isHost == true {
                    TBLabel1.text = hostName
                } else {
            TBLabel1.text = "\(Player1Name)"
                }
            TBLabel2.text = "0"
            
            TapWeak.isHidden = false
            } else if SecondPlayer == true {
                TBLabel1.text = "\(Player2Name)"
                TBLabel2.text = "0"
                TapWeak.frame.origin = CGPoint(x: 95, y: 335)
                TapWeak.isHidden = false
                TBLabel1.textColor = P2Color
            }
        } else {
        CountDownCount += -1
        TBLabel2.text = String(CountDownCount)
            
        }
        
        
        }
        
        
        
        
    }
    @IBOutlet weak var TBLabel1: UILabel!
    
    @IBOutlet weak var MultiTimeLabel: UILabel!
    @IBOutlet weak var TBLabel2: UILabel!
    @IBOutlet weak var PauseWeak: UIButton!
    @IBOutlet weak var ExitWeak: UIButton!
    
    @IBOutlet weak var TapWeak: UIButton!
    
    @IBOutlet weak var SecondPlayerTBWeak: UIButton!
    
    @IBOutlet weak var TBLabel0: UILabel!
    
    func appendLocations() {
        for _ in 0...NOTBP {
            
            
            let buttonWidthD = TapWeak.frame.width //89
            let buttonHeightD = TapWeak.frame.height //88
            
            
            let viewWidthD = TapWeak.superview!.bounds.width // 375
            let viewHeightD = TapWeak.superview!.bounds.height //667
            
            
            let xwidth = viewWidthD - buttonWidthD
            let yheight = viewHeightD - buttonHeightD
            
            
            TBXoffSet.append(arc4random_uniform(UInt32(xwidth)))
            TBYoffSet.append(arc4random_uniform(UInt32(yheight)))
            
        }
        
    }
    func MultiPlayerHide() {
        TapWeak.frame.origin = CGPoint(x: 110, y: 335)
        CFT = (CFT * 100).rounded() / 100
        TBTimer.invalidate()
        TapWeak.isHidden = true
        TBLabel1.isHidden = true
        SecondPlayerTBWeak.isHidden = false
        PauseWeak.isHidden = true
        ExitWeak.isHidden = true
    }
    func TBCheck() {
        if hasTenPlayerPack == true || hasBundle == true  {
            if NOP == NOPH && NOTBH == NOTBP {
                CFT = (CFT * 100).rounded() / 100
                if NOP == 10 {
                    P10Time = CFT
                } else if NOP == 9 {
                    P9Time = CFT
                } else if NOP == 8 {
                    P8Time = CFT
                } else if NOP == 7 {
                    P7Time = CFT
                } else if NOP == 6 {
                    P6Time = CFT
                } else if NOP == 5 {
                    P5Time = CFT
                } else if NOP == 4 {
                    P4Time = CFT
                } else if NOP == 3 {
                    P3Time = CFT
                } else if NOP == 2 {
                    P2Time = CFT
                }

            MultiTimeLabel.isHidden = false
                TBLabel1.isHidden = true
                TBLabel2.isHidden = true
                if NOP == 2 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time)"
                } else if NOP == 3 {
                     MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time)"
                } else if NOP == 4 {
                      MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time)"
                } else if NOP == 5 {
                      MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time)"
                } else if NOP == 6 {
                     MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time)"
                } else if NOP == 7 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time)"
                } else if NOP == 8 {
                    MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time)"
                } else if NOP == 9 {
                     MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time)"
                } else if NOP == 10 {
                     MultiTimeLabel.text = "\(Player1Name)'s time was \(P1Time) | \(Player2Name)'s time was \(P2Time) | \(Player3Name)'s time was \(P3Time) | \(Player4Name)'s time was \(P4Time) | \(Player5Name)'s time was \(P5Time) | \(Player6Name)'s time was \(P6Time) | \(Player7Name)'s time was \(P7Time) | \(Player8Name)'s time was \(P8Time) | \(Player9Name)'s time was \(P9Time) | \(Player10Name)'s time was \(P10Time)"
                }
                TBTimer.invalidate()
                PauseWeak.isHidden = true
                 scores = [P1Time, P2Time, P3Time, P4Time, P5Time, P6Time, P7Time, P8Time, P9Time, P10Time]
                let min = scores.min()
                popup = createAndLoadInterstitial()
                if min == P1Time {
                    TBLabel0.textColor = P1Color
                    if P1isShadow == true {
                        TBLabel0.layer.shadowColor = P1Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P1Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player1Name) wins!"
                } else if min == P2Time {
                    TBLabel0.textColor = P2Color
                    if P2isShadow == true {
                        TBLabel0.layer.shadowColor = P2Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P2Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player2Name) wins!"
                } else if min == P3Time {
                    TBLabel0.textColor = P3Color
                    if P3isShadow == true {
                        TBLabel0.layer.shadowColor = P3Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P3Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player3Name) wins!"
                } else if min == P4Time {
                    TBLabel0.textColor = P4Color
                    if P4isShadow == true {
                        TBLabel0.layer.shadowColor = P4Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P4Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player4Name) wins!"
                } else if min == P5Time {
                    TBLabel0.textColor = P5Color
                    if P5isShadow == true {
                        TBLabel0.layer.shadowColor = P5Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P5Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player5Name) wins!"
                } else if min == P6Time {
                    TBLabel0.textColor = P6Color
                    if P6isShadow == true {
                        TBLabel0.layer.shadowColor = P6Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P6Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player6Name) wins!"
                }  else if min == P7Time {
                    TBLabel0.textColor = P7Color
                    if P7isShadow == true {
                        TBLabel0.layer.shadowColor = P7Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P7Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player7Name) wins!"
                }  else if min == P8Time {
                    TBLabel0.textColor = P8Color
                    if P8isShadow == true {
                        TBLabel0.layer.shadowColor = P8Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P8Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player8Name) wins!"
                }  else if min == P9Time {
                    TBLabel0.textColor = P9Color
                    if P9isShadow == true {
                        TBLabel0.layer.shadowColor = P9Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P9Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player9Name) wins!"
                }  else if min == P10Time {
                    TBLabel0.textColor = P10Color
                    if P10isShadow == true {
                        TBLabel0.layer.shadowColor = P10Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P10Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                    TBLabel0.text = "\(Player10Name) wins!"
                    
                }
                if isTournament == false {
                    SecondPlayerTBWeak.setTitle("Rematch", for: .normal)
                } else {
                    SecondPlayerTBWeak.isHidden = true
                }
                
                ExitWeak.isHidden = true
                TapWeak.isHidden = true
                TBLabel0.isHidden = false
                SecondPlayerTBWeak.isHidden = false
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                
                
            } else {
                if NOTBH == NOTBP {
                    CFT = (CFT * 100).rounded() / 100
                    if doingMD == true {
                        P1Time = CFT + HMW
                         SeeingTimes = true
                        if isHost == true {
                            self.performSegue(withIdentifier: "goMake", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "goInvite", sender: self)
                        }
                        
                    }
                    TBTimer.invalidate()
                    
                    PauseWeak.isHidden = true
                    ExitWeak.isHidden = true
                    if NOPH == 1 {
                        P1Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player2Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P2Color, for: .normal)
                        TBLabel1.text = "\(Player2Name) in"
                        TBLabel1.textColor = P2Color
                        if P2isShadow == true {
                            TBLabel1.layer.shadowColor = P2Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P2Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 2 {
                        P2Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player3Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P3Color, for: .normal)
                        TBLabel1.text = "\(Player3Name) in"
                        TBLabel1.textColor = P3Color
                        if P3isShadow == true {
                            TBLabel1.layer.shadowColor = P3Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P3Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 3 {
                       P3Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player4Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P4Color, for: .normal)
                        TBLabel1.text = "\(Player4Name) in"
                        TBLabel1.textColor = P4Color
                        if P4isShadow == true {
                            TBLabel1.layer.shadowColor = P4Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P4Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 4 {
                        P4Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player5Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P5Color, for: .normal)
                        TBLabel1.text = "\(Player5Name) in"
                        TBLabel1.textColor = P5Color
                        if P5isShadow == true {
                            TBLabel1.layer.shadowColor = P5Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P5Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 5 {
                        P5Time = CFT
                       MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player6Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P6Color, for: .normal)
                        TBLabel1.text = "\(Player6Name) in"
                        TBLabel1.textColor = P6Color
                        if P6isShadow == true {
                            TBLabel1.layer.shadowColor = P6Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P6Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 6 {
                        P6Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player7Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P7Color, for: .normal)
                        TBLabel1.text = "\(Player7Name) in"
                        TBLabel1.textColor = P7Color
                        if P7isShadow == true {
                            TBLabel1.layer.shadowColor = P7Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P7Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 7 {
                        P7Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player8Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P8Color, for: .normal)
                        TBLabel1.text = "\(Player8Name) in"
                        TBLabel1.textColor = P8Color
                        if P8isShadow == true {
                            TBLabel1.layer.shadowColor = P8Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P8Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 8 {
                        P8Time = CFT
                        MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player9Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P9Color, for: .normal)
                        TBLabel1.text = "\(Player9Name) in"
                        TBLabel1.textColor = P9Color
                        if P9isShadow == true {
                            TBLabel1.layer.shadowColor = P9Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P9Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    } else if NOPH == 9 {
                        P9Time = CFT
                       MultiPlayerHide()
                        SecondPlayerTBWeak.setTitle("\(Player10Name)'s turn", for: .normal)
                        SecondPlayerTBWeak.setTitleColor(P10Color, for: .normal)
                        TBLabel1.text = "\(Player10Name) in"
                        TBLabel1.textColor = P10Color
                        if P10isShadow == true {
                            TBLabel1.layer.shadowColor = P10Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(5,0)
                            TBLabel1.layer.shadowRadius = 2
                            TBLabel1.layer.shadowOpacity = 0.25
                            
                        } else {
                            TBLabel1.layer.shadowColor = P10Color.cgColor
                            TBLabel1.layer.shadowOffset = CGSize(0,0)
                            TBLabel1.layer.shadowRadius = 0
                            TBLabel1.layer.shadowOpacity = 0.0
                        }
                    }
                    
                    TapWeak.isHidden = true
                    TBLabel1.isHidden = true
                    SecondPlayerTBWeak.isHidden = false
                
                    CountDownCount = 6
                    NOTBH = 0
                    
                    NOPH += 1
                  
                    CFT = 0
                    
                    
                    
                    
                }
            }
            
        } else if hasTenPlayerPack == false {
        if NOTBH == NOTBP {
           
            if SecondPlayer == false {
                  CFT = (CFT * 100).rounded() / 100
                if doingMD == true {
                    P1Time = CFT + HMW
                     SeeingTimes = true
                   if isHost == true {
                        self.performSegue(withIdentifier: "goMake", sender: self)
                    } else {
                       self.performSegue(withIdentifier: "goInvite", sender: self)
                    }
                    
                }
                TBTimer.invalidate()
                TapWeak.isHidden = true
                TBLabel1.isHidden = true
                SecondPlayerTBWeak.isHidden = false
            PauseWeak.isHidden = true
                ExitWeak.isHidden = true
              
                P1Time = CFT
                
                
                TBLabel1.text = "\(Player2Name) in"
                TBLabel2.text = "Time: \(P1Time)"
                CountDownCount = 6
               NOTBH = 0
                TBLabel1.textColor = P2Color
                if P2isShadow == true {
                    TBLabel1.layer.shadowColor = P2Color.cgColor
                    TBLabel1.layer.shadowOffset = CGSize(5,0)
                    TBLabel1.layer.shadowRadius = 2
                    TBLabel1.layer.shadowOpacity = 0.25
                    
                } else {
                    TBLabel1.layer.shadowColor = P2Color.cgColor
                    TBLabel1.layer.shadowOffset = CGSize(0,0)
                    TBLabel1.layer.shadowRadius = 0
                    TBLabel1.layer.shadowOpacity = 0.0
                }
                SecondPlayer = true
                CFT = 0
            } else if SecondPlayer == true {
               TBTimer.invalidate()
                   CFT = (CFT * 100).rounded() / 100
                P2Time = CFT
                PauseWeak.isHidden = true
             
                if isTournament == false {
                    SecondPlayerTBWeak.setTitle("Rematch", for: .normal)
                } else {
                    SecondPlayerTBWeak.isHidden = true
                }
                ExitWeak.isHidden = true
                TapWeak.isHidden = true
                TBLabel0.isHidden = false
                SecondPlayerTBWeak.isHidden = false
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                popup = createAndLoadInterstitial()

                TBLabel1.text = "\(Player1Name)'s time was \(P1Time)"
                TBLabel2.text = "\(Player2Name)'s time was \(P2Time)"
                TBLabel2.textColor = P2Color
                TBLabel1.textColor = P1Color
                if P1Time > P2Time {
                    TBLabel0.text = "\(Player2Name) Wins!"
                    TBLabel0.textColor = P2Color
                    if P2isShadow == true {
                        TBLabel0.layer.shadowColor = P2Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P2Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                } else if P1Time < P2Time {
                      TBLabel0.text = "\(Player1Name) Wins!"
                    TBLabel0.textColor = P1Color
                    if P1isShadow == true {
                        TBLabel0.layer.shadowColor = P1Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(5,0)
                        TBLabel0.layer.shadowRadius = 2
                        TBLabel0.layer.shadowOpacity = 0.25
                        
                    } else {
                        TBLabel0.layer.shadowColor = P1Color.cgColor
                        TBLabel0.layer.shadowOffset = CGSize(0,0)
                        TBLabel0.layer.shadowRadius = 0
                        TBLabel0.layer.shadowOpacity = 0.0
                    }
                } else if P1Time == P2Time {
                    TBLabel0.textColor = UIColor.black
                      TBLabel0.text = "Tie!"
                }
                
            }
        }
        
        
        
        
        }
        
        
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        
        popup = GADInterstitial(adUnitID: "ca-app-pub-2312248651171588/1230141252")
        popup.delegate = self
        popup.load(GADRequest())
        
        return popup
    }
    @IBAction func Tap(_ sender: UIButton) {
        WhooshSoundPlayer.play()
        
        let buttonWidthD = TapWeak.frame.width
        let buttonHeightD = TapWeak.frame.height
        

        
       
    
        TapWeak.center.x = CGFloat(TBXoffSet[TBArrayNumber]) + buttonWidthD / 2
        TapWeak.center.y = CGFloat(TBYoffSet[TBArrayNumber]) + buttonHeightD / 2
        TBArrayNumber += 1
        NOTBH += 1
        TBCheck()
        print(NOTBH)

    }
    
    @IBAction func SecondPlayerTB(_ sender: UIButton) {
        if SecondPlayerTBWeak.currentTitle == "Rematch" {
            TapWeak.isHidden = true
            TBXoffSet.removeAll()
            TBYoffSet.removeAll()
            appendLocations()
            PauseWeak.isHidden = true
            ExitWeak.isHidden = true
            TBLabel0.isHidden = true
            SecondPlayer = false
           
            SecondPlayerTBWeak.setTitle("\(Player2Name)'s turn", for: .normal)
            SecondPlayerTBWeak.setTitleColor(P2Color, for: .normal)
            if P2isShadow == true {
                SecondPlayerTBWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerTBWeak.layer.shadowOffset = CGSize(5,0)
                SecondPlayerTBWeak.layer.shadowRadius = 2
                SecondPlayerTBWeak.layer.shadowOpacity = 0.25
                
            } else {
                SecondPlayerTBWeak.layer.shadowColor = P2Color.cgColor
                SecondPlayerTBWeak.layer.shadowOffset = CGSize(0,0)
                SecondPlayerTBWeak.layer.shadowRadius = 0
                SecondPlayerTBWeak.layer.shadowOpacity = 0.0
                
            }
           TBArrayNumber = 0
            CFT = 0
            P1Time = 0
            SecondPlayerTBWeak.isHidden = true
            P2Time = 0
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            NOTBH = 0
            CountDownCount = 6
            TBLabel2.text = "Loading"
             CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TBCDFunc), userInfo: nil, repeats: true)
            TBLabel1.text = "\(Player1Name) in"
            TBLabel1.textColor = P1Color
            if P1isShadow == true {
                TBLabel1.layer.shadowColor = P1Color.cgColor
                TBLabel1.layer.shadowOffset = CGSize(5,0)
                TBLabel1.layer.shadowRadius = 2
                TBLabel1.layer.shadowOpacity = 0.25
                
            } else {
                TBLabel1.layer.shadowColor = P1Color.cgColor
                TBLabel1.layer.shadowOffset = CGSize(0,0)
                TBLabel1.layer.shadowRadius = 0
                TBLabel1.layer.shadowOpacity = 0.0
            }
            TBLabel1.isHidden = false
            TapWeak.frame.origin = CGPoint(x: 95, y: 335)
            TBLabel2.isHidden = false
            if hasTenPlayerPack == true || hasBundle == true  {
                NOPH = 1
                MultiTimeLabel.isHidden = true
            }
            
            
        } else {
             TBArrayNumber = 0
        TBLabel2.text = "Loading"
        CountDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TBCDFunc), userInfo: nil, repeats: true)
        TBLabel1.isHidden = false
        TBLabel2.isHidden = false
        SecondPlayerTBWeak.isHidden = true
        }
        
        
    }
    
    
    @IBAction func Pause(_ sender: UIButton) {
        TBTimer.invalidate()
        if SecondPlayer == true {
            PRN = Player2Name
            HP1P = String(P1Time)
        } else if SecondPlayer == false {
            PRN = Player1Name
        }
        let Alert = UIAlertController(title: "Paused", message: "The Game has been paused |Times Button has been pressed is \(NOTBH) out of \(NOTBP)", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Countinue", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            TBTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.TBUpdateCounter), userInfo: nil, repeats: true)
            
        })
        
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func Exit(_ sender: UIButton) {
        
        TBTimer.invalidate()
        let Alert = UIAlertController(title: "Quit?", message: "Are you sure you want to quit?", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "Quit", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
           
            _ = self.navigationController?.popViewController(animated: true)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            NOTBH = 0
            SecondPlayer = false
            CFT = 0
          
            
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            TBTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.TBUpdateCounter), userInfo: nil, repeats: true)
            
        })
        
        Alert.addAction(DissmissButton)
        Alert.addAction(cancel)
        
        self.present(Alert, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
}
