//
//  DataManager.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/27/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import Foundation
 var tempForData = 1
public class DataManager {
    // get documentry directory
    static fileprivate func getDocumentDirectory () -> URL {
        if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        return url
        }else{
            fatalError("Unable Document Directory")
        }
    }
    
    
    

//save codeable objects
static func save <T:Encodable> (_ object:T, with fileName:String) {
    let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
    let encoder = JSONEncoder()
    
    do {
        let data = try encoder.encode(object)
        if FileManager.default.fileExists(atPath: url.path) {
            try FileManager.default.removeItem(at: url)
        }
        
        FileManager.default.createFile(atPath: url.path, contents: data, attributes: nil)
        
    } catch {
        fatalError(error.localizedDescription)
    }
}
    
    //load codable objects
    static func load <T:Decodable> (_ fileName:String, with type:T.Type) -> T {
        let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        if !FileManager.default.fileExists(atPath: url.path) {
            fatalError("File not found \(url.path)")
            
        }
        if let data = FileManager.default.contents(atPath: url.path) {
            do {
                let model = try JSONDecoder().decode(type, from: data)
                return model
            }catch {
                fatalError(error.localizedDescription)
            }
            
        }else {
            fatalError("Data Not there")
        }
    }
    
    
   static func loadData (_ fileName:String) -> Data? {
    let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        if !FileManager.default.fileExists(atPath: url.path) {
            fatalError("File not found \(url.path)")
            
        }
       if let data = FileManager.default.contents(atPath: url.path) {
            return data
            
        }else {
            fatalError("Data Not there")
       }
    }
    //load all files
    static func loadAll <T:Decodable> (_ type:T.Type) -> [T] {
        do {
            let files = try FileManager.default.contentsOfDirectory(atPath: getDocumentDirectory().path)
            var modelObjects = [T]()
            for fileName in files {
                modelObjects.append(load(fileName, with: type))
            }
            return modelObjects
        }catch{
            fatalError("Could not Load data")
        }
    }
    // delete a file
    static func delete (_ fileName:String) {
        let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(at: url)
            }catch{
                fatalError(error.localizedDescription)
            }
        }
    }
    static func deleteAll() {
        do {
            
        let files = try FileManager.default.contentsOfDirectory(atPath: getDocumentDirectory().path)
            
        for fileName in files {
            if fileName.contains("99DadIf1") {
                if fileName == "removeAds99DadIf1" {
                    noAds = true
                } else if fileName == "unlockBundle99DadIf1" {
                    hasBundle = true
                    noAds = true
                    hasTenPlayerPack = true
                    hasTheme =  true
                } else if fileName == "unlockTenPack99DadIf1" {
                    hasTenPlayerPack = true
                } else if fileName == "unlockThemes99DadIf1" {
                     hasTheme =  true
                }
                
               
                
            } else {
               
                
            DataManager.delete(fileName)
                print("Item Removed (\(tempForData)) File: \(fileName)")
                tempForData += 1
            }
        }
        } catch {
            print("Error Couldnt Delete All")
        }
    }
    
}




