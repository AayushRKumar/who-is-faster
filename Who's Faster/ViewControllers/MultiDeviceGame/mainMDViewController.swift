//
//  mainMDViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 12/26/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
class mainMDViewController: UIViewController, GADBannerViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        namesWeak.layer.cornerRadius = 10
        namesWeak.clipsToBounds = true
        joinWeak.layer.cornerRadius = 10
        joinWeak.clipsToBounds = true
        hostWeak.layer.cornerRadius = 10
        hostWeak.clipsToBounds = true
    //    if noAds == false {
         
        //340, 248
        
        //FRAME>SZIE>HEIGHGT IS BAD
      //  bannerView.translatesAutoresizingMaskIntoConstraints = false
      //  view.addSubview(bannerView)
        
      //  bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
      //  bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
      //   bannerView.topAnchor.constraint(equalTo: hostWeak.bottomAnchor).isActive = true
      //
     //   bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
     //   bannerView.rootViewController = self
     //   bannerView.load(GADRequest())
     //   bannerView.delegate = self
     //   }
        
        
        
        
      joinWeak.titleLabel?.adjustsFontSizeToFitWidth = true
         hostWeak.titleLabel?.adjustsFontSizeToFitWidth = true
         namesWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        normalFont()
        self.view.backgroundColor = UIColor.white
        joinWeak.backgroundColor = UIColor.purple
        namesWeak.backgroundColor = UIColor.orange
        hostWeak.backgroundColor = UIColor.purple
        if hasTheme == true {
         
                if Theme == 0 {
                    //default
                    normalFont()
                    self.view.backgroundColor = UIColor.white
                    joinWeak.backgroundColor = UIColor.purple
                    namesWeak.backgroundColor = UIColor.orange
                    hostWeak.backgroundColor = UIColor.purple
                } else if Theme == 1 {
                    //dark
                    joinWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    hostWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    namesWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    self.view.backgroundColor = UIColor(red: (192/255.0), green: (192/255.0), blue: (192/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.darkGray;
                    hostWeak.backgroundColor = UIColor.darkGray
                     namesWeak.backgroundColor = UIColor.black
                } else if Theme == 2 {
                    //chalk
                    joinWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                    hostWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                    namesWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    joinWeak.backgroundColor = UIColor.green;
                    hostWeak.backgroundColor = UIColor.green
                    namesWeak.backgroundColor = UIColor.green
                } else if Theme == 3 {
                    //anicent
                    joinWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    hostWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    namesWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.brown;
                    hostWeak.backgroundColor = UIColor.brown
                    namesWeak.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                } else if Theme == 4 {
                    //notes
                    joinWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 22)!
                    hostWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 22)!
                    namesWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 22)!
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    joinWeak.backgroundColor = UIColor.yellow;
                    hostWeak.backgroundColor = UIColor.yellow
                     namesWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 5 {
                    //space
                    normalFont()
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    joinWeak.backgroundColor = UIColor.black;
                    hostWeak.backgroundColor = UIColor.black
                    namesWeak.backgroundColor = UIColor.black
                } else if Theme == 6 {
                    //ocean
                    normalFont()
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    joinWeak.backgroundColor = UIColor.blue;
                    hostWeak.backgroundColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                } else if Theme == 7 {
                    //red
                    normalFont()
                     self.view.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.red;
                    hostWeak.backgroundColor = UIColor.red
                    namesWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.blue;
                    hostWeak.backgroundColor = UIColor.blue
                    namesWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                } else if Theme == 9 {
                    //green
                    joinWeak.backgroundColor = UIColor.green;
                    hostWeak.backgroundColor = UIColor.green
                    normalFont()
                     self.view.backgroundColor = UIColor(red: (152/255.0), green: (251/255.0), blue: (152/255.0), alpha: 1.0)
                     namesWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    joinWeak.backgroundColor = UIColor.purple;
                    hostWeak.backgroundColor = UIColor.purple
                     self.view.backgroundColor = UIColor(red: (147/255.0), green: (112/255.0), blue: (219/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.orange;
                    hostWeak.backgroundColor = UIColor.orange
                    namesWeak.backgroundColor = UIColor.orange
                } else if Theme == 12 {
                    //pink
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (190/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor(red: (255/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0);
                   hostWeak.backgroundColor = UIColor(red: (255/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                    namesWeak.backgroundColor = UIColor(red: (199/255.0), green: (21/255.0), blue: (133/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                     self.view.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                    joinWeak.backgroundColor = UIColor.yellow;
                    hostWeak.backgroundColor = UIColor.yellow
                    namesWeak.backgroundColor = UIColor.yellow
                }
            
        }
    }
    func normalFont() {
        joinWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
        hostWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
         namesWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
    }

    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    @IBOutlet weak var joinWeak: UIButton!
    
    @IBOutlet weak var hostWeak: UIButton!
    
    @IBOutlet weak var namesWeak: UIButton!
   
    
    @IBAction func Down(_ sender: UISwipeGestureRecognizer) {
       
        bannerView.frame.size.height += -10
        
    }
}
