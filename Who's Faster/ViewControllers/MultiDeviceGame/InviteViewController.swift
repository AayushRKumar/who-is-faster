//
//  InviteViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/20/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//
import MultipeerConnectivity
import UIKit
import GoogleMobileAds
var ColorToCross = UIColor.black
var gameFromHost = "Math"
var Names:MDPlayerItem!
var MDstartisHappening = false
var sendTenPlayerPackFromInvite:MDPlayerItem!
var sendThemes:MDPlayerItem!
var MDWinner = ""

// line 22
class InviteViewController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate, GADBannerViewDelegate  {
    var peerID:MCPeerID!
   var mcSession:MCSession!
    var mcAdvertiserAssistant:MCAdvertiserAssistant!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timesFromOthers.removeAll()
    namesFromOthers.removeAll()
        
        if SeeingTimes == true {
            yourNameLabel.isHidden = true
            //let mcBrowser
            
           let mcBrowser = MCBrowserViewController(serviceType: "babc", session: self.mcSession)
       mcBrowser.delegate = self
          self.present(mcBrowser, animated: true, completion: nil)
            
            SettingsLabel.isHidden = true
            ThemeLabel.isHidden = true
            WaitingLabel.text = "Waiting For Everyone To Finish"
            NameLabel.text = "Your Time: \(P1Time)"
            peoplePlayingLabel.isHidden = true
            PlayerList.isHidden = true
             FiveSecTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(HostUC), userInfo: nil, repeats: true)
         
            N1.removeAll()
            N2.removeAll()
            Words.removeAll()
            NumbersForWords.removeAll()
            TBYoffSet.removeAll()
            TBXoffSet.removeAll()
            randomOrderSetB.removeAll()
            randomOrderSetA.removeAll()
        } else {
           
            NameLabel.text = Player1Name
            yourNameLabel.isHidden = false
            NameLabel.isHidden = false
            WaitingLabel.text = "Waiting for \(gameFromHost) to start"
            SettingsLabel.isHidden = false
            ThemeLabel.isHidden = false
            PlayerList.isHidden = false
             self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if leavingForGame == false {
            mcSession.disconnect()
        }
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RandomOp.removeAll()
        NameLabel.adjustsFontSizeToFitWidth = true
        yourNameLabel.adjustsFontSizeToFitWidth = true
        WaitingLabel.adjustsFontSizeToFitWidth = true
        SettingsLabel.adjustsFontSizeToFitWidth = true
        ThemeLabel.adjustsFontSizeToFitWidth = true
        peoplePlayingLabel.adjustsFontSizeToFitWidth = true
        PlayerList.adjustsFontSizeToFitWidth = true
        N1.removeAll()
        N2.removeAll()
        if Player1Name == "" || Player1Name == "Player 1" {
            Player1Name = "Anonymous"
        }
       isHost = false
        NameLabel.text = Player1Name
       
     Forever = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(Update), userInfo: nil, repeats: true)
      
        if P1isShadow == true {
            NameLabel.layer.shadowColor = P1Color.cgColor
            NameLabel.layer.shadowOffset = CGSize(5,0)
            NameLabel.layer.shadowRadius = 2
            NameLabel.layer.shadowOpacity = 0.25
            
        }
        let NameToCross = MDPlayerItem(title: Player1Name, completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
        NameToCross.saveItem()
        Names = NameToCross
        let packToCross = MDPlayerItem(title: "sendingTenPack", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
        packToCross.saveItem()
        sendTenPlayerPackFromInvite = packToCross
        let themePackToCross = MDPlayerItem(title: "sendingTheme", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
        themePackToCross.saveItem()
        sendThemes = themePackToCross
        
        
        
        
        
        setUpConnectivity()
      //let mcBrowser
    //  let mcBrowser = MCBrowserViewController(serviceType: "babc", session: self.mcSession)
        if SeeingTimes == false {
        let mcBrowser = MCBrowserViewController(serviceType: "babc", session: self.mcSession)
        mcBrowser.delegate = self
        self.present(mcBrowser, animated: true, completion: nil)
        }
    }
    func setUpConnectivity() {
        peerID = MCPeerID(displayName: "\(Player1Name) (\(UIDevice.current.name))")
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
        
    }
    
    @IBOutlet weak var NameLabel: UILabel!
   
    @IBOutlet weak var yourNameLabel: UILabel!
    @IBOutlet weak var WaitingLabel: UILabel!
    
    @IBOutlet weak var SettingsLabel: UILabel!
    @IBOutlet weak var ThemeLabel: UILabel!
    
    @IBOutlet weak var peoplePlayingLabel: UILabel!
    
    @IBOutlet weak var PlayerList: UILabel!
    
    
    
    
    
    
    func lost() {
        let Alert = UIAlertController(title: "You Lost!", message: "Better Luck Next Time! \(MDWinner) won!", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
           self.navigationController?.setNavigationBarHidden(false, animated: true)
            FiveSecTimer.invalidate()
             numberOfDones = 0
           
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        
    }
    func won() {
        let Alert = UIAlertController(title: "You Won! ", message: "Good Job!", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            FiveSecTimer.invalidate()
             numberOfDones = 0
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
    }
    
    
    @objc func HostUC() {
        let done = MDPlayerItem(title: String(P1Time), completed: true, sendingInt: 0, sendingString: Player1Name, itemIdentifier: UUID())
         done.saveItem()
        
          DonePlayerItem = done
         sendName(DonePlayerItem)
    }
    
    
    
    
    
    @objc func Update() {
        if hasTheme == true {
        
        if Theme == 0 {
            ThemeLabel.text = "Theme: Default"
        } else if Theme == 1 {
            ThemeLabel.text = "Theme: Dark"
        } else if Theme == 2 {
            ThemeLabel.text = "Theme: Chalk"
        } else if Theme == 3 {
            ThemeLabel.text = "Theme: Anicent"
        } else if Theme == 4 {
            ThemeLabel.text = "Theme: Notes"
        } else if Theme == 5 {
            ThemeLabel.text = "Theme: Space"
        } else if Theme == 6 {
            ThemeLabel.text = "Theme: Ocean"
        } else if Theme == 7 {
            ThemeLabel.text = "Theme: Red"
        } else if Theme == 8 {
            ThemeLabel.text = "Theme: Blue"
        } else if Theme == 9 {
            ThemeLabel.text = "Theme: Green"
        } else if Theme == 10 {
            ThemeLabel.text = "Theme: Purple"
        } else if Theme == 11 {
            ThemeLabel.text = "Theme: Orange"
        } else if Theme == 12 {
            ThemeLabel.text = "Theme: Pink"
        } else if Theme == 13 {
            ThemeLabel.text = "Theme: Yellow"
        }
        
        
        
        
        } else {
             ThemeLabel.text = "Themes Not Unlocked"
        }
        
        
        if gameFromHost == "Math" {
            SettingsLabel.text = "Number of Questions: \(NOQ)"
        } else if gameFromHost == "TB" {
             SettingsLabel.text = "Number of times button must be pressed: \(NOTBP)"
        } else if gameFromHost == "CM" {
             SettingsLabel.text = "Number of Rounds: \(NOR)"
        } else if gameFromHost == "CW" {
             SettingsLabel.text = "Number of Words: \(NOW)"
        }
    }
    
    func sendName (_ MDItem:MDPlayerItem) {
        if mcSession.connectedPeers.count > 0 {
            if let MDdata = DataManager.loadData(MDItem.itemIdentifier.uuidString) {
                do {
                    try mcSession.send(MDdata, toPeers: mcSession.connectedPeers, with: .reliable)
                }catch{
                    fatalError("Could not send")
                }
            }
        }
    }
    //MARK
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case MCSessionState.connected:
            print("Connected: \(peerID.displayName)")
        //  mcBrowser.dismiss(animated: true, completion: nil)
            
        case MCSessionState.connecting:
            print("Connecting: \(peerID.displayName)")
        case MCSessionState.notConnected:
            print("Not Connected: \(peerID.displayName)")
           
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let MDItem = try JSONDecoder().decode(MDPlayerItem.self, from: data)
            
            DataManager.save(MDItem, with: MDItem.itemIdentifier.uuidString)
            DispatchQueue.main.async {
                
                if MDItem.completed == true {
                    if namesFromOthers.contains(MDItem.sendingString) {
                        print("Name Already here")
                    } else {
                        namesFromOthers.append(MDItem.sendingString)
                    }
                   
                    if timesFromOthers.contains(Double(MDItem.title)!) {
                        print("Already here")
                    } else {
                        timesFromOthers.append(Double(MDItem.title)!)
                        print("Wasnt There. TimesFromOthers: \(timesFromOthers)")
                       
                        numberOfDones += 1

                        
                        if numberOfDones == myOwnNumberOfConnectedPeers {
                            timesFromOthers.append(P1Time)
                            namesFromOthers.append(Player1Name)
                            if P1Time == timesFromOthers.min() {
                                self.won()
                            } else {
                                tempForName = 0
                                for _ in 1...myOwnNumberOfConnectedPeers {
                                    if timesFromOthers[tempForName] == timesFromOthers.min() {
                                        MDWinner = namesFromOthers[tempForName]
                                    }
                                    tempForName += 1
                                }
                                self.lost()
                            }
 
                            
                            
                        }
                        
                    }
                    
                } else {
                    switch MDItem.title {
                        
                    case "startGame" :
                        MDstartisHappening = true
                       
                            //get the nav controller here and try to push your anotherViewController
                        leavingForGame = true
                        
                        if gameFromHost == "Math" {
                            
                            self.performSegue(withIdentifier: "goMath", sender: self)
                        } else if gameFromHost == "TB" {
                            self.performSegue(withIdentifier: "goTB", sender: self)
                        } else if gameFromHost == "CM" {
                            self.performSegue(withIdentifier: "goCM", sender: self)
                        } else if gameFromHost == "CW" {
                            self.performSegue(withIdentifier: "goCW", sender: self)
                        }
                        //self
                        self.mcSession.disconnect()
              
                    case "myOwnVar" :
                        myOwnNumberOfConnectedPeers = MDItem.sendingInt
                        MDstartisHappening = false
               
                    case "sendingTenPackFromHost" :
                        tempTenPack = true
                        hasTenPlayerPack = true
                        
                    case "gamePlaying" :
                        gameFromHost = MDItem.sendingString
                        if gameFromHost == "Math" {
                            displayChosen = "Math"
                        } else if gameFromHost == "TB" {
                            displayChosen = "Tap Button"
                        } else if gameFromHost == "CM" {
                            displayChosen = "Color Matching"
                        } else if gameFromHost == "CW" {
                            displayChosen = "Copy Word"
                        }
                        self.WaitingLabel.text = "Waiting for host to start \(displayChosen)"
                        
                    case "themeCount" :
                        if hasTheme == true {
                        Theme = MDItem.sendingInt
                        
                        self.yourNameLabel.adjustsFontSizeToFitWidth = true
                        self.peoplePlayingLabel.adjustsFontSizeToFitWidth = true
                        self.SettingsLabel.adjustsFontSizeToFitWidth = true
                        self.WaitingLabel.adjustsFontSizeToFitWidth = true
                        self.ThemeLabel.adjustsFontSizeToFitWidth = true
                        
                        if Theme == 0 {
                            //default
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor.white
                            self.yourNameLabel.textColor = UIColor.black
                            self.peoplePlayingLabel.textColor = UIColor.black
                            self.SettingsLabel.textColor = UIColor.black
                            self.WaitingLabel.textColor = UIColor.black
                            self.ThemeLabel.textColor = UIColor.black
                        } else if Theme == 1 {
                            //dark
                            self.yourNameLabel.font = UIFont (name: "Marker Felt", size: 30)!
                            self.peoplePlayingLabel.font = UIFont (name: "Marker Felt", size: 30)!
                            self.SettingsLabel.font = UIFont (name: "Marker Felt", size: 30)!
                            self.WaitingLabel.font = UIFont (name: "Marker Felt", size: 30)!
                            self.ThemeLabel.font = UIFont (name: "Marker Felt", size: 30)
                            self.view.backgroundColor = UIColor(red: (192/255.0), green: (192/255.0), blue: (192/255.0), alpha: 1.0)
                            
                            self.yourNameLabel.textColor = UIColor.black
                            self.peoplePlayingLabel.textColor = UIColor.black
                            self.SettingsLabel.textColor = UIColor.black
                            self.WaitingLabel.textColor = UIColor.black
                            self.ThemeLabel.textColor = UIColor.black
                        } else if Theme == 2 {
                            //chalk
                            self.yourNameLabel.font = UIFont (name: "Chalkduster", size: 30)!
                            self.peoplePlayingLabel.font = UIFont (name: "Chalkduster", size: 30)!
                            self.SettingsLabel.font = UIFont (name: "Chalkduster", size: 30)!
                            self.WaitingLabel.font = UIFont (name: "Chalkduster", size: 30)!
                            self.ThemeLabel.font = UIFont (name: "Chalkduster", size: 30)
                            self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                            self.yourNameLabel.textColor = UIColor.white
                            self.peoplePlayingLabel.textColor = UIColor.white
                            self.SettingsLabel.textColor = UIColor.white
                            self.WaitingLabel.textColor = UIColor.white
                            self.ThemeLabel.textColor = UIColor.white
                        } else if Theme == 3 {
                            //anicent
                            self.yourNameLabel.font = UIFont (name: "Papyrus", size: 30)!
                            self.peoplePlayingLabel.font = UIFont (name: "Papyrus", size: 30)!
                            self.SettingsLabel.font = UIFont (name: "Papyrus", size: 30)!
                            self.WaitingLabel.font = UIFont (name: "Papyrus", size: 30)!
                            self.ThemeLabel.font = UIFont (name: "Papyrus", size: 30)
                            self.view.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.brown
                            self.peoplePlayingLabel.textColor = UIColor.brown
                            self.SettingsLabel.textColor = UIColor.brown
                            self.WaitingLabel.textColor = UIColor.brown
                            self.ThemeLabel.textColor = UIColor.brown
                        } else if Theme == 4 {
                            //notes
                            self.yourNameLabel.font = UIFont (name: "Noteworthy", size: 30)!
                            self.peoplePlayingLabel.font = UIFont (name: "Noteworthy", size: 30)!
                            self.SettingsLabel.font = UIFont (name: "Noteworthy", size: 30)!
                            self.WaitingLabel.font = UIFont (name: "Noteworthy", size: 30)!
                            self.ThemeLabel.font = UIFont (name: "Noteworthy", size: 30)
                            self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                            self.yourNameLabel.textColor = UIColor.black
                            self.peoplePlayingLabel.textColor = UIColor.black
                            self.SettingsLabel.textColor = UIColor.black
                            self.WaitingLabel.textColor = UIColor.black
                            self.ThemeLabel.textColor = UIColor.black
                        } else if Theme == 5 {
                            //space
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                            self.yourNameLabel.textColor = UIColor.white
                            self.peoplePlayingLabel.textColor = UIColor.white
                            self.SettingsLabel.textColor = UIColor.white
                            self.WaitingLabel.textColor = UIColor.white
                            self.ThemeLabel.textColor = UIColor.white
                        } else if Theme == 6 {
                            //ocean
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                            self.yourNameLabel.textColor = UIColor.blue
                            self.peoplePlayingLabel.textColor = UIColor.blue
                            self.SettingsLabel.textColor = UIColor.blue
                            self.WaitingLabel.textColor = UIColor.blue
                            self.ThemeLabel.textColor = UIColor.blue
                        } else if Theme == 7 {
                            //red
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.red
                            self.peoplePlayingLabel.textColor = UIColor.red
                            self.SettingsLabel.textColor = UIColor.red
                            self.WaitingLabel.textColor = UIColor.red
                            self.ThemeLabel.textColor = UIColor.red
                        } else if Theme == 8 {
                            //blue
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.blue
                            self.peoplePlayingLabel.textColor = UIColor.blue
                            self.SettingsLabel.textColor = UIColor.blue
                            self.WaitingLabel.textColor = UIColor.blue
                            self.ThemeLabel.textColor = UIColor.blue
                        } else if Theme == 9 {
                            //green
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (152/255.0), green: (251/255.0), blue: (152/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.green
                            self.peoplePlayingLabel.textColor = UIColor.green
                            self.SettingsLabel.textColor = UIColor.green
                            self.WaitingLabel.textColor = UIColor.green
                            self.ThemeLabel.textColor = UIColor.green
                        } else if Theme == 10 {
                            //purple
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (147/255.0), green: (112/255.0), blue: (219/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.purple
                            self.peoplePlayingLabel.textColor = UIColor.purple
                            self.SettingsLabel.textColor = UIColor.purple
                            self.WaitingLabel.textColor = UIColor.purple
                            self.ThemeLabel.textColor = UIColor.purple
                        } else if Theme == 11 {
                            //orange
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.white
                            self.peoplePlayingLabel.textColor = UIColor.white
                            self.SettingsLabel.textColor = UIColor.white
                            self.WaitingLabel.textColor = UIColor.white
                            self.ThemeLabel.textColor = UIColor.white
                        } else if Theme == 12 {
                            //pink
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (190/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                            self.peoplePlayingLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                            self.SettingsLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                            self.WaitingLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                            self.ThemeLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                        } else if Theme == 13 {
                            //yellow
                            self.MDINormalFont()
                            self.view.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                            self.yourNameLabel.textColor = UIColor.yellow
                            self.peoplePlayingLabel.textColor = UIColor.yellow
                            self.SettingsLabel.textColor = UIColor.yellow
                            self.WaitingLabel.textColor = UIColor.yellow
                            self.ThemeLabel.textColor = UIColor.yellow
                        }
                        }
                    case "questionCount" :
                        if gameFromHost == "Math" {
                            NOQ = MDItem.sendingInt
                            
                        } else if gameFromHost == "CW" {
                            NOW = MDItem.sendingInt
                            
                        } else if gameFromHost == "CM" {
                            NOR = MDItem.sendingInt
                            
                        } else if gameFromHost == "TB" {
                            NOTBP = MDItem.sendingInt
                            
                        }
                    case "N1Values" :
            
                        N1.append(MDItem.sendingInt)
                   
                        
                    case "N2Values" :
                         N2.append(MDItem.sendingInt)
                         
                    case "changingOp" :
                        ChangingOp = true
                        
                        RandomOp.append(MDItem.sendingInt)
                        
                    case "op" :
                         OpCount = MDItem.sendingInt
                    
                    case "word" :
                         Words.append(MDItem.sendingString)
                         
                    case "sendNumbersForWords" :
                         NumbersForWords.append(MDItem.sendingInt)
                        
                    case "theHostName" :
                 hostName = MDItem.sendingString
                 
                    case "forYourPlayerList" :
                         self.PlayerList.text = MDItem.sendingString + "\(hostName) (Your Host)"
                         
                    case "XoffSets" :
                     
                    TBXoffSet.append(UInt32(MDItem.sendingInt))
                  
                    
                    case "YoffSets" :
                
                    TBYoffSet.append(UInt32(MDItem.sendingInt))
                    
                    case "randomOrderSetA" :
                         randomOrderSetA.append(MDItem.sendingInt)
                        
                    case "randomOrderSetB" :
               
                     randomOrderSetB.append(MDItem.sendingInt)
                    
                    case "hostLeft" :
                        
                        let Alert = UIAlertController(title: "The Host Left", message: "You have to leave", preferredStyle: .alert)
                        let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                            (alert: UIAlertAction!) -> Void in
                             self.performSegue(withIdentifier: "toMD", sender: self)
                        })
                        Alert.addAction(DissmissButton)
                        self.present(Alert, animated: true, completion: nil)
                    default:
                        print("")
                }
                    
                   
                }
                //Brace before is end of Else
            }
           
        }catch{
            fatalError("UNABLE TO PROCCESS DATA")
        }
        
        
    }
    func MDINormalFont() {
        yourNameLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        peoplePlayingLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        SettingsLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        WaitingLabel.font = UIFont (name: "Helvetica Neue", size: 30)!
        ThemeLabel.font = UIFont (name: "Helvetica Neue", size: 30)
    }
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated:true, completion: nil)
        WaitingLabel.text = "Waiting for host to start \(gameFromHost)"
        sendName(Names)
       
        if hasTenPlayerPack == true {
            sendName(sendTenPlayerPackFromInvite)
        }
        if SeeingTimes == true {
             SeeingTimes = false
            leavingForGame = false
        }
        if hasTheme == true {
           
        }
        if hasTenPlayerPack == false {
            if mcSession.connectedPeers.count > 1 {
                let Alert = UIAlertController(title: "You Can't Join!", message: "You can not join, you or the host must have the Ten player Pack!", preferredStyle: .alert)
                let DissmissButton = UIAlertAction(title: "Buy", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                    //self
                    self.mcSession.disconnect()
                
                    self.performSegue(withIdentifier: "goShop", sender: self)
                    
                    
                    
                })
                let cancel = UIAlertAction(title: "Go Back", style: .destructive, handler: { (action) -> Void in
                    _ = self.navigationController?.popViewController(animated: true)
                    //self
                    self.mcSession.disconnect()
                })
                
                Alert.addAction(DissmissButton)
                Alert.addAction(cancel)
                
                self.present(Alert, animated: true, completion: nil)
                
            }
        }
            
        
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
         dismiss(animated:true, completion: nil)
        if SeeingTimes == false {
    
        
        self.performSegue(withIdentifier: "toMD", sender: self)
        } else {
            let Alert = UIAlertController(title: "Are you sure?", message: "If the connection did not not work you can try again", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "Try Again", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                let mcBrowser = MCBrowserViewController(serviceType: "babc", session: self.mcSession)
                mcBrowser.delegate = self
                self.present(mcBrowser, animated: true, completion: nil)
                
                
                
            })
            let cancel = UIAlertAction(title: "Go Back", style: .destructive, handler: { (action) -> Void in
                self.performSegue(withIdentifier: "toMD", sender: self)
            })
            
            Alert.addAction(DissmissButton)
            Alert.addAction(cancel)
            
            self.present(Alert, animated: true, completion: nil)
        }
    }
    

}

