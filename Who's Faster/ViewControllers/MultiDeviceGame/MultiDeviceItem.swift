//
//  MultiDeviceItem.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/27/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import Foundation
import CloudKit
struct MDPlayerItem : Codable {
    var title:String
    var completed:Bool
   // var createdAt:Date
    var sendingInt:Int
    
    var sendingString:String
    var itemIdentifier:UUID
   
    func saveItem() {
        
        DataManager.save(self, with: itemIdentifier.uuidString)
    }
    func deleteItem() {
        DataManager.delete(itemIdentifier.uuidString)
    }
    
    func saveAdsItem() {
         DataManager.save(self, with: "removeAds99DadIf1")
    }
    func saveBundleItem() {
        DataManager.save(self, with: "unlockBundle99DadIf1")
    }
    func saveTenPackItem() {
        DataManager.save(self, with: "unlockTenPack99DadIf1")
    }
    func saveThemesItem() {
        DataManager.save(self, with: "unlockThemes99DadIf1")
    }
}

