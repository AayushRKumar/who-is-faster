//
//  MakeGameViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/13/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import GoogleMobileAds
var Game:MDPlayerItem!
var SendingTheme:MDPlayerItem!
var SendingSetting:MDPlayerItem!
var sendStart:MDPlayerItem!
var sendQuestions:MDPlayerItem!
var DonePlayerItem:MDPlayerItem!
var myOwnConnectedPeersItem:MDPlayerItem!
var sendTenPlayerPackFromHost:MDPlayerItem!
var sendQuestion:MDPlayerItem!
var sendPlayerList:MDPlayerItem!
var sendHostName:MDPlayerItem!
var leavingSession:MDPlayerItem!
var tempTheme = false
var tempTenPack = false
var myOwnNumberOfConnectedPeers = 0
var FiveSecTimer = Timer()
var numberOfDones = 0
var timesFromOthers = [Double]()
var namesFromOthers = [String]()
var isHost = false
var SeeingTimes = false
var MDCount = 1
var hostName = ""
var tempForName = 0
var leavingForGame = false
var leavingToChooseGame = false
var leavingToChooseTheme = false
var SettingChosen = MDPlayerItem(title: "", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
var questionToCross = MDPlayerItem(title: "", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
class MakeGameViewController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate, GADBannerViewDelegate {
    
    var peerID:MCPeerID!
    var mcSession:MCSession!
    var mcAdvertiserAssistant:MCAdvertiserAssistant!
    
    
   
   
    
    
//-----------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if leavingForGame == false && leavingToChooseGame == false && leavingToChooseTheme == false {
            print("Leaving")
            questionToCross = MDPlayerItem(title: "hostLeft", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
            questionToCross.saveItem()
            sendQuestion = questionToCross
            sendGame(sendQuestion)
            mcAdvertiserAssistant.stop()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if hasTheme == false {
            ThemeWeak.isHidden = true
        }
        leavingToChooseTheme = false
        leavingToChooseGame = false
        ChooseGameWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        ThemeWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        StartWeakButton.titleLabel?.adjustsFontSizeToFitWidth = true
        PlayerList.adjustsFontSizeToFitWidth = true
        youLabel.adjustsFontSizeToFitWidth = true
        hostNameLabel.adjustsFontSizeToFitWidth = true
        ThemeLabel.adjustsFontSizeToFitWidth = true
        SettingsLabel.adjustsFontSizeToFitWidth = true
        settingsColonLabel.adjustsFontSizeToFitWidth = true
        GameLabel.adjustsFontSizeToFitWidth = true
        normalFont()
        self.view.backgroundColor = UIColor.white
        PlayerList.textColor = UIColor.black
        youLabel.textColor = UIColor.black
        PlayerList.textColor = UIColor.black
        ThemeLabel.textColor = UIColor.black
        SettingsLabel.textColor = UIColor.black
        settingsColonLabel.textColor = UIColor.black
        GameLabel.textColor = UIColor.black
        ChooseGameWeak.backgroundColor = UIColor.orange
        StartWeakButton.backgroundColor = UIColor.orange
        ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
        if hasTheme == true {
            
                if Theme == 0 {
                    //default
                     normalFont()
                    self.view.backgroundColor = UIColor.white
                    PlayerList.textColor = UIColor.black
                    youLabel.textColor = UIColor.black
                    PlayerList.textColor = UIColor.black
                    ThemeLabel.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    GameLabel.textColor = UIColor.black
                    ChooseGameWeak.backgroundColor = UIColor.orange
                    StartWeakButton.backgroundColor = UIColor.orange
                    ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                } else if Theme == 1 {
                    //dark
                    ChooseGameWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 25)
                    ThemeWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 25)
                    StartWeakButton.titleLabel?.font = UIFont (name: "Marker Felt", size: 25)
                    PlayerList.font = UIFont (name: "Marker Felt", size: 25)
                    youLabel.font = UIFont (name: "Marker Felt", size: 25)
                    hostNameLabel.font = UIFont (name: "Marker Felt", size: 25)
                    ThemeLabel.font = UIFont (name: "Marker Felt", size: 25)
                    SettingsLabel.font = UIFont (name: "Marker Felt", size: 25)
                    settingsColonLabel.font = UIFont (name: "Marker Felt", size: 25)
                    GameLabel.font = UIFont (name: "Marker Felt", size: 25)
                    self.view.backgroundColor = UIColor(red: (192/255.0), green: (192/255.0), blue: (192/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.black
                    youLabel.textColor = UIColor.black
                    PlayerList.textColor = UIColor.black
                    ThemeLabel.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    GameLabel.textColor = UIColor.black
                    ChooseGameWeak.backgroundColor = UIColor.black
                    StartWeakButton.backgroundColor = UIColor.black
                    ThemeWeak.backgroundColor = UIColor.black
                } else if Theme == 2 {
                    //chalk
                    ChooseGameWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 25)
                    ThemeWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 25)
                    StartWeakButton.titleLabel?.font = UIFont (name: "Chalkduster", size: 25)
                    PlayerList.font = UIFont (name: "Chalkduster", size: 25)
                    youLabel.font = UIFont (name: "Chalkduster", size: 25)
                    hostNameLabel.font = UIFont (name: "Chalkduster", size: 25)
                    ThemeLabel.font = UIFont (name: "Chalkduster", size: 25)
                    SettingsLabel.font = UIFont (name: "Chalkduster", size: 25)
                    settingsColonLabel.font = UIFont (name: "Chalkduster", size: 25)
                    GameLabel.font = UIFont (name: "Chalkduster", size: 25)
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    PlayerList.textColor = UIColor.white
                    youLabel.textColor = UIColor.white
                    PlayerList.textColor = UIColor.white
                    ThemeLabel.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    GameLabel.textColor = UIColor.white
                    ChooseGameWeak.backgroundColor = UIColor.green
                        StartWeakButton.backgroundColor = UIColor.green
                    ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 3 {
                    //anicent
                    ChooseGameWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 25)
                    ThemeWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 25)
                    StartWeakButton.titleLabel?.font = UIFont (name: "Papyrus", size: 25)
                    PlayerList.font = UIFont (name: "Papyrus", size: 25)
                    youLabel.font = UIFont (name: "Papyrus", size: 25)
                    hostNameLabel.font = UIFont (name: "Papyrus", size: 25)
                    ThemeLabel.font = UIFont (name: "Papyrus", size: 25)
                    SettingsLabel.font = UIFont (name: "Papyrus", size: 25)
                    settingsColonLabel.font = UIFont (name: "Papyrus", size: 25)
                    GameLabel.font = UIFont (name: "Papyrus", size: 25)
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.brown
                    youLabel.textColor = UIColor.brown
                    PlayerList.textColor = UIColor.brown
                    ThemeLabel.textColor = UIColor.brown
                    SettingsLabel.textColor = UIColor.brown
                    settingsColonLabel.textColor = UIColor.brown
                    GameLabel.textColor = UIColor.brown
                    ChooseGameWeak.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor.brown
                } else if Theme == 4 {
                    //notes
                    ChooseGameWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 25)
                    ThemeWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 25)
                    StartWeakButton.titleLabel?.font = UIFont (name: "Noteworthy", size: 25)
                    PlayerList.font = UIFont (name: "Noteworthy", size: 25)
                    youLabel.font = UIFont (name: "Noteworthy", size: 25)
                    hostNameLabel.font = UIFont (name: "Noteworthy", size: 25)
                    ThemeLabel.font = UIFont (name: "Noteworthy", size: 25)
                    SettingsLabel.font = UIFont (name: "Noteworthy", size: 25)
                    settingsColonLabel.font = UIFont (name: "Noteworthy", size: 25)
                    GameLabel.font = UIFont (name: "Noteworthy", size: 25)
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    PlayerList.textColor = UIColor.black
                    youLabel.textColor = UIColor.black
                    PlayerList.textColor = UIColor.black
                    ThemeLabel.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                    settingsColonLabel.textColor = UIColor.black
                    GameLabel.textColor = UIColor.black
                    ChooseGameWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 5 {
                    //space
                     normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    PlayerList.textColor = UIColor.white
                    youLabel.textColor = UIColor.white
                    PlayerList.textColor = UIColor.white
                    ThemeLabel.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    GameLabel.textColor = UIColor.white
                    ChooseGameWeak.backgroundColor = UIColor.black
                        StartWeakButton.backgroundColor = UIColor.black
                    ThemeWeak.backgroundColor = UIColor.black
                } else if Theme == 6 {
                    //ocean
                     normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    PlayerList.textColor = UIColor.blue
                    youLabel.textColor = UIColor.blue
                    PlayerList.textColor = UIColor.blue
                    ThemeLabel.textColor = UIColor.blue
                    SettingsLabel.textColor = UIColor.blue
                    settingsColonLabel.textColor = UIColor.blue
                    GameLabel.textColor = UIColor.blue
                    ChooseGameWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (72/255.0), green: (209/255.0), blue: (204/255.0), alpha: 1.0)
                } else if Theme == 7 {
                    //red
                     normalFont()
                    self.view.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.red
                    youLabel.textColor = UIColor.red
                    PlayerList.textColor = UIColor.red
                    ThemeLabel.textColor = UIColor.red
                    SettingsLabel.textColor = UIColor.red
                    settingsColonLabel.textColor = UIColor.red
                    GameLabel.textColor = UIColor.red
                    ChooseGameWeak.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (205/255.0), green: (92/255.0), blue: (92/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                } else if Theme == 8 {
                    //blue
                     normalFont()
                    self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.blue
                    youLabel.textColor = UIColor.blue
                    PlayerList.textColor = UIColor.blue
                    ThemeLabel.textColor = UIColor.blue
                    SettingsLabel.textColor = UIColor.blue
                    settingsColonLabel.textColor = UIColor.blue
                    GameLabel.textColor = UIColor.blue
                    ChooseGameWeak.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (127/255.0), green: (255/255.0), blue: (212/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                } else if Theme == 9 {
                    //green
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (152/255.0), green: (251/255.0), blue: (152/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.green
                    youLabel.textColor = UIColor.green
                    PlayerList.textColor = UIColor.green
                    ThemeLabel.textColor = UIColor.green
                    SettingsLabel.textColor = UIColor.green
                    settingsColonLabel.textColor = UIColor.green
                    GameLabel.textColor = UIColor.green
                    ChooseGameWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                } else if Theme == 10 {
                    //purple
                     normalFont()
                    self.view.backgroundColor = UIColor(red: (147/255.0), green: (112/255.0), blue: (219/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.purple
                    youLabel.textColor = UIColor.purple
                    PlayerList.textColor = UIColor.purple
                    ThemeLabel.textColor = UIColor.purple
                    SettingsLabel.textColor = UIColor.purple
                    settingsColonLabel.textColor = UIColor.purple
                    GameLabel.textColor = UIColor.purple
                    ChooseGameWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                } else if Theme == 11 {
                    //orange
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.white
                    youLabel.textColor = UIColor.white
                    PlayerList.textColor = UIColor.white
                    ThemeLabel.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                    settingsColonLabel.textColor = UIColor.white
                    GameLabel.textColor = UIColor.white
                    ChooseGameWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 12 {
                    //pink
                     normalFont()
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (190/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    youLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    ThemeLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    settingsColonLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    GameLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    ChooseGameWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                        StartWeakButton.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                     self.view.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                    PlayerList.textColor = UIColor.yellow
                    youLabel.textColor = UIColor.yellow
                    PlayerList.textColor = UIColor.yellow
                    ThemeLabel.textColor = UIColor.yellow
                    SettingsLabel.textColor = UIColor.yellow
                    settingsColonLabel.textColor = UIColor.yellow
                    GameLabel.textColor = UIColor.yellow
                    ChooseGameWeak.backgroundColor = UIColor.yellow
                        StartWeakButton.backgroundColor = UIColor.yellow
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                }
            
        }
        
        

      timesFromOthers.removeAll()
       namesFromOthers.removeAll()
      
        if SeeingTimes == true {
            MDCount = 1
            leavingForGame = false
            youLabel.isHidden = true
            hostNameLabel.isHidden = true
            GameLabel.isHidden = true
            SettingsLabel.text = "Waiting For Everyone To Finish"
            ThemeLabel.isHidden = true
           settingsColonLabel.isHidden = true
            ChooseGameWeak.isHidden = true
            ThemeWeak.isHidden = true
            if StartWeakButton != nil {
                StartWeakButton.isHidden = true
            }
                PlayerList.text = "Your Time: \(P1Time)"
            
            FiveSecTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(HostUC), userInfo: nil, repeats: true)
        N1.removeAll()
            N2.removeAll()
            Words.removeAll()
           NumbersForWords.removeAll()
            TBXoffSet.removeAll()
            TBYoffSet.removeAll()
            if N1.isEmpty && N2.isEmpty {
                for _ in 0..<50 {
                    N1.append(Int(arc4random_uniform(UInt32(RangeMath))))
                    N2.append(Int(arc4random_uniform(UInt32(RangeMath))))
                }
            }
            Words.removeAll()
            for _ in 0..<100 {
                Words.append(randomWord())
            }
            for _ in 0..<50 {
                NumbersForWords.append(Int(arc4random_uniform(UInt32(100))))
            }
            
        } else {
            
            if N1.isEmpty && N2.isEmpty {
                for _ in 0..<50 {
                    N1.append(Int(arc4random_uniform(UInt32(RangeMath))))
                    N2.append(Int(arc4random_uniform(UInt32(RangeMath))))
                }
            }
            Words.removeAll()
            for _ in 0..<100 {
                Words.append(randomWord())
            }
            for _ in 0..<50 {
                NumbersForWords.append(Int(arc4random_uniform(UInt32(100))))
            }
            setUpGame()
            
            
            
        }
      
        
        
    }
    func normalFont() {
        ChooseGameWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 25)
         ThemeWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 25)
         StartWeakButton.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 25)
        PlayerList.font = UIFont (name: "Helvetica Neue", size: 25)
        youLabel.font = UIFont (name: "Helvetica Neue", size: 25)
         hostNameLabel.font = UIFont (name: "Helvetica Neue", size: 25)
        ThemeLabel.font = UIFont (name: "Helvetica Neue", size: 25)
        SettingsLabel.font = UIFont (name: "Helvetica Neue", size: 25)
         settingsColonLabel.font = UIFont (name: "Helvetica Neue", size: 25)
         GameLabel.font = UIFont (name: "Helvetica Neue", size: 25)
        
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       leavingForGame = false
        StartWeakButton.layer.cornerRadius = 10
        StartWeakButton.clipsToBounds = true
        ThemeWeak.layer.cornerRadius = 10
        ThemeWeak.clipsToBounds = true
        ChooseGameWeak.layer.cornerRadius = 10
        ChooseGameWeak.clipsToBounds = true
        youLabel.adjustsFontSizeToFitWidth = true
        hostNameLabel.adjustsFontSizeToFitWidth = true
        settingsColonLabel.adjustsFontSizeToFitWidth = true
        GameLabel.adjustsFontSizeToFitWidth = true
        SettingsLabel.adjustsFontSizeToFitWidth = true
        PlayerList.adjustsFontSizeToFitWidth = true
        ThemeLabel.adjustsFontSizeToFitWidth = true
       isHost = true
        if hostName == "" {
        hostName = Player1Name
        }
        if hostName == "" || hostName == "Player 1" || hostName == "Player1Name" {
            hostName = "Anonymous"
        }
        let tenPackToCross = MDPlayerItem(title: "sendingTenPackFromHost", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
        tenPackToCross.saveItem()
        sendTenPlayerPackFromHost = tenPackToCross
        Player1Name = ""
         Player2Name = ""
         Player3Name = ""
         Player4Name = ""
         Player5Name = ""
         Player6Name = ""
        
        Forever = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
        
        setUpConnectivity()
        if hasTheme == false {
            ThemeWeak.isHidden = true
        }
        
        while mcAdvertiserAssistant != nil {
           mcAdvertiserAssistant.stop()
        }
       
        self.mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "babc", discoveryInfo: nil, session: self.mcSession)
    
        
        
        self.mcAdvertiserAssistant.start()
        
        
        
        // Do any additional setup after loading the view.
    }
    @objc func HostUC() {
        let done = MDPlayerItem(title: String(P1Time), completed: true, sendingInt: 0, sendingString: hostName, itemIdentifier: UUID())
        
       done.saveItem()
        DonePlayerItem = done
        sendGame(DonePlayerItem)
        
       
    }
    @IBOutlet weak var StartWeakButton: UIButton!
    
    
    @IBOutlet weak var youLabel: UILabel!
    
    @IBOutlet weak var hostNameLabel: UILabel!
   
    
    @IBOutlet weak var settingsColonLabel: UILabel!
    
    @IBOutlet weak var GameLabel: UILabel!
    
    @IBOutlet weak var SettingsLabel: UILabel!
    @IBOutlet weak var PlayerList: UILabel!
    
    @IBOutlet weak var ThemeWeak: UIButton!
    
    @IBOutlet weak var ChooseGameWeak: UIButton!
    
    @IBOutlet weak var ThemeLabel: UILabel!
    func setUpGame() {
          navigationController?.setNavigationBarHidden(false, animated: true)
        if GameLabel != nil && ThemeLabel != nil && ChooseGameWeak != nil && ThemeWeak != nil && StartWeakButton != nil && youLabel != nil && hostNameLabel != nil {
            GameLabel.isHidden = false
            settingsColonLabel.isHidden = false
            ThemeLabel.isHidden = false
            PlayerList.isHidden = false
            ChooseGameWeak.isHidden = false
            if hasTheme == true || hasBundle == true {
            ThemeWeak.isHidden = false
            }
            StartWeakButton.isHidden = false
            youLabel.isHidden = false
           
            hostNameLabel.isHidden = false
        }
        hostNameLabel.text = hostName
        GameLabel.text = "Game Chosen: \(displayChosen)"
        if GameChosen == "Math" {
            SettingsLabel.text = "Number of Questions: \(NOQ) | Range: \(RangeMath) | Operation: \(OPFAV)"
            
        } else if GameChosen == "TB" {
            SettingsLabel.text = "Number of Times Button must be pressed: \(NOTBP)"
        } else if GameChosen == "CM" {
            SettingsLabel.text = "Number of Rounds: \(NOR)"
        } else if GameChosen == "CW" {
            SettingsLabel.text = "Number of Words: \(NOW)"
            
        }
        
        
        if hasTheme == false {
            ThemeLabel.isHidden = true
        } else {
            
            if Theme == 0 {
                ThemeLabel.text = "Theme: Default"
            } else if Theme == 1 {
                ThemeLabel.text = "Theme: Dark"
            } else if Theme == 2 {
                ThemeLabel.text = "Theme: Chalk"
            } else if Theme == 3 {
                ThemeLabel.text = "Theme: Anicent"
            } else if Theme == 4 {
                ThemeLabel.text = "Theme: Notes"
            } else if Theme == 5 {
                ThemeLabel.text = "Theme: Space"
            } else if Theme == 6 {
                ThemeLabel.text = "Theme: Ocean"
            } else if Theme == 7 {
                ThemeLabel.text = "Theme: Red"
            } else if Theme == 8 {
                ThemeLabel.text = "Theme: Blue"
            } else if Theme == 9 {
                ThemeLabel.text = "Theme: Green"
            } else if Theme == 10 {
                ThemeLabel.text = "Theme: Purple"
            } else if Theme == 11 {
                ThemeLabel.text = "Theme: Orange"
            } else if Theme == 12 {
                ThemeLabel.text = "Theme: Pink"
            } else if Theme == 13 {
                ThemeLabel.text = "Theme: Yellow"
            }
        }
        
        
        
        sendBasic()
    }
    func sendBasic() {
        let hostNameThing = MDPlayerItem(title: "theHostName", completed: false, sendingInt: 0, sendingString: hostName, itemIdentifier: UUID())
        hostNameThing.saveItem()
        sendHostName = hostNameThing
        sendGame(sendHostName)
        let peopleSend = MDPlayerItem(title: "forYourPlayerList", completed: false, sendingInt: 0, sendingString: PlayerList.text!, itemIdentifier: UUID())
        peopleSend.saveItem()
        sendPlayerList = peopleSend
        sendGame(sendPlayerList)
        let GameChose = MDPlayerItem(title: "gamePlaying", completed: false, sendingInt: 0, sendingString: GameChosen, itemIdentifier: UUID())
        GameChose.saveItem()
        
        let ThemeChosen = MDPlayerItem(title: "themeCount", completed: false, sendingInt: Theme, sendingString: "", itemIdentifier: UUID())
        ThemeChosen.saveItem()
        if GameChosen == "Math" {
            SettingChosen = MDPlayerItem(title: "questionCount", completed: false, sendingInt: NOQ, sendingString: "", itemIdentifier: UUID())
            
        } else if GameChosen == "TB" {
            SettingChosen = MDPlayerItem(title: "questionCount", completed: false, sendingInt: NOTBP, sendingString: "", itemIdentifier: UUID())
            
        } else if GameChosen == "CM" {
            SettingChosen = MDPlayerItem(title: "questionCount", completed: false, sendingInt: NOR, sendingString: "", itemIdentifier: UUID())
            
        } else if GameChosen == "CW" {
            SettingChosen = MDPlayerItem(title: "questionCount", completed: false, sendingInt: NOW, sendingString: "", itemIdentifier: UUID())
            
        }
        SettingChosen.saveItem()
        
        if mcSession.connectedPeers.count > 0 {
            self.sendGame(sendTenPlayerPackFromHost)
            Game = GameChose
            sendGame(Game)
            SendingTheme = ThemeChosen
            sendGame(SendingTheme)
            SendingSetting = SettingChosen
            sendGame(SendingSetting)
        }
        
        
        //How Can we Send Questions? Perhaps the titles go qForMath, qForCW, ...... And then in sendString or Int we write the thing. Then for how many questions we repeat it. We can use for _ in _. For Changing OP we can "changingOP"
        if GameChosen == "Math" {
            //got to pass N1, N2, Operation, Changing OP WOW HARDCORE
            for number in N1 {
                questionToCross = MDPlayerItem(title: "N1Values", completed: false, sendingInt: number, sendingString: "", itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
            
            for number in N2 {
                questionToCross = MDPlayerItem(title: "N2Values", completed: false, sendingInt: number, sendingString: "", itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
            
            if ChangingOp == true {
                for op in RandomOp {
                    questionToCross = MDPlayerItem(title: "changingOp", completed: false, sendingInt: op, sendingString: "", itemIdentifier: UUID())
                    questionToCross.saveItem()
                    sendQuestion = questionToCross
                    sendGame(sendQuestion)
                }
            } else {
                questionToCross = MDPlayerItem(title: "op", completed: false, sendingInt: OpCount, sendingString: "", itemIdentifier: UUID())
                
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
        } else if GameChosen == "CW" {
            for word in Words {
                questionToCross = MDPlayerItem(title: "word", completed: false, sendingInt: 0, sendingString: word, itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
            for numbers in NumbersForWords {
                questionToCross = MDPlayerItem(title: "sendNumbersForWords", completed: false, sendingInt: numbers, sendingString: "", itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
        } else if GameChosen == "TB" {
            TBYoffSet.removeAll()
            TBXoffSet.removeAll()
            appendButtons()
            
        } else if GameChosen == "CM" {
            randomOrderSetA.removeAll()
            randomOrderSetB.removeAll()
            var Round1A = [0, 1, 2, 3, 4].shuffled()
            var Round1B = [0, 1, 2, 3, 4].shuffled()
            var Round2A = [0, 1, 2, 3, 4].shuffled()
            var Round2B = [0, 1, 2, 3, 4].shuffled()
            var Round3A = [0, 1, 2, 3, 4].shuffled()
            var Round3B = [0, 1, 2, 3, 4].shuffled()
            var Round4A = [0, 1, 2, 3, 4].shuffled()
            var Round4B = [0, 1, 2, 3, 4].shuffled()
            var Round5A = [0, 1, 2, 3, 4].shuffled()
            var Round5B = [0, 1, 2, 3, 4].shuffled()
            var Round6A = [0, 1, 2, 3, 4].shuffled()
            var Round6B = [0, 1, 2, 3, 4].shuffled()
            var Round7A = [0, 1, 2, 3, 4].shuffled()
            var Round7B = [0, 1, 2, 3, 4].shuffled()
            var Round8A = [0, 1, 2, 3, 4].shuffled()
            var Round8B = [0, 1, 2, 3, 4].shuffled()
            var Round9A = [0, 1, 2, 3, 4].shuffled()
            var Round9B = [0, 1, 2, 3, 4].shuffled()
            var Round10A = [0, 1, 2, 3, 4].shuffled()
            var Round10B = [0, 1, 2, 3, 4].shuffled()
            for _ in 1...5 {
                randomOrderSetA.append(Round1A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round2A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round3A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round4A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round5A.remove(at: 0))
            };for _ in 1...5 {
                randomOrderSetA.append(Round6A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round7A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round8A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round9A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetA.append(Round10A.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round1B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round2B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round3B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round4B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round5B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round6B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round7B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round8B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round9B.remove(at: 0))
            }
            for _ in 1...5 {
                randomOrderSetB.append(Round10B.remove(at: 0))
            }
            for interger in randomOrderSetA {
                questionToCross = MDPlayerItem(title: "randomOrderSetA", completed: false, sendingInt: interger, sendingString: "", itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
            
            for interger in randomOrderSetB {
                questionToCross = MDPlayerItem(title: "randomOrderSetB", completed: false, sendingInt: interger, sendingString: "", itemIdentifier: UUID())
                questionToCross.saveItem()
                sendQuestion = questionToCross
                sendGame(sendQuestion)
            }
            print(randomOrderSetA)
            print(randomOrderSetB)
            //start again at second player
            // just have 2, 0,1 , 3, 4 NEW ROUND, 2, 0, 1, 3, 4
            //THINK ABOUT MULTILE ROUNDS AND U HAVE TO SAVE DA NUMBERS
        }
    }
    func appendButtons() {
        for _ in 0...NOTBP {
            
            
            let buttonWidthD = 89
            let buttonHeightD = 88
            
            
            let viewWidthD = 375
            let viewHeightD = 667
            
            
            let xwidth = viewWidthD - buttonWidthD
            let yheight = viewHeightD - buttonHeightD
            
            
            TBXoffSet.append(arc4random_uniform(UInt32(xwidth)))
            TBYoffSet.append(arc4random_uniform(UInt32(yheight)))
            
        }
        for location in TBXoffSet {
            questionToCross = MDPlayerItem(title: "XoffSets", completed: false, sendingInt: Int(location), sendingString: "", itemIdentifier: UUID())
            questionToCross.saveItem()
            sendQuestion = questionToCross
            sendGame(sendQuestion)
        }
        for location in TBYoffSet {
            questionToCross = MDPlayerItem(title: "YoffSets", completed: false, sendingInt: Int(location), sendingString: "", itemIdentifier: UUID())
            questionToCross.saveItem()
            sendQuestion = questionToCross
            sendGame(sendQuestion)
        }
    }
    func lost() {
        let Alert = UIAlertController(title: "You Lost!", message: "Better Luck Next Time! \(MDWinner) won!", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.setUpGame()
            FiveSecTimer.invalidate()
            numberOfDones = 0
            SeeingTimes = false
            self.mcAdvertiserAssistant.stop()
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
        
    }
    func won() {
        let Alert = UIAlertController(title: "You Won!", message: "Good Job!", preferredStyle: .alert)
        let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.setUpGame()
            FiveSecTimer.invalidate()
             numberOfDones = 0
            SeeingTimes = false
            self.mcAdvertiserAssistant.stop()
            
        })
        Alert.addAction(DissmissButton)
        self.present(Alert, animated: true, completion: nil)
    }
    
    
    func doNothing() {
        
    }
    
    
    
    
    @objc func UpdateCounter() {
         PlayerList.text = "\(Player1Name), \(Player2Name), \(Player3Name), \(Player4Name), \(Player5Name), \(Player6Name) "
        
    }
    func randomWord(wordLength: Int = 6) -> String {
        
        let kCons = 1
        let kVows = 2
        
        var cons: [String] = [
            // single consonants. Beware of Q, it"s often awkward in words
            "b", "c", "d", "f", "g", "h", "j", "k", "l", "m",
            "n", "p", "r", "s", "t", "v", "w", "x", "z",
            // possible combinations excluding those which cannot start a word
            "pt", "gl", "gr", "ch", "ph", "ps", "sh", "st", "th", "wh"
        ]
        
        // consonant combinations that cannot start a word
        let cons_cant_start: [String] = [
            "ck", "cm",
            "dr", "ds",
            "ft",
            "gh", "gn",
            "kr", "ks",
            "ls", "lt", "lr",
            "mp", "mt", "ms",
            "ng", "ns",
            "rd", "rg", "rs", "rt",
            "ss",
            "ts", "tch"
        ]
        
        var vows : [String] = [
            // single vowels
            "a", "e", "i", "o", "u", "y",
            // vowel combinations your language allows
            "ee", "oa", "oo",
            ]
        
        // start by vowel or consonant ?
        var current = (Int(arc4random_uniform(2)) == 1 ? kCons : kVows );
        
        var word : String = ""
        while ( word.count < wordLength ){
            // After first letter, use all consonant combos
            if word.count == 2 {
                cons = cons + cons_cant_start
            }
            
            // random sign from either $cons or $vows
            var rnd: String = "";
            var index: Int;
            if current == kCons {
                index = Int(arc4random_uniform(UInt32(cons.count)))
                rnd = cons[index]
            } else if current == kVows {
                index = Int(arc4random_uniform(UInt32(vows.count)))
                rnd = vows[index]
            }
            
            
            // check if random sign fits in word length
            //    var tempWord = "\(word)\(rnd)"
            if( word.count <= wordLength ) {
                word = "\(word)\(rnd)"
                // alternate sounds
                current = ( current == kCons ) ? kVows : kCons;
            }
            
            
            //PLZ WORLD PLZZ
        }
        
        
        
        return word
    }
    
    
    
    func setUpConnectivity() {
        peerID = MCPeerID(displayName: "\(hostName) (\(UIDevice.current.name))")
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
    
        mcSession.delegate = self
    
    }
    
    func sendGame (_ MDItem:MDPlayerItem) {
        if mcSession.connectedPeers.count > 0 {
            if let MDdata = DataManager.loadData(MDItem.itemIdentifier.uuidString) {
                do {
                    try mcSession.send(MDdata, toPeers: mcSession.connectedPeers, with: .reliable)
                }catch{
                    fatalError("Could not send")
                }
            }
        }
    }
    
    //MARK:
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        
            do {
            let MDItem = try JSONDecoder().decode(MDPlayerItem.self, from: data)
                DataManager.save(MDItem, with: MDItem.itemIdentifier.uuidString)
                DispatchQueue.main.async {
                    if MDItem.completed == true {
                        //if time is a new one that is not in the array. Append IT. Else Do nothing
                        if namesFromOthers.contains(MDItem.sendingString) {
                            print("Name Already here")
                        } else {
                            namesFromOthers.append(MDItem.sendingString)
                        }
                        if timesFromOthers.contains(Double(MDItem.title)!) {
                            print("Already here")
                        } else {
                             timesFromOthers.append(Double(MDItem.title)!)
                            print("Wasnt There. TimesFromOthers: \(timesFromOthers)")
                            numberOfDones += 1
                           print("Dones \(numberOfDones)")
                            if numberOfDones == myOwnNumberOfConnectedPeers {
                                
                               timesFromOthers.append(P1Time)
                                namesFromOthers.append(hostName)
                                if P1Time == timesFromOthers.min() {
                                    self.won()
                                } else {
                                   tempForName = 0
                                    for _ in 1...myOwnNumberOfConnectedPeers {
                                        if timesFromOthers[tempForName] == timesFromOthers.min() {
                                            MDWinner = namesFromOthers[tempForName]
                                        }
                                       tempForName += 1
                                    }
                                    self.lost()
                                }
 
                                
                            }
                            
                        }
                    
                       
                        
                      
                    } else {
                        self.sendBasic()
                        
                        if MDItem.title == "sendingTheme" {
                       ////   tempTheme = true
                         //   if SeeingTimes == false {
                         //       if hasTheme == true || hasBundle == true {
                         //   self.ThemeWeak.isHidden = false
                        //        }
                         //   }
                        } else if MDItem.title == "sendingTenPack" {
                            hasTenPlayerPack = true
                            tempTenPack = true
                              self.sendGame(sendTenPlayerPackFromHost)
                        } else {
                            
                    if MDCount == 1 {
                        MDCount += 1
                        Player1Name = MDItem.title
                    } else if MDCount == 2 {
                        if hasTenPlayerPack == true || tempTenPack == true {
                           MDCount += 1
                         Player2Name = MDItem.title
                        }
                    } else if MDCount == 3 {
                           MDCount += 1
                         Player3Name = MDItem.title
                    } else if MDCount == 4 {
                           MDCount += 1
                         Player4Name = MDItem.title
                    } else if MDCount == 5 {
                           MDCount += 1
                         Player5Name = MDItem.title
                    } else if MDCount == 6 {
                           MDCount += 1
                         Player6Name = MDItem.title
                    }
                        
                        }
                        
                        
                        
                        
                    }
                }
                
            }catch{
                fatalError("UNABLE TO PROCCESS DATA!!!!!!!!!!!!!!!")
            }
       
        
        
        
    }
    
    
    
    
    @IBAction func start(_ sender: UIButton) {
        if mcSession.connectedPeers.count > 0 {
        isHost = true
        leavingForGame = true
        
            //GET RID OF ^^
       myOwnNumberOfConnectedPeers = mcSession.connectedPeers.count
        print("MY OWN VAR \(myOwnNumberOfConnectedPeers)")
        let startSignal = MDPlayerItem(title: "startGame", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
        startSignal.saveItem()
        sendStart = startSignal
        sendGame(sendStart)
        let myOwnVarSignal = MDPlayerItem(title: "myOwnVar", completed: false, sendingInt: myOwnNumberOfConnectedPeers, sendingString: "", itemIdentifier: UUID())
        myOwnVarSignal.saveItem()
        myOwnConnectedPeersItem = myOwnVarSignal
        sendGame(myOwnConnectedPeersItem)
       
        if GameChosen == "Math" {
            
            self.performSegue(withIdentifier: "goMath", sender: self)
        } else if GameChosen == "TB" {
            self.performSegue(withIdentifier: "goTB", sender: self)
        } else if GameChosen == "CM" {
            self.performSegue(withIdentifier: "goCM", sender: self)
        } else if GameChosen == "CW" {
            self.performSegue(withIdentifier: "goCW", sender: self)
        }
      mcSession.disconnect()
        mcAdvertiserAssistant.stop()
        
        } else {
            let Alert = UIAlertController(title: "Can't Start!", message: "Have someone else join the game", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    @IBAction func ChooseGame(_ sender: UIButton) {
        leavingToChooseGame = true
    }
    
    
    @IBAction func ChooseTheme(_ sender: UIButton) {
        leavingToChooseTheme = true
    }
    
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        
    }
    
    
}
