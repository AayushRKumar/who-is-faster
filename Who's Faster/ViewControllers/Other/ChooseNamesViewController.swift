//
//  SecondViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 7/9/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
 var selectedValue = ""
var pickerData: [String] = [String]()
var NameCount = 1
var P1Color = UIColor.black
var P2Color = UIColor.black
var P3Color = UIColor.black
var P4Color = UIColor.black
var P5Color = UIColor.black
var P6Color = UIColor.black
var P7Color = UIColor.black
var P8Color = UIColor.black
var P9Color = UIColor.black
var P10Color = UIColor.black
var P1isShadow = false
var P2isShadow = false
var P3isShadow = false
var P4isShadow = false
var P5isShadow = false
var P6isShadow = false
var P7isShadow = false
var P8isShadow = false
var P9isShadow = false
var P10isShadow = false
var Forever = Timer()
var P1HT = false
var P2HT = false
var P3HT = false
var P4HT = false
var P5HT = false
var P6HT = false
var P7HT = false
var P8HT = false
var P9HT = false
var P10HT = false
//Player X Has Typed
var P1RC = 0
var P2RC = 0
var P3RC = 0
var P4RC = 0
var P5RC = 0
var P6RC = 0
var P7RC = 0
var P8RC = 0
var P9RC = 0
var P10RC = 0
//Player X Row Color
var Player3Name = "Player 3"
var Player4Name = "Player 4"
var Player5Name = "Player 5"
var Player6Name = "Player 6"
var Player7Name = "Player 7"
var Player8Name = "Player 8"
var Player9Name = "Player 9"
var Player10Name = "Player 10"
class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, GADBannerViewDelegate {
    @IBOutlet weak var NameTFWeak: UITextField!
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        if noAds == false {
            
            
            // addBannerViewToView(bannerView)
        
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
             bannerView.topAnchor.constraint(equalTo: NameP1Weak.bottomAnchor).isActive = true
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
        if hasTheme == true {
            if CustomTheme == true {
                self.view.backgroundColor = PrimaryColor
                normalFont()
                NameTitle.textColor = TextColor
                shadowLabel.textColor = TextColor
                colorsWeak.textColor = TextColor
                NameTFWeak.backgroundColor = TextColor
                
            } else {
                if Theme == 0 {
                    //default
                     normalFont()
                    self.view.backgroundColor = UIColor.white
                NameTitle.textColor = UIColor.black
                    shadowLabel.textColor = UIColor.black
                    colorsWeak.textColor = UIColor.black
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 1 {
                    //dark
                    NameTitle.font = UIFont (name: "Marker Felt", size: 20)!
                    shadowLabel.font = UIFont (name: "Marker Felt", size: 20)!
                    colorsWeak.font = UIFont (name: "Marker Felt", size: 20)!
                    NameTFWeak.font = UIFont (name: "Marker Felt", size: 20)!
                    DisplayName.font = UIFont (name: "Marker Felt", size: 30)!
                    
                    self.view.backgroundColor = UIColor(red: (192/255.0), green: (192/255.0), blue: (192/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.black
                    shadowLabel.textColor = UIColor.black
                    colorsWeak.textColor = UIColor.black
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 2 {
                    //chalk
                    NameTitle.font = UIFont (name: "Chalkduster", size: 20)!
                    shadowLabel.font = UIFont (name: "Chalkduster", size: 20)!
                    colorsWeak.font = UIFont (name: "Chalkduster", size: 20)!
                    NameTFWeak.font = UIFont (name: "Chalkduster", size: 20)!
                    DisplayName.font = UIFont (name: "Chalkduster", size: 30)!
                     self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    NameTitle.textColor = UIColor.white
                    shadowLabel.textColor = UIColor.white
                    colorsWeak.textColor = UIColor.white
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.white, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.white, for: .normal)
                } else if Theme == 3 {
                    //anicent
                    NameTitle.font = UIFont (name: "Papyrus", size: 20)!
                    shadowLabel.font = UIFont (name: "Papyrus", size: 20)!
                    colorsWeak.font = UIFont (name: "Papyrus", size: 20)!
                    NameTFWeak.font = UIFont (name: "Papyrus", size: 20)!
                    DisplayName.font = UIFont (name: "Papyrus", size: 30)!
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (228/255.0), blue: (181/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.brown
                    shadowLabel.textColor = UIColor.brown
                    colorsWeak.textColor = UIColor.brown
                    NameTFWeak.backgroundColor = UIColor.brown
                    NameP1Weak.setTitleColor(UIColor.brown, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.brown, for: .normal)
                } else if Theme == 4 {
                    //notes
                    NameTitle.font = UIFont (name: "Noteworthy", size: 20)!
                    shadowLabel.font = UIFont (name: "Noteworthy", size: 20)!
                    colorsWeak.font = UIFont (name: "Noteworthy", size: 20)!
                    NameTFWeak.font = UIFont (name: "Noteworthy", size: 20)!
                    DisplayName.font = UIFont (name: "Noteworthy", size: 30)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    NameTitle.textColor = UIColor.black
                    shadowLabel.textColor = UIColor.black
                    colorsWeak.textColor = UIColor.black
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 5 {
                    //space
                     normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    NameTitle.textColor = UIColor.white
                    shadowLabel.textColor = UIColor.white
                    colorsWeak.textColor = UIColor.white
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.white, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.white, for: .normal)
                    ColorPicker.backgroundColor = UIColor.white
                } else if Theme == 6 {
                    //ocean
                     normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    NameTitle.textColor = UIColor.blue
                    shadowLabel.textColor = UIColor.blue
                    colorsWeak.textColor = UIColor.blue
                    NameTFWeak.backgroundColor = UIColor.blue
                    NameP1Weak.setTitleColor(UIColor.blue, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.blue, for: .normal)
                } else if Theme == 7 {
                    //red
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.red
                    shadowLabel.textColor = UIColor.red
                    colorsWeak.textColor = UIColor.red
                    NameTFWeak.backgroundColor = UIColor.red
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 8 {
                    //blue
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.blue
                    shadowLabel.textColor = UIColor.blue
                    colorsWeak.textColor = UIColor.blue
                    NameTFWeak.backgroundColor = UIColor.blue
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 9 {
                    //green
                     normalFont()
                    self.view.backgroundColor = UIColor(red: (152/255.0), green: (251/255.0), blue: (152/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.green
                    shadowLabel.textColor = UIColor.green
                    colorsWeak.textColor = UIColor.green
                    NameTFWeak.backgroundColor = UIColor.green
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 10 {
                    //purple
                     normalFont()
                      self.view.backgroundColor = UIColor(red: (147/255.0), green: (112/255.0), blue: (219/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.purple
                    shadowLabel.textColor = UIColor.purple
                    colorsWeak.textColor = UIColor.purple
                    NameTFWeak.backgroundColor = UIColor.purple
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 11 {
                    //orange
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.white
                    shadowLabel.textColor = UIColor.white
                    colorsWeak.textColor = UIColor.white
                    NameTFWeak.backgroundColor = UIColor.white
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 12 {
                    //pink
                     normalFont()
                     self.view.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (190/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    shadowLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    colorsWeak.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    NameTFWeak.backgroundColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                    self.view.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                    NameTitle.textColor = UIColor.yellow
                    shadowLabel.textColor = UIColor.yellow
                    colorsWeak.textColor = UIColor.yellow
                    NameTFWeak.backgroundColor = UIColor.yellow
                    NameP1Weak.setTitleColor(UIColor.black, for: .normal)
                    NameP2Weak.setTitleColor(UIColor.black, for: .normal)
                    
                }
            }
        }
        
        
        
        NameTitle.adjustsFontSizeToFitWidth = true
        DisplayName.adjustsFontSizeToFitWidth = true
        shadowLabel.adjustsFontSizeToFitWidth = true
        colorsWeak.adjustsFontSizeToFitWidth = true
        if doingMD == true {
          NameP2Weak.isHidden = true
        }
      NOPH = 1
       // self.textField(NameTFWeak, shouldChangeCharactersIn: NSRange(location: 3, length: 2), replacementString: "")
        
  Forever = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounterName), userInfo: nil, repeats: true)
        NameTFWeak.delegate = self
  NameP1Weak.isHidden = true
    DisplayName.text = "Player 1"
        self.ColorPicker.delegate = self
        self.ColorPicker.dataSource = self
        
        pickerData = ["Black", "Orange", "Blue", "Purple", "Red", "Yellow", "Magenta"]
        
self.addDoneButtonOnKeyboardTwo()
        
        
       
    }
    func normalFont() {
        NameTitle.font = UIFont (name: "Helvetica Neue", size: 20)!
        shadowLabel.font = UIFont (name: "Helvetica Neue", size: 20)!
        colorsWeak.font = UIFont (name: "Helvetica Neue", size: 20)!
         NameTFWeak.font = UIFont (name: "Helvetica Neue", size: 20)!
         DisplayName.font = UIFont (name: "Helvetica Neue", size: 30)!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func UpdateCounterName() {
        Colors()
       
      // print("UPDATE COUNTER \(pickerData[ColorPicker.selectedRow(inComponent: 0)])")
       
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = NameTFWeak.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 12
    }
    
  
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
       
    }
    //hiphopfuvv
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
        
    }
 
    
    

    @IBOutlet weak var NameTitle: UILabel!
    
   
    @IBOutlet weak var shadowLabel: UILabel!
    @IBOutlet weak var colorsWeak: UILabel!
    
    @IBOutlet weak var ShadowWeak: UISwitch!
    
    @IBOutlet weak var ColorPicker: UIPickerView!
    
    @IBOutlet weak var DisplayName: UILabel!
    @IBOutlet weak var NameP2Weak: UIButton!
   
   
   
    func Colors() {
        
         selectedValue = pickerData[ColorPicker.selectedRow(inComponent: 0)]
      
        if selectedValue == "Red" {
            DisplayName.textColor = UIColor.red
        } else if selectedValue == "Orange" {
            DisplayName.textColor = UIColor.orange
        }else if selectedValue == "Magenta" {
            DisplayName.textColor = UIColor.magenta
        } else if selectedValue == "Blue" {
            DisplayName.textColor = UIColor.blue
        } else if selectedValue == "Yellow" {
            DisplayName.textColor = UIColor.yellow
        } else if selectedValue == "Purple" {
            DisplayName.textColor = UIColor.purple
            
        } else if selectedValue == "Black" {
             DisplayName.textColor = UIColor.black
        }
        
        if NameCount == 1 {
            
            P1Color = DisplayName.textColor
        } else if NameCount == 2 {
            P2Color = DisplayName.textColor
            
        } else if NameCount == 3 {
            P3Color = DisplayName.textColor
        } else if NameCount == 4 {
            P4Color = DisplayName.textColor
        } else if NameCount == 5 {
            P5Color = DisplayName.textColor
        } else if NameCount == 6 {
            P6Color = DisplayName.textColor
        } else if NameCount == 7 {
            P7Color = DisplayName.textColor
        } else if NameCount == 8 {
            P8Color = DisplayName.textColor
        } else if NameCount == 9 {
            P9Color = DisplayName.textColor
        } else if NameCount == 10 {
            P10Color = DisplayName.textColor
        }
        
            
        }
    func addDoneButtonOnKeyboardTwo()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SecondViewController.doneButtonActionTwo))
        //Selector("doneButtonActionTwo"))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
      
        self.NameTFWeak.inputAccessoryView = doneToolbar
        
    }
    @objc func doneButtonActionTwo(){
        NameTFWeak.resignFirstResponder()
       
           DisplayName.text = NameTFWeak.text
        if NameCount == 1 {
            Player1Name = NameTFWeak.text!
        } else if NameCount == 2 {
            Player2Name = NameTFWeak.text!
        } else if NameCount == 3 {
              Player3Name = NameTFWeak.text!
        } else if NameCount == 4 {
              Player4Name = NameTFWeak.text!
        } else if NameCount == 5 {
               Player5Name = NameTFWeak.text!
        } else if NameCount == 6 {
              Player6Name = NameTFWeak.text!
        } else if NameCount == 7 {
              Player7Name = NameTFWeak.text!
        } else if NameCount == 8 {
              Player8Name = NameTFWeak.text!
        } else if NameCount == 9 {
              Player9Name = NameTFWeak.text!
        } else if NameCount == 10 {
              Player10Name = NameTFWeak.text!
        }
        
    }
    func Shadow() {
      
        if (ShadowWeak.isOn) {
            if NameCount == 1 {
            DisplayName.layer.shadowColor = P1Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P1isShadow = true
            } else if NameCount == 2 {
                DisplayName.layer.shadowColor = P2Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P2isShadow = true
            } else if NameCount == 3 {
               
                DisplayName.layer.shadowColor = P3Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P3isShadow = true
                
            } else if NameCount == 4 {
                DisplayName.layer.shadowColor = P4Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P4isShadow = true
            } else if NameCount == 5 {
                DisplayName.layer.shadowColor = P5Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P5isShadow = true
            } else if NameCount == 6 {
                DisplayName.layer.shadowColor = P6Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P6isShadow = true
            } else if NameCount == 7 {
                DisplayName.layer.shadowColor = P7Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P7isShadow = true
            } else if NameCount == 8 {
                DisplayName.layer.shadowColor = P8Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P8isShadow = true
            } else if NameCount == 9 {
                DisplayName.layer.shadowColor = P9Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P9isShadow = true
            } else if NameCount == 10 {
                DisplayName.layer.shadowColor = P10Color.cgColor
                DisplayName.layer.shadowOffset = CGSize(5,0)
                DisplayName.layer.shadowRadius = 2
                DisplayName.layer.shadowOpacity = 0.25
                P10isShadow = true
            }
            
        
        } else {
            if NameCount == 1 {
                P1isShadow = false
                
            } else if NameCount == 2 {
                P2isShadow = false
            } else if NameCount == 3 {
                P3isShadow = false
            } else if NameCount == 4 {
                P4isShadow = false
            } else if NameCount == 5 {
                P5isShadow = false
            } else if NameCount == 6 {
                P6isShadow = false
            } else if NameCount == 7 {
                P7isShadow = false
            } else if NameCount == 8 {
                P8isShadow = false
            } else if NameCount == 9 {
                P9isShadow = false
            } else if NameCount == 10 {
                P10isShadow = false
            }
            DisplayName.layer.shadowColor = UIColor.white.cgColor
            DisplayName.layer.shadowOffset = CGSize(0,0)
            DisplayName.layer.shadowRadius = 0
            DisplayName.layer.shadowOpacity = 0
        }
    }
   
    
    @IBOutlet weak var NameP1Weak: UIButton!
    
  
    @IBAction func NameP2(_ sender: UIButton) {
        if hasTenPlayerPack == true || hasBundle == true {
            if NOPH == 1 {
                NameP1Weak.isHidden = false
                if P2Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P2Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P2Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P2Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P2Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P2Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P2Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 2 Name"
                NOPH = 2
                NameCount = 2
                NameTFWeak.text = ""
                DisplayName.text = Player2Name
                DisplayName.textColor = P2Color
                if P2isShadow == false {
                    ShadowWeak.isOn = false
                } else if P2isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 2 {
                if P3Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P3Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P3Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P3Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P3Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P3Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P3Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 3 Name"
                NOPH = 3
                NameCount = 3
                NameTFWeak.text = ""
                DisplayName.text = Player3Name
                DisplayName.textColor = P3Color
                if P3isShadow == false {
                    ShadowWeak.isOn = false
                } else if P3isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 3 {
                if P4Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P4Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P4Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P4Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P4Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P4Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P4Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
            
                NameTitle.text = "Player 4 Name"
                NOPH = 4
                NameCount = 4
                NameTFWeak.text = ""
                DisplayName.text = Player4Name
                DisplayName.textColor = P4Color
                if P4isShadow == false {
                    ShadowWeak.isOn = false
                } else if P4isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 4 {
                if P5Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P5Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P5Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P5Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P5Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P5Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P5Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
            
                NameTitle.text = "Player 5 Name"
                NOPH = 5
                NameCount = 5
                NameTFWeak.text = ""
                DisplayName.text = Player5Name
                DisplayName.textColor = P5Color
                if P5isShadow == false {
                    ShadowWeak.isOn = false
                } else if P5isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 5 {
                if P6Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P6Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P6Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P6Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P6Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P6Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P6Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
              
                NameTitle.text = "Player 6 Name"
                NOPH = 6
                NameCount = 6
                NameTFWeak.text = ""
                DisplayName.text = Player6Name
                DisplayName.textColor = P2Color
                if P6isShadow == false {
                    ShadowWeak.isOn = false
                } else if P6isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 6 {
                if P7Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P7Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P7Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P7Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P7Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P7Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P7Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
                
                NameTitle.text = "Player 7 Name"
                NOPH = 7
                NameCount = 7
                NameTFWeak.text = ""
                DisplayName.text = Player7Name
                DisplayName.textColor = P7Color
                if P7isShadow == false {
                    ShadowWeak.isOn = false
                } else if P7isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 7 {
                if P8Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P8Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P8Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P8Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P8Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P8Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P8Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
                
                NameTitle.text = "Player 8 Name"
                NOPH = 8
                NameCount = 8
                NameTFWeak.text = ""
                DisplayName.text = Player8Name
                DisplayName.textColor = P8Color
                if P8isShadow == false {
                    ShadowWeak.isOn = false
                } else if P8isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 8 {
                if P9Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P9Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P9Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P9Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P9Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P9Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P9Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 9 Name"
                NOPH = 9
                NameCount = 9
                NameTFWeak.text = ""
                DisplayName.text = Player9Name
                DisplayName.textColor = P9Color
                if P9isShadow == false {
                    ShadowWeak.isOn = false
                } else if P9isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 9 {
                NameP2Weak.isHidden = true
                if P10Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P10Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P10Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P10Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P10Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P10Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P10Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = true
               
                NameTitle.text = "Player 10 Name"
                NOPH = 10
                NameCount = 10
                NameTFWeak.text = ""
                DisplayName.text = Player10Name
                DisplayName.textColor = P10Color
                if P10isShadow == false {
                    ShadowWeak.isOn = false
                } else if P10isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            }
        } else {
        if P2Color == UIColor.black {
            ColorPicker.selectRow(0, inComponent: 0, animated: false)
        } else if P2Color == UIColor.orange {
            ColorPicker.selectRow(1, inComponent: 0, animated: false)
        } else if P2Color == UIColor.blue {
            ColorPicker.selectRow(2, inComponent: 0, animated: false)
        } else if P2Color == UIColor.purple {
            ColorPicker.selectRow(3, inComponent: 0, animated: false)
        } else if P2Color == UIColor.red {
            ColorPicker.selectRow(4, inComponent: 0, animated: false)
        } else if P2Color == UIColor.yellow {
            ColorPicker.selectRow(5, inComponent: 0, animated: false)
        } else if P2Color == UIColor.magenta {
            ColorPicker.selectRow(6, inComponent: 0, animated: false)
        }
        NameP2Weak.isHidden = true
      NameP1Weak.isHidden = false
        NameTitle.text = "Player 2 Name"
        NameCount = 2
       
        NameTFWeak.text = ""
        DisplayName.text = Player2Name
        DisplayName.textColor = P2Color
        if P2isShadow == false {
             ShadowWeak.isOn = false
        } else if P2isShadow == true {
           ShadowWeak.isOn = true
        }
        Shadow()
       
        }
        
        
        
    }
    
    @IBAction func VC(_ sender: Any) {
        DisplayName.text = NameTFWeak.text!
    }
 
    @IBAction func Shadow(_ sender: UISwitch) {
        Shadow()
    }
    
    @IBAction func NameP1(_ sender: UIButton) {
        if hasTenPlayerPack == true || hasBundle == true {
            if NOPH == 2 {
                NameP1Weak.isHidden = true
                if P1Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P1Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P1Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P1Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P1Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P1Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P1Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
                
                NameTitle.text = "Player 1 Name"
                NOPH = 1
                NameCount = 1
                NameTFWeak.text = ""
                DisplayName.text = Player1Name
                DisplayName.textColor = P1Color
                if P1isShadow == false {
                    ShadowWeak.isOn = false
                } else if P1isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 3 {
                if P2Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P2Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P2Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P2Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P2Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P2Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P2Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 2 Name"
                NOPH = 2
                NameCount = 2
                NameTFWeak.text = ""
                DisplayName.text = Player2Name
                DisplayName.textColor = P2Color
                if P2isShadow == false {
                    ShadowWeak.isOn = false
                } else if P2isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 4 {
                if P3Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P3Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P3Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P3Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P3Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P3Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P3Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
              
                NameTitle.text = "Player 3 Name"
                NOPH = 3
                NameCount = 3
                NameTFWeak.text = ""
                DisplayName.text = Player3Name
                DisplayName.textColor = P3Color
                if P3isShadow == false {
                    ShadowWeak.isOn = false
                } else if P3isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
                
            } else if NOPH == 5 {
                if P4Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P4Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P4Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P4Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P4Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P4Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P4Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 4 Name"
                NOPH = 4
                NameCount = 4
                NameTFWeak.text = ""
                DisplayName.text = Player4Name
                DisplayName.textColor = P4Color
                if P4isShadow == false {
                    ShadowWeak.isOn = false
                } else if P4isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 6 {
                if P5Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P5Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P5Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P5Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P5Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P5Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P5Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 5 Name"
                NOPH = 5
                NameCount = 5
                NameTFWeak.text = ""
                DisplayName.text = Player5Name
                DisplayName.textColor = P5Color
                if P5isShadow == false {
                    ShadowWeak.isOn = false
                } else if P5isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 7 {
                if P6Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P6Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P6Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P6Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P6Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P6Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P6Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
             
                NameTitle.text = "Player 6 Name"
                NOPH = 6
                NameCount = 6
                NameTFWeak.text = ""
                DisplayName.text = Player6Name
                DisplayName.textColor = P2Color
                if P6isShadow == false {
                    ShadowWeak.isOn = false
                } else if P6isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 8 {
                if P7Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P7Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P7Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P7Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P7Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P7Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P7Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
             
                NameTitle.text = "Player 7 Name"
                NOPH = 7
                NameCount = 7
                NameTFWeak.text = ""
                DisplayName.text = Player7Name
                DisplayName.textColor = P7Color
                if P7isShadow == false {
                    ShadowWeak.isOn = false
                } else if P7isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
                
            } else if NOPH == 9 {
                if P8Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P8Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P8Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P8Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P8Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P8Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P8Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 8 Name"
                NOPH = 8
                NameCount = 8
                NameTFWeak.text = ""
                DisplayName.text = Player8Name
                DisplayName.textColor = P8Color
                if P8isShadow == false {
                    ShadowWeak.isOn = false
                } else if P8isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            } else if NOPH == 10 {
                NameP2Weak.isHidden = false
                if P9Color == UIColor.black {
                    ColorPicker.selectRow(0, inComponent: 0, animated: false)
                } else if P9Color == UIColor.orange {
                    ColorPicker.selectRow(1, inComponent: 0, animated: false)
                } else if P9Color == UIColor.blue {
                    ColorPicker.selectRow(2, inComponent: 0, animated: false)
                } else if P9Color == UIColor.purple {
                    ColorPicker.selectRow(3, inComponent: 0, animated: false)
                } else if P9Color == UIColor.red {
                    ColorPicker.selectRow(4, inComponent: 0, animated: false)
                } else if P9Color == UIColor.yellow {
                    ColorPicker.selectRow(5, inComponent: 0, animated: false)
                } else if P9Color == UIColor.magenta {
                    ColorPicker.selectRow(6, inComponent: 0, animated: false)
                }
                
                NameP2Weak.isHidden = false
               
                NameTitle.text = "Player 9 Name"
                NOPH = 9
                NameCount = 9
                NameTFWeak.text = ""
                DisplayName.text = Player9Name
                DisplayName.textColor = P9Color
                if P9isShadow == false {
                    ShadowWeak.isOn = false
                } else if P9isShadow == true {
                    ShadowWeak.isOn = true
                }
                Shadow()
            }
            
            
            
            
        } else {
        if P1Color == UIColor.black {
          ColorPicker.selectRow(0, inComponent: 0, animated: false)
        } else if P1Color == UIColor.orange {
              ColorPicker.selectRow(1, inComponent: 0, animated: false)
        } else if P1Color == UIColor.blue {
              ColorPicker.selectRow(2, inComponent: 0, animated: false)
        } else if P1Color == UIColor.purple {
              ColorPicker.selectRow(3, inComponent: 0, animated: false)
        } else if P1Color == UIColor.red {
              ColorPicker.selectRow(4, inComponent: 0, animated: false)
        } else if P1Color == UIColor.yellow {
              ColorPicker.selectRow(5, inComponent: 0, animated: false)
        } else if P1Color == UIColor.magenta {
               ColorPicker.selectRow(6, inComponent: 0, animated: false)
        }
        
        NameP2Weak.isHidden = false
            NameP1Weak.isHidden = true
            
            
       
        NameTitle.text = "Player 1 Name"
        
        NameCount = 1
        NameTFWeak.text = ""
        DisplayName.text = Player1Name
          DisplayName.textColor = P1Color
        if P1isShadow == false {
            ShadowWeak.isOn = false
        } else if P1isShadow == true {
            ShadowWeak.isOn = true
        }
       Shadow()
        }
    }
    
    
  
    //["Black", "Orange", "Blue", "Purple", "Red", "Yellow", "Magenta"]
  
    
    @IBAction func SwipeDown(_ sender: UISwipeGestureRecognizer) {
        NameTFWeak.resignFirstResponder()
    }
    
    
    @IBAction func TFValueChanged(_ sender: UITextField) {
        DisplayName.text = NameTFWeak.text
    }
    
    
    @IBAction func TFEditingChanged(_ sender: UITextField) {
          DisplayName.text = NameTFWeak.text
       
        
        DisplayName.text = NameTFWeak.text
        if NameCount == 1 {
            Player1Name = NameTFWeak.text!
        } else if NameCount == 2 {
            Player2Name = NameTFWeak.text!
        } else if NameCount == 3 {
            Player3Name = NameTFWeak.text!
        } else if NameCount == 4 {
            Player4Name = NameTFWeak.text!
        } else if NameCount == 5 {
            Player5Name = NameTFWeak.text!
        } else if NameCount == 6 {
            Player6Name = NameTFWeak.text!
        } else if NameCount == 7 {
            Player7Name = NameTFWeak.text!
        } else if NameCount == 8 {
            Player8Name = NameTFWeak.text!
        } else if NameCount == 9 {
            Player9Name = NameTFWeak.text!
        } else if NameCount == 10 {
            Player10Name = NameTFWeak.text!
        }
    }
    
    
    
    @IBAction func TFEditingDidEnd(_ sender: UITextField) {
        NameTFWeak.resignFirstResponder()
        
        DisplayName.text = NameTFWeak.text
        if NameCount == 1 {
            Player1Name = NameTFWeak.text!
        } else if NameCount == 2 {
            Player2Name = NameTFWeak.text!
        } else if NameCount == 3 {
            Player3Name = NameTFWeak.text!
        } else if NameCount == 4 {
            Player4Name = NameTFWeak.text!
        } else if NameCount == 5 {
            Player5Name = NameTFWeak.text!
        } else if NameCount == 6 {
            Player6Name = NameTFWeak.text!
        } else if NameCount == 7 {
            Player7Name = NameTFWeak.text!
        } else if NameCount == 8 {
            Player8Name = NameTFWeak.text!
        } else if NameCount == 9 {
            Player9Name = NameTFWeak.text!
        } else if NameCount == 10 {
            Player10Name = NameTFWeak.text!
        }
    }
    
    
    
    
    
    
}




