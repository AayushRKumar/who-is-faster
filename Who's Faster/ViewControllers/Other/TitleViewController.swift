//
//  TitleViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 7/25/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
var GameChosen = "Math"
var displayChosen = "Math"
var isTournament = false

class TitleViewController: UIViewController, GADBannerViewDelegate, GADInterstitialDelegate {
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if doingMD == false {
        if Player1Name == "Anonymous" || Player1Name == "" {
            Player1Name = "Player 1"
        }
        if Player2Name == "Anonymous" || Player2Name == "" {
            Player2Name = "Player 2"
        }
        }
        normalFont()
        self.view.backgroundColor = UIColor.orange
        navigationController?.navigationBar.backgroundColor = UIColor.white
        
        cwWeak.backgroundColor = UIColor.blue
        cmWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
        mathWeak.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
        tbWeak.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
        
        cygLabel.textColor = UIColor.black
        mainLabel.textColor = UIColor.black
        SettingsLabel.textColor = UIColor.black
        cwWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        cmWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        mathWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        tbWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        cygLabel.adjustsFontSizeToFitWidth = true
        mainLabel.adjustsFontSizeToFitWidth = true
        SettingsLabel.adjustsFontSizeToFitWidth = true
        if hasTheme == true {
            
            
            if CustomTheme == true {
                self.view.backgroundColor = PrimaryColor
                cwWeak.backgroundColor = SecondaryColor
                cmWeak.backgroundColor = SecondaryColor
                mathWeak.backgroundColor = SecondaryColor
                tbWeak.backgroundColor = SecondaryColor
                navigationController?.navigationBar.backgroundColor = PrimaryColor
                cygLabel.textColor = TextColor
                mainLabel.textColor = TextColor
                SettingsLabel.textColor = TextColor
            } else {
                if Theme == 0 {
                    //default
                    normalFont()
                    self.view.backgroundColor = UIColor.orange
                    navigationController?.navigationBar.backgroundColor = UIColor.white
                    
                    cwWeak.backgroundColor = UIColor.blue
                    cmWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
                    
                    cygLabel.textColor = UIColor.black
                    mainLabel.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                } else if Theme == 1 {
                    //dark
                    mainLabel.font = UIFont (name: "Marker Felt", size: 37)!
                    cygLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    SettingsLabel.font = UIFont (name: "Marker Felt", size: 25)!
                    cwWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 23)!
                    cmWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 23)!
                    mathWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 23)!
                    tbWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 23)!
                    self.view.backgroundColor = UIColor.darkGray
                    navigationController?.navigationBar.backgroundColor = UIColor.darkGray
                    
                    cygLabel.textColor = UIColor.gray
                    mainLabel.textColor = UIColor.gray
                    SettingsLabel.textColor = UIColor.gray
                    cwWeak.backgroundColor = UIColor(red: (105/255.0), green: (105/255.0), blue: (105/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (128/255.0), green: (128/255.0), blue: (128/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (112/255.0), green: (128/255.0), blue: (144/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (119/255.0), green: (136/255.0), blue: (153/255.0), alpha: 1.0)
                    
                    
                } else if Theme == 2 {
                    //chalk
                    cwWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 23)!
                    cmWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 23)!
                    mathWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 23)!
                    tbWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 23)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    mainLabel.font = UIFont (name: "Chalkduster", size: 37)!
                    cygLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    SettingsLabel.font = UIFont (name: "Chalkduster", size: 25)!
                    navigationController?.navigationBar.backgroundColor = UIColor.green
                    cwWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (0/255.0), green: (128/255.0), blue: (0/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                    
                    cygLabel.textColor = UIColor.white
                    mainLabel.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                } else if Theme == 3 {
                    //anicent
                    cwWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 23)!
                    cmWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 23)!
                    mathWeak.titleLabel?.font = UIFont (name: "Papyrus", size:23)!
                    tbWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 23)!
                    mainLabel.font = UIFont (name: "Papyrus", size: 37)!
                    cygLabel.font = UIFont (name: "Papyrus", size: 25)!
                    SettingsLabel.font = UIFont (name: "Papyrus", size: 25)!
                    view.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    navigationController?.navigationBar.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    
                    cwWeak.backgroundColor = UIColor(red: (139/255.0), green: (69/255.0), blue: (19/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (160/255.0), green: (82/255.0), blue: (45/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (205/255.0), green: (133/255.0), blue: (63/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor.brown
                    
                    cygLabel.textColor = UIColor.brown
                    mainLabel.textColor = UIColor.brown
                    SettingsLabel.textColor = UIColor.brown
                    
                } else if Theme == 4 {
                    //notes
                    cwWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 23)!
                    cmWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 23)!
                    mathWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 23)!
                    tbWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 23)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    mainLabel.font = UIFont (name: "Noteworthy", size: 37)!
                    cygLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    SettingsLabel.font = UIFont (name: "Noteworthy", size: 25)!
                    navigationController?.navigationBar.backgroundColor = UIColor.yellow
                    cwWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor.yellow
                    mathWeak.backgroundColor = UIColor(red: (218/255.0), green: (165/255.0), blue: (32/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    
                    cygLabel.textColor = UIColor.black
                    mainLabel.textColor = UIColor.black
                    SettingsLabel.textColor = UIColor.black
                } else if Theme == 5 {
                    //space
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    navigationController?.navigationBar.backgroundColor = UIColor.black
                    cwWeak.backgroundColor = UIColor.black
                    cmWeak.backgroundColor = UIColor.black
                    mathWeak.backgroundColor = UIColor.black
                    tbWeak.backgroundColor = UIColor.black
                    
                    cygLabel.textColor = UIColor.white
                    mainLabel.textColor = UIColor.white
                    SettingsLabel.textColor = UIColor.white
                } else if Theme == 6 {
                    //ocean
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    navigationController?.navigationBar.backgroundColor = UIColor.blue
                    cwWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (205/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor.blue
                    tbWeak.backgroundColor = UIColor(red: (30/255.0), green: (144/255.0), blue: (255/255.0), alpha: 1.0)
                    
                    cygLabel.textColor = UIColor.cyan
                    mainLabel.textColor = UIColor.cyan
                    SettingsLabel.textColor = UIColor.cyan
                } else if Theme == 7 {
                    //red
                    normalFont()
                    self.view.backgroundColor = UIColor.red
                    navigationController?.navigationBar.backgroundColor = UIColor.red
                    cwWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (178/255.0), green: (34/255.0), blue: (34/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (255/255.0), green: (99/255.0), blue: (71/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    
                    cygLabel.textColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (139/255.0), green: (0/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    self.view.backgroundColor = UIColor.blue
                    navigationController?.navigationBar.backgroundColor = UIColor.blue
                    cwWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (128/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (30/255.0), green: (144/255.0), blue: (255/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (135/255.0), green: (206/255.0), blue: (250/255.0), alpha: 1.0)
                    cygLabel.textColor = UIColor(red: (135/255.0), green: (206/255.0), blue: (250/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (135/255.0), green: (206/255.0), blue: (250/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (135/255.0), green: (206/255.0), blue: (250/255.0), alpha: 1.0)
                } else if Theme == 9 {
                    //green
                    normalFont()
                    self.view.backgroundColor = UIColor.green
                    navigationController?.navigationBar.backgroundColor = UIColor.green
                    cwWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (0/255.0), green: (128/255.0), blue: (0/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                    cygLabel.textColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (34/255.0), green: (139/255.0), blue: (34/255.0), alpha: 1.0)
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    self.view.backgroundColor = UIColor.purple
                    navigationController?.navigationBar.backgroundColor = UIColor.purple
                    cwWeak.backgroundColor = UIColor(red: (106/255.0), green: (90/255.0), blue: (205/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (148/255.0), green: (0/255.0), blue: (211/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (72/255.0), green: (61/255.0), blue: (139/255.0), alpha: 1.0)
                    cygLabel.textColor = UIColor(red: (72/255.0), green: (61/255.0), blue: (139/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (72/255.0), green: (61/255.0), blue: (139/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (72/255.0), green: (61/255.0), blue: (139/255.0), alpha: 1.0)
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    self.view.backgroundColor = UIColor.orange
                    navigationController?.navigationBar.backgroundColor = UIColor.orange
                    cwWeak.backgroundColor = UIColor(red: (255/255.0), green: (127/255.0), blue: (80/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (255/255.0), green: (165/255.0), blue: (0/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (255/255.0), green: (99/255.0), blue: (71/255.0), alpha: 1.0)
                    cygLabel.textColor = UIColor(red: (255/255.0), green: (165/255.0), blue: (0/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (255/255.0), green: (165/255.0), blue: (0/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (255/255.0), green: (165/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 12 {
                    //pink
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                    navigationController?.navigationBar.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                    cwWeak.backgroundColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (255/255.0), green: (182/255.0), blue: (193/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (255/255.0), green: (192/255.0), blue: (203/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (218/255.0), green: (112/255.0), blue: (214/255.0), alpha: 1.0)
                    normalFont()
                    cygLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (255/255.0), green: (105/255.0), blue: (180/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    self.view.backgroundColor = UIColor.yellow
                    navigationController?.navigationBar.backgroundColor = UIColor.yellow
                    normalFont()
                    cwWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    cmWeak.backgroundColor = UIColor(red: (218/255.0), green: (165/255.0), blue: (32/255.0), alpha: 1.0)
                    mathWeak.backgroundColor = UIColor(red: (238/255.0), green: (232/255.0), blue: (170/255.0), alpha: 1.0)
                    tbWeak.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    
                    normalFont()
                    cygLabel.textColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    mainLabel.textColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    SettingsLabel.textColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                }
            }
        }
        cView.backgroundColor = self.view.backgroundColor
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     popup = createAndLoadInterstitial()
        if noAds == false {
            
            
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            
            bannerView.topAnchor.constraint(equalTo: PlayerSlider.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
         navigationController?.setNavigationBarHidden(false, animated: true)
        if doingMD == true {
            PlayerSlider.isHidden = true
        }
        
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        
        popup = GADInterstitial(adUnitID: "ca-app-pub-2312248651171588/1230141252")
        popup.delegate = self
        popup.load(GADRequest())
        
        return popup
    }
    func normalFont() {
      cwWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)!
        cmWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)!
        mathWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)!
        tbWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 23)!
        mainLabel.font = UIFont (name: "Helvetica Neue", size: 37)!
        cygLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
        SettingsLabel.font = UIFont (name: "Helvetica Neue", size: 25)!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbWeak.layer.cornerRadius = 10
        tbWeak.clipsToBounds = true
        mathWeak.layer.cornerRadius = 10
        mathWeak.clipsToBounds = true
        cwWeak.layer.cornerRadius = 10
        cwWeak.clipsToBounds = true
        cmWeak.layer.cornerRadius = 10
        cmWeak.clipsToBounds = true
        normalFont()
        bannerContainer.isHidden = true
       
        self.view.backgroundColor = UIColor.orange
        navigationController?.navigationBar.backgroundColor = UIColor.white
        
        cwWeak.backgroundColor = UIColor.blue
        cmWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
        mathWeak.backgroundColor = UIColor(red: (0/255.0), green: (191/255.0), blue: (255/255.0), alpha: 1.0)
        tbWeak.backgroundColor = UIColor(red: (138/255.0), green: (43/255.0), blue: (226/255.0), alpha: 1.0)
        
        SettingsLabel.adjustsFontSizeToFitWidth = true
        if hasTenPlayerPack == true {
            
        }
        
        
        if hasTenPlayerPack == false {
            PlayerSlider.isHidden = true
            SettingsLabel.isHidden = true
        }
        
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    @IBOutlet weak var SettingsLabel: UILabel!
    
    @IBOutlet weak var PlayerSlider: UISlider!
   
    
   
    
    
    @IBAction func PlayerSliderStrong(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        
        SettingsLabel.text = "Number of Players: \(currentValue)"
        
        NOP = Int("\(currentValue)")!
        
        
        
        
        
        
    }
    
    @IBAction func Math(_ sender: UIButton) {
        GameChosen = "Math"
        displayChosen = "Math"
    }
    
    
    @IBAction func TB(_ sender: UIButton) {
        GameChosen = "TB"
       displayChosen = "Tap Button"
    }
    
    
    @IBAction func CW(_ sender: UIButton) {
        GameChosen = "CW"
        displayChosen = "Copy Word"
    }
    
    @IBAction func CM(_ sender: UIButton) {
        GameChosen = "CM"
        displayChosen = "Color Matching"
    }
    
    
    @IBOutlet weak var cmWeak: UIButton!
    
    @IBOutlet weak var mathWeak: UIButton!
    
    @IBOutlet weak var tbWeak: UIButton!
    
    @IBOutlet weak var cwWeak: UIButton!
    
    
    @IBOutlet weak var mainLabel: UILabel!
    
    
    @IBOutlet weak var cygLabel: UILabel!
    
    
    
    @IBOutlet weak var cView: UIView!
    
   
    @IBOutlet weak var bannerContainer: UIView!
    
}
