//
//  FirstPageViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/7/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import MessageUI
import GoogleMobileAds
var hasTheme = false
var Theme = 0
var doingMD = false
var hasBundle = false
var hasTenPlayerPack = false
var noAds = false
 
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

 var bannerView: GADBannerView!
class FirstPageViewController: UIViewController, MFMailComposeViewControllerDelegate, GADBannerViewDelegate {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        normalFont()
        print("NO ads \(noAds)")
        navigationController?.navigationBar.backgroundColor = UIColor.white
        view.backgroundColor = UIColor.white
        ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
        singleWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
        multiWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
        if hasTheme == true {
            if CustomTheme == true {
                normalFont()
                self.view.backgroundColor = PrimaryColor
                navigationController?.navigationBar.backgroundColor = PrimaryColor
                ThemeWeak.backgroundColor = SecondaryColor
                singleWeak.backgroundColor = SecondaryColor
                multiWeak.backgroundColor = SecondaryColor
            } else {
                if Theme == 0 {
                    //default
                    normalFont()
                    navigationController?.navigationBar.backgroundColor = UIColor.white
                    view.backgroundColor = UIColor.white
                    ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (255/255.0), alpha: 1.0)
                    //        view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                } else if Theme == 1 {
                    //dark
                    navigationController?.navigationBar.backgroundColor = UIColor.darkGray
                    ThemeWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    singleWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    multiWeak.titleLabel?.font = UIFont (name: "Marker Felt", size: 22)!
                    view.backgroundColor = UIColor.darkGray
                    ThemeWeak.backgroundColor = UIColor.black
                    singleWeak.backgroundColor = UIColor.black
                    multiWeak.backgroundColor = UIColor.black
                } else if Theme == 2 {
                    //chalk
                    navigationController?.navigationBar.backgroundColor = UIColor.green
                    ThemeWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                    singleWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                    multiWeak.titleLabel?.font = UIFont (name: "Chalkduster", size: 22)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    ThemeWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (0/255.0), green: (100/255.0), blue: (0/255.0), alpha: 1.0)
                    
                    
                    
                } else if Theme == 3 {
                    //anicent
                    navigationController?.navigationBar.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    ThemeWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    singleWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    multiWeak.titleLabel?.font = UIFont (name: "Papyrus", size: 22)!
                    view.backgroundColor = UIColor(red: (240/255.0), green: (230/255.0), blue: (140/255.0), alpha: 1.0)
                    ThemeWeak.backgroundColor = UIColor.brown
                    singleWeak.backgroundColor = UIColor.brown
                    multiWeak.backgroundColor = UIColor.brown
                } else if Theme == 4 {
                    //notes
                    ThemeWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 22)!
                    singleWeak.titleLabel?.font = UIFont (name: "Noteworthy", size: 22)!
                    multiWeak.titleLabel?.font = UIFont (name: "NoteWorthy", size: 22)!
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    navigationController?.navigationBar.backgroundColor = UIColor.yellow
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (255/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (255/255.0), green: (212/255.0), blue: (0/255.0), alpha: 1.0)
                    
                    
                } else if Theme == 5 {
                    //space
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    normalFont()
                    navigationController?.navigationBar.backgroundColor = UIColor.black
                    ThemeWeak.backgroundColor = UIColor.black
                    singleWeak.backgroundColor = UIColor.black
                    multiWeak.backgroundColor = UIColor.black
                } else if Theme == 6 {
                    //ocean
                    normalFont()
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    navigationController?.navigationBar.backgroundColor = UIColor.blue
                    ThemeWeak.backgroundColor = UIColor(red: (72/255.0), green: (209/255.0), blue: (204/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (72/255.0), green: (209/255.0), blue: (204/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (72/255.0), green: (209/255.0), blue: (204/255.0), alpha: 1.0)
                } else if Theme == 7 {
                    //red
                    normalFont()
                    self.view.backgroundColor = UIColor.red
                    navigationController?.navigationBar.backgroundColor = UIColor.red
                    ThemeWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (220/255.0), green: (20/255.0), blue: (60/255.0), alpha: 1.0)
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    navigationController?.navigationBar.backgroundColor = UIColor.blue
                    ThemeWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (65/255.0), green: (105/255.0), blue: (225/255.0), alpha: 1.0)
                    self.view.backgroundColor = UIColor.blue
                } else if Theme == 9 {
                    //green
                    normalFont()
                    self.view.backgroundColor = UIColor.green
                    navigationController?.navigationBar.backgroundColor = UIColor.green
                    ThemeWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (85/255.0), green: (107/255.0), blue: (47/255.0), alpha: 1.0)
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    navigationController?.navigationBar.backgroundColor = UIColor.purple
                    self.view.backgroundColor = UIColor.purple
                    ThemeWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (153/255.0), green: (50/255.0), blue: (204/255.0), alpha: 1.0)
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    self.view.backgroundColor = UIColor.orange
                    navigationController?.navigationBar.backgroundColor = UIColor.orange
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (255/255.0), green: (140/255.0), blue: (0/255.0), alpha: 1.0)
                } else if Theme == 12 {
                    //pink
                    view.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                    navigationController?.navigationBar.backgroundColor = UIColor(red: (255/255.0), green: (148/255.0), blue: (161/255.0), alpha: 1.0)
                    normalFont()
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (255/255.0), green: (20/255.0), blue: (147/255.0), alpha: 1.0)
                } else if Theme == 13 {
                    //yellow
                    self.view.backgroundColor = UIColor.yellow
                    navigationController?.navigationBar.backgroundColor = UIColor.yellow
                    normalFont()
                    ThemeWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    singleWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                    multiWeak.backgroundColor = UIColor(red: (255/255.0), green: (215/255.0), blue: (0/255.0), alpha: 1.0)
                }
            }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
                bannerContainer.isHidden = true
             ThemeWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        singleWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        multiWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        
        
        
        
        if noAds == false {
            
            let adSize = GADAdSizeFromCGSize(CGSize(width: screenWidth, height: (view.frame.size.height - (cartWeak.frame.size.height + cartWeak.frame.origin.y)) - 15))
            bannerView = GADBannerView(adSize: adSize)
            //    bannerView = GADBannerView()
            
            
            
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: cartWeak.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
           
        }
        
        if tempTheme == true {
            hasTheme = false
            Theme = 0
        }
        if tempTenPack == true {
            hasTenPlayerPack = false
            
        }
        
    }
    
    

    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
      
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription) ")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    func normalFont() {
         ThemeWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
        singleWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
        multiWeak.titleLabel?.font = UIFont (name: "Helvetica Neue", size: 22)!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
   //     TextColor = UIColor.black
   //     SecondaryColor = UIColor.orange
        print(UUID())
     
    //    App42API.initialize(withAPIKey: "3f3b0301e5ec512f037ac58c94a67ba2d6a786a4da11569aabf0254c2430fbd6", andSecretKey:"1fef373ef0324a424f0ddad36554e594be5166c191eb4644be76d624ffab4a29")
        ThemeWeak.layer.cornerRadius = 10
        ThemeWeak.clipsToBounds = true
        
        singleWeak.layer.cornerRadius = 10
        singleWeak.clipsToBounds = true
        
        multiWeak.layer.cornerRadius = 10
        multiWeak.clipsToBounds      = true
        onlineWeak.layer.cornerRadius = 10
        onlineWeak.clipsToBounds      = true
       
       // HERE
       DataManager.deleteAll()
        if hasBundle == true {
            hasTheme = true
            hasTenPlayerPack = true
            noAds = true
        }
        if hasTheme == false {
            ThemeWeak.isHidden = true
        }
        //-- Here
        print("No Ads: \(noAds) Bundle: \(hasBundle) Pack: \(hasTenPlayerPack) Theme: \(hasTheme)")
       
        
       
    }
    
    @IBOutlet weak var onlineWeak: UIButton!
    
    @IBOutlet weak var ThemeWeak: UIButton!
    
    @IBOutlet weak var cartWeak: UIButton!
    @IBOutlet weak var multiWeak: UIButton!
    
    @IBOutlet weak var singleWeak: UIButton!
    @IBAction func PlayOnThisDevice(_ sender: UIButton) {
        doingMD = false
       
       
    }
    @IBAction func PlayOnMultipleDevices(_ sender: UIButton) {
        doingMD = true
        
        
    }
    
    @IBAction func playOnline(_ sender: UIButton) {
    }
    @IBAction func FeedBack(_ sender: UIBarButtonItem) {
        
        let toRecipet = ["rajivrk@compubaba.com"]
        let emailTitle = "Who's Quicker FeedBack"
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setToRecipients(toRecipet)
        mc.setSubject(emailTitle)
        self.present(mc, animated: true, completion: nil)
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var bannerContainer: UIView!
}
