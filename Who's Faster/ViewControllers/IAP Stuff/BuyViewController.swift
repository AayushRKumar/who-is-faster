//
//  BuyViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 12/16/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
var alreadyBoughtAds = false
var alreadyBoughtPack = false
var alreadyBoughtThemes = false
var alreadyBoughtBundle = false
class BuyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        adsWeak.layer.cornerRadius = 5
        adsWeak.clipsToBounds = true
        tenWeak.layer.cornerRadius = 5
        tenWeak.clipsToBounds = true
        themesWeak.layer.cornerRadius = 5
        themesWeak.clipsToBounds = true
        bundleWeak.layer.cornerRadius = 5
        bundleWeak.clipsToBounds = true
        IAPHandler.shared.fetchAvailableProducts()
        
        if hasBundle == true {
             alreadyBoughtAds = true
             alreadyBoughtPack = true
             alreadyBoughtThemes = true
            alreadyBoughtBundle = true
        } else if noAds == true {
            alreadyBoughtAds = true
        } else if hasTenPlayerPack == true {
             alreadyBoughtPack = true
        } else if hasTheme == true {
               alreadyBoughtThemes = true
        }
    }
    
    @IBAction func RestorcePurchases(_ sender: UIBarButtonItem) {
    IAPHandler.shared.restorePurchase()

    }
    
    func notUsing() {
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "Buying", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
    }
    func seeIfCantBuy() {
        if cantBuy == true {
            let Alert = UIAlertController(title: "You Can't Buy!", message: "Please Enable In-App Purchases to buy this product. ", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func RemoveAdds(_ sender: UIButton) {
        if alreadyBoughtAds == true {
            let Alert = UIAlertController(title: "You have already purchased this Item", message: "", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
            
        } else {
        IAPHandler.shared.purchaseMyProduct(index: 0)
       
        seeIfCantBuy()
        }

    }
    
    
    @IBAction func BuyThemes(_ sender: UIButton) {
        if alreadyBoughtThemes {
            let Alert = UIAlertController(title: "You have already purchased this Item", message: "", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
        
        } else {
        IAPHandler.shared.purchaseMyProduct(index: 3)
        seeIfCantBuy()
        }
    }
    
    
    @IBAction func BuyTenPlayerPack(_ sender: UIButton) {
        if alreadyBoughtPack == true {
            let Alert = UIAlertController(title: "You have already purchased this Item", message: "", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
        } else {
        IAPHandler.shared.purchaseMyProduct(index: 2)
        seeIfCantBuy()
            
        }
    }
    
    
    @IBAction func BuySuperBundle(_ sender: UIButton) {
        if alreadyBoughtBundle == true {
            let Alert = UIAlertController(title: "You have already purchased this Item", message: "", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
        } else {
        IAPHandler.shared.purchaseMyProduct(index: 1)
        seeIfCantBuy()
        }
    }
    
    
    
    @IBOutlet weak var adsWeak: UIButton!
    
    
    @IBOutlet weak var themesWeak: UIButton!
    
    @IBOutlet weak var tenWeak: UIButton!
    
    @IBOutlet weak var bundleWeak: UIButton!
}
//Item Removed (1) File: 1FC70863-784B-4079-B866-A44139E1BD0A
//Item Removed (2) File: 5ED52253-DB83-4440-AE5B-737351D589B3

//Item Removed (1) File: DEF752A1-F861-4893-9C16-DCA6483438F0
//Item Removed (2) File: C0BAE252-BCE8-4B9A-BF39-01AB0FE7A0EC

//Item Removed (1) File: C473914C-C1A6-4C4C-A19D-86C32BB49B9E
//Item Removed (2) File: 194981D0-068D-491F-A29B-8CBEF0DBB075
