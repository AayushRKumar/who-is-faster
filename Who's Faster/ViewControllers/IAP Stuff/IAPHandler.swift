



//
//  IAPHandler.swift
//
//  Created by Dejan Atanasov on 13/07/2017.
//  Copyright © 2017 Dejan Atanasov. All rights reserved.
//
import UIKit
import StoreKit
var cantBuy = false
var product = SKProduct()
enum IAPHandlerAlertType{
    case disabled
    case restored
    case purchased
    
    func message() -> String{
        switch self {
        case .disabled: return "Purchases are disabled in your device!"
        case .restored: return "You've successfully restored your purchase!"
        case .purchased: return "You've successfully bought this purchase!"
        }
    }
}


class IAPHandler: NSObject {
    static let shared = IAPHandler()
    
    let CONSUMABLE_PURCHASE_PRODUCT_ID = "testpurchase"
    let NON_CONSUMABLE_PURCHASE_PRODUCT_ID = "non.consumable"
    let remove_ads = "remove.ads"
    let unlock_super_bundle = "unlock.super.bundle"
    let unlock_ten_player_pack = "unlock.ten.player.pack"
    let unlock_themes = "unlock.themes"

    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
    
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(index: Int){
        if iapProducts.count == 0 {
            
            return
            
        }
        
        if self.canMakePurchases() {
             product = iapProducts[index]
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
            productID = product.productIdentifier
        } else {
            purchaseStatusBlock?(.disabled)
            print("CANT MAKE PURCHASESSS")
           cantBuy = true
        }
    }
    
    // MARK: - RESTORE PURCHASE
    func restorePurchase(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts(){
        
        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: remove_ads, unlock_super_bundle, unlock_ten_player_pack, unlock_themes)

        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
}

extension IAPHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver{
    // MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        
        if (response.products.count > 0) {
            iapProducts = response.products
            for product in iapProducts{
                
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
                let price1Str = numberFormatter.string(from: product.price)
                print(product.localizedDescription + "\nfor just \(price1Str!)")
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        purchaseStatusBlock?(.restored)
    }
    
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    print("purchased Item")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.purchased)
                    if product == iapProducts[0] {
                        let important = MDPlayerItem(title: "removeAds99DadIf1", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        
                        important.saveAdsItem()
                         print(important)
                    } else if product == iapProducts[1] {
                        let important = MDPlayerItem(title: "unlockSuperBundle99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveBundleItem()
                         print(important)
                    } else if product == iapProducts[2] {
                        let important = MDPlayerItem(title: "unlockTenPlayerPack99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveTenPackItem()
                         print(important)
                    } else if product == iapProducts[3] {
                        let important = MDPlayerItem(title: "unlockThemes99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveThemesItem()
                         print(important)
                    }
                 // C805E80D-24BD-43CE-8DF2-D3F195DE7017
                    
                    //Who_s_Faster.MDPlayerItem(title: "op", completed: false, sendingInt: 1, sendingString: "", itemIdentifier: 51F083F8-C246-4CB0-9EC2-00046B47E221)
                    
                    
                    
                    
                    break
                    
                case .failed:
                    print("failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break
                case .restored:
                    print("restored")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
                    
                    print("restore... \(productIdentifier)")
                    if productIdentifier == "unlock.ten.player.pack" {
                        let important = MDPlayerItem(title: "unlockTenPlayerPack99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveTenPackItem()
                        print(important)
                    } else if productIdentifier == "remove.ads" {
                        let important = MDPlayerItem(title: "removeAds99DadIf1", completed: false, sendingInt: 0, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveAdsItem()
                        print(important)
                    } else if productIdentifier == "unlock.themes" {
                        let important = MDPlayerItem(title: "unlockThemes99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveThemesItem()
                        print(important)
                    } else if productIdentifier == "unlock.super.bundle" {
                        let important = MDPlayerItem(title: "unlockSuperBundle99DadIf1", completed: false, sendingInt: 99, sendingString: "", itemIdentifier: UUID())
                        important.saveItem()
                        important.saveBundleItem()
                        print(important)

                    }
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    break
                    
                default: break
                }}}
    }
    
    
}
