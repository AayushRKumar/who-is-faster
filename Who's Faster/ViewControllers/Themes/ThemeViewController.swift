//
//  ThemeViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/14/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ThemeViewController: UIViewController, GADBannerViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeWeak.layer.cornerRadius = 10
        makeWeak.clipsToBounds = true
        pickWeak.layer.cornerRadius = 10
        pickWeak.clipsToBounds = true
       // if noAds == false {
       //     bannerView.translatesAutoresizingMaskIntoConstraints = false
       //     view.addSubview(bannerView)
       //
       //     bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
       //     bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
       //      bannerView.topAnchor.constraint(equalTo: makeWeak.bottomAnchor).isActive = true
          
       //     bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
       //     bannerView.rootViewController = self
        //    bannerView.load(GADRequest())
        //    bannerView.delegate = self
       // }//
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func PickTheme(_ sender: UIButton) {
        CustomTheme = false
    }
    
    @IBAction func MakeTheme(_ sender: UIButton) {
        CustomTheme = true
    }
    
    @IBOutlet weak var makeWeak: UIButton!
    
    @IBOutlet weak var pickWeak: UIButton!
    
}
