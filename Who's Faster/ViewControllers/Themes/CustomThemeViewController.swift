//
//  CustomThemeViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/13/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
var ThemeColors = ["Black", "Gray", "Dark Gray", "Orange", "Red", "Blue", "Yellow", "Purple", "Brown", "White", "Magenta", "Cyan"]
var PrimaryColor = UIColor.black
var SecondaryColor = UIColor.orange
var CustomTheme = false
var TextColor = UIColor.white
var FLPrimary = UIColor()
var FLSecondary = UIColor()
var FLtextColor = UIColor()
class CustomThemeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, GADBannerViewDelegate {

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if doingMD == true {
            let Alert = UIAlertController(title: "Sorry", message: "Custom Theme Does not work with Multiple Devices", preferredStyle: .alert)
            let DissmissButton = UIAlertAction(title: "OK", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                _ = self.navigationController?.popViewController(animated: true)
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            })
            Alert.addAction(DissmissButton)
            self.present(Alert, animated: true, completion: nil)
        } else {
            Forever.invalidate()
        print("PrimaryColor: \(PrimaryColor)")
        PrimaryColor = UIColor.red
        switch PrimaryColor {
        case UIColor.black:
            PrimaryColorPicker.selectRow(0, inComponent: 0, animated: false)
            
        case UIColor.gray:
             PrimaryColorPicker.selectRow(1, inComponent: 0, animated: false)
        case UIColor.darkGray:
             PrimaryColorPicker.selectRow(2, inComponent: 0, animated: false)
        case UIColor.orange:
             PrimaryColorPicker.selectRow(3, inComponent: 0, animated: false)
        case UIColor.red:
             PrimaryColorPicker.selectRow(4, inComponent: 0, animated: false)
        case UIColor.blue:
             PrimaryColorPicker.selectRow(5, inComponent: 0, animated: false)
        case UIColor.yellow:
             PrimaryColorPicker.selectRow(6, inComponent: 0, animated: false)
        case UIColor.purple:
             PrimaryColorPicker.selectRow(7, inComponent: 0, animated: false)
             
            print("Selected Row: \(PrimaryColorPicker.selectedRow(inComponent: 0))")
        case UIColor.brown:
             PrimaryColorPicker.selectRow(8, inComponent: 0, animated: false)
        case UIColor.white:
             PrimaryColorPicker.selectRow(9, inComponent: 0, animated: false)
        case UIColor.magenta:
             PrimaryColorPicker.selectRow(10, inComponent: 0, animated: false)
        case UIColor.cyan:
             PrimaryColorPicker.selectRow(11, inComponent: 0, animated: false)
        default:
       print("DAb")
        }
    
            
        switch SecondaryColor {
        case UIColor.black:
             SecondaryColorPicker.selectRow(0, inComponent: 0, animated: false)
        case UIColor.gray:
             SecondaryColorPicker.selectRow(1, inComponent: 0, animated: false)
        case UIColor.darkGray:
             SecondaryColorPicker.selectRow(2, inComponent: 0, animated: false)
        case UIColor.orange:
             SecondaryColorPicker.selectRow(3, inComponent: 0, animated: false)
        case UIColor.red:
             SecondaryColorPicker.selectRow(4, inComponent: 0, animated: false)
        case UIColor.blue: 
             SecondaryColorPicker.selectRow(5, inComponent: 0, animated: false)
        case UIColor.yellow:
             SecondaryColorPicker.selectRow(6, inComponent: 0, animated: false)
        case UIColor.purple:
             SecondaryColorPicker.selectRow(7, inComponent: 0, animated: false)
        case UIColor.brown:
             SecondaryColorPicker.selectRow(8, inComponent: 0, animated: false)
        case UIColor.white:
             SecondaryColorPicker.selectRow(9, inComponent: 0, animated: false)
        case UIColor.magenta:
             SecondaryColorPicker.selectRow(10, inComponent: 0, animated: false)
        case UIColor.cyan:
             SecondaryColorPicker.selectRow(11, inComponent: 0, animated: false)
        default:
           print("")
        }
        switch TextColor {
        case UIColor.black:
             TextColorPicker.selectRow(0, inComponent: 0, animated: false)
        case UIColor.gray:
             TextColorPicker.selectRow(1, inComponent: 0, animated: false)
        case UIColor.darkGray:
             TextColorPicker.selectRow(2, inComponent: 0, animated: false)
        case UIColor.orange:
             TextColorPicker.selectRow(3, inComponent: 0, animated: false)
        case UIColor.red:
             TextColorPicker.selectRow(4, inComponent: 0, animated: false)
        case UIColor.blue:
             TextColorPicker.selectRow(5, inComponent: 0, animated: false)
        case UIColor.yellow:
             TextColorPicker.selectRow(6, inComponent: 0, animated: false)
        case UIColor.purple:
             TextColorPicker.selectRow(7, inComponent: 0, animated: false)
        case UIColor.brown:
             TextColorPicker.selectRow(8, inComponent: 0, animated: false)
        case UIColor.white:
             TextColorPicker.selectRow(9, inComponent: 0, animated: false)
        case UIColor.magenta:
             TextColorPicker.selectRow(10, inComponent: 0, animated: false)
        case UIColor.cyan:
             TextColorPicker.selectRow(11, inComponent: 0, animated: false)
        default:
            print("")
        }
        
         
        
        
        
        Forever = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateCounter), userInfo: nil, repeats: true)
        self.PrimaryColorPicker.delegate = self
        self.PrimaryColorPicker.dataSource = self
        self.SecondaryColorPicker.delegate = self
        self.SecondaryColorPicker.dataSource = self
        self.TextColorPicker.delegate = self
        self.TextColorPicker.dataSource = self
        // Do any additional setup after loading the view.
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  if noAds == false {
           
            
            // addBannerViewToView(bannerView)
      //      bannerView.translatesAutoresizingMaskIntoConstraints = false
      //      view.addSubview(bannerView)
            
      //      bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
      //      bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            
       //      bannerView.topAnchor.constraint(equalTo: TextColorPicker.bottomAnchor).isActive = true
            
       //     bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        //    bannerView.rootViewController = self
        //    bannerView.load(GADRequest())
        //    bannerView.delegate = self
      //  }
       
       
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    @objc func UpdateCounter() {
        let selectedValue = ThemeColors[PrimaryColorPicker.selectedRow(inComponent: 0)]
        
        if selectedValue == "Black" {
        PrimaryColor = UIColor.black
           
        } else if selectedValue == "Gray" {
            PrimaryColor = UIColor.gray

        } else if selectedValue == "Dark Gray" {
            PrimaryColor = UIColor.darkGray

        } else if selectedValue == "Orange" {
           PrimaryColor = UIColor.orange
        } else if selectedValue == "Red" {
           PrimaryColor = UIColor.red
        } else if selectedValue == "Blue" {
           PrimaryColor = UIColor.blue
            
        } else if selectedValue == "Yellow" {
            PrimaryColor = UIColor.yellow
        } else if selectedValue == "Purple" {
            PrimaryColor = UIColor.purple
        } else if selectedValue == "Brown" {
            PrimaryColor = UIColor.brown
        } else if selectedValue == "White" {
            PrimaryColor = UIColor.white
        } else if selectedValue == "Magenta" {
            PrimaryColor = UIColor.magenta
        } else if selectedValue == "Cyan" {
            PrimaryColor = UIColor.cyan
        }
       
        FLPrimary = PrimaryColor
         let selectedValue2 = ThemeColors[SecondaryColorPicker.selectedRow(inComponent: 0)]
        if selectedValue2 == "Black" {
            SecondaryColor = UIColor.black
        } else if selectedValue2 == "Gray" {
            SecondaryColor = UIColor.gray
            
        } else if selectedValue2 == "Dark Gray" {
            SecondaryColor = UIColor.darkGray
            
        } else if selectedValue2 == "Orange" {
            SecondaryColor = UIColor.orange
        } else if selectedValue2 == "Red" {
            SecondaryColor = UIColor.red
        } else if selectedValue2 == "Blue" {
            SecondaryColor = UIColor.blue
            
        } else if selectedValue2 == "Yellow" {
            SecondaryColor = UIColor.yellow
        } else if selectedValue2 == "Purple" {
            SecondaryColor = UIColor.purple
        } else if selectedValue2 == "Brown" {
            SecondaryColor = UIColor.brown
        } else if selectedValue2 == "White" {
            SecondaryColor = UIColor.white
        } else if selectedValue2 == "Magenta" {
            SecondaryColor = UIColor.magenta
        } else if selectedValue2 == "Cyan" {
            SecondaryColor = UIColor.cyan
        }
        FLSecondary = SecondaryColor
        let selectedValue3 = ThemeColors[TextColorPicker.selectedRow(inComponent: 0)]
        if selectedValue3 == "Black" {
            TextColor = UIColor.black
        } else if selectedValue3 == "Gray" {
            TextColor = UIColor.gray
            
        } else if selectedValue3 == "Dark Gray" {
            TextColor = UIColor.darkGray
            
        } else if selectedValue3 == "Orange" {
            TextColor = UIColor.orange
        } else if selectedValue3 == "Red" {
            TextColor = UIColor.red
        } else if selectedValue3 == "Blue" {
            TextColor = UIColor.blue
            
        } else if selectedValue3 == "Yellow" {
            TextColor = UIColor.yellow
        } else if selectedValue3 == "Purple" {
            TextColor = UIColor.purple
        } else if selectedValue3 == "Brown" {
            TextColor = UIColor.brown
        } else if selectedValue3 == "White" {
            TextColor = UIColor.white
        } else if selectedValue3 == "Magenta" {
            TextColor = UIColor.magenta
        } else if selectedValue3 == "Cyan" {
            TextColor = UIColor.cyan
        }
        FLtextColor = TextColor
        
  //      print("Primary \(PrimaryColor)")
 //       print("Secondary \(SecondaryColor)")
 //       print("Text \(TextColor)")
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    //hiphopfuvv
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ThemeColors.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ThemeColors[row]
        
    }
    
    
    
    
    
    
    @IBOutlet weak var PrimaryColorPicker: UIPickerView!
    
    @IBOutlet weak var SecondaryColorPicker: UIPickerView!
    
    
    @IBOutlet weak var TextColorPicker: UIPickerView!
    
    
    @IBOutlet weak var PrimaryColorLabel: UILabel!
    
    @IBOutlet weak var SecondaryColorLabel: UILabel!
    @IBOutlet weak var TextColorLabel: UILabel!
}
