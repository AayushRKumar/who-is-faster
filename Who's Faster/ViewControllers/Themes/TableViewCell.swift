//
//  TableViewCell.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 10/7/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
   
    
    @IBOutlet weak var CellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
