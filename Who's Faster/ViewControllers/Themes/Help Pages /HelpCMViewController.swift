//
//  HelpCMViewController.swift
//  Who's Faster
//
//  Created by Aayush R Kumar on 12/26/18.
//  Copyright © 2018 Aayush R Kumar. All rights reserved.
//

import UIKit
import GoogleMobileAds
class HelpCMViewController: UIViewController, GADBannerViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        if noAds == false {
          
            
            // addBannerViewToView(bannerView)
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            bannerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
            bannerView.topAnchor.constraint(equalTo: helpPara.bottomAnchor).isActive = true
            
            bannerView.adUnitID = "ca-app-pub-2312248651171588/2980823738"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        }
        if hasTheme == true {
            helpPara.adjustsFontSizeToFitWidth = true
            helpTitle.adjustsFontSizeToFitWidth = true
            
            if CustomTheme == true {
                helpPara.textColor = TextColor
                helpTitle.textColor = TextColor
                self.view.backgroundColor = PrimaryColor
                normalFont()
                
            } else {
                if Theme == 0 {
                    //default
                    self.view.backgroundColor = UIColor.white
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    normalFont()
                    
                } else if Theme == 1 {
                    //dark
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    self.view.backgroundColor = UIColor.darkGray
                    helpPara.font = UIFont (name: "Marker Felt", size: 25)
                    helpTitle.font = UIFont (name: "Marker Felt", size: 35)
                } else if Theme == 2 {
                    //chalk
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Chalkboard")!)
                    helpPara.font = UIFont (name: "Chalkduster", size: 25)
                    helpTitle.font = UIFont (name: "Chalkduster", size: 35)
                } else if Theme == 3 {
                    //anicent
                    self.view.backgroundColor = UIColor.brown
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    helpPara.font = UIFont (name: "Papyrus", size: 25)
                    helpTitle.font = UIFont (name: "Papyrus", size: 35)
                } else if Theme == 4 {
                    //notes
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Note")!)
                    helpPara.font = UIFont (name: "Noteworthy", size: 25)
                    helpTitle.font = UIFont (name: "Noteworthy", size: 35)
                } else if Theme == 5 {
                    //space
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Space")!)
                    normalFont()
                    
                } else if Theme == 6 {
                    //ocean
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                    self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Underwater")!)
                    normalFont()
                    
                } else if Theme == 7 {
                    //red
                    normalFont()
                    
                    self.view.backgroundColor = UIColor.red
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 8 {
                    //blue
                    normalFont()
                    
                    self.view.backgroundColor = UIColor.blue
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 9 {
                    //green
                    normalFont()
                    
                    self.view.backgroundColor = UIColor.green
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 10 {
                    //purple
                    normalFont()
                    
                    self.view.backgroundColor = UIColor.purple
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 11 {
                    //orange
                    normalFont()
                    
                    self.view.backgroundColor = UIColor.orange
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 12 {
                    //pink
                    normalFont()
                    
                    self.view.backgroundColor = UIColor(red: (255/255.0), green: (192/255.0), blue: (203/255.0), alpha: 1.0)
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                } else if Theme == 13 {
                    //yellow
                    normalFont()
                    self.view.backgroundColor = UIColor.yellow
                    helpPara.textColor = UIColor.black
                    helpTitle.textColor = UIColor.black
                }
            }
        }
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    @IBOutlet weak var helpTitle: UILabel!
    
    @IBOutlet weak var helpPara: UILabel!
    func normalFont() {
        helpPara.font = UIFont (name: "Helvetica Neue", size: 25)
        helpTitle.font = UIFont (name: "Helvetica Neue", size: 35)
    }
}
